
CREATE USER photo WITH ENCRYPTED PASSWORD 'photopass';
CREATE DATABASE photo;
GRANT ALL PRIVILEGES ON DATABASE photo TO photo;
\connect photo
-- Scheme

CREATE TABLE photo (
	id BIGSERIAL PRIMARY KEY,
  uuid UUID NOT NULL UNIQUE,
  creator_uuid UUID NOT NULL,
  child_safe BOOLEAN NOT NULL DEFAULT false,
  dominant_color varchar(7) NOT NULL
);

CREATE TABLE subject_cache (
	subject_uuid UUID NOT NULL UNIQUE
);

GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO photo;
GRANT USAGE, SELECT ON ALL SEQUENCES IN SCHEMA public to photo;
