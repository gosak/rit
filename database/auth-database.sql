/*
 * Copyright (c) 2018. This file is subject to the terms and conditions defined in file 'LICENSE.txt' which is part of this source code package.
 */

-- User & Database

CREATE USER auth WITH ENCRYPTED PASSWORD 'authpass';
CREATE DATABASE auth;
GRANT ALL PRIVILEGES ON DATABASE auth TO auth;
\connect auth
-- Scheme

CREATE TABLE _user (
	id BIGSERIAL PRIMARY KEY,
	email VARCHAR(255) NOT NULL UNIQUE,
	name VARCHAR(40) NOT NULL UNIQUE,
	password_hash VARCHAR(70) NOT NULL,
	enabled BOOLEAN NOT NULL DEFAULT false,
  uuid UUID NOT NULL
);

CREATE TABLE device (
  id BIGSERIAL,
  uuid UUID NOT NULL,
  user_id BIGINT REFERENCES _user(id) ON UPDATE CASCADE,
  resource_secret VARCHAR(128) NOT NULL UNIQUE,
  refresh_secret VARCHAR(128) NOT NULL UNIQUE,
  CONSTRAINT device_pk PRIMARY KEY (id, user_id)
);

CREATE TABLE role (
	id BIGSERIAL PRIMARY KEY,
	name VARCHAR(40) NOT NULL UNIQUE
);

CREATE TABLE user_role (
	user_id BIGINT REFERENCES _user(id) ON UPDATE CASCADE,
	role_id BIGINT REFERENCES role(id) ON UPDATE CASCADE,
	CONSTRAINT user_role_pk PRIMARY KEY (user_id, role_id)
);

CREATE TABLE privilege (
	id BIGSERIAL PRIMARY KEY,
	name VARCHAR(40) NOT NULL UNIQUE
);

CREATE TABLE role_privilege (
	privilege_id BIGINT REFERENCES privilege(id) ON UPDATE CASCADE,
	role_id BIGINT REFERENCES role(id) ON UPDATE CASCADE,
	CONSTRAINT privilege_role_pk PRIMARY KEY (privilege_id, role_id)
);

GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO auth;
GRANT USAGE, SELECT ON ALL SEQUENCES IN SCHEMA public to auth;

-- Data

INSERT INTO role (id, name) VALUES
(1, 'ROLE_USER'),
(2, 'ROLE_RECOMME_ADMIN');

INSERT INTO privilege (id, name) VALUES
(1, 'ADD_PRODUCT_PRIVILEGE'),
(2, 'RATE_PRODUCT_PRIVILEGE'),
(3, 'CLEAR_RECOMME_DATABASE_PRIVILEGE'),
(4, 'DELETE_RECOMME_ITEM_PRIVILEGE');

INSERT INTO role_privilege (role_id, privilege_id) VALUES
(1, 1),
(1, 2),
(2, 3),
(2, 4);

INSERT INTO _user (email, name, password_hash, enabled, uuid) VALUES
('admin@recombee.com', 'recombee-admin', '$2a$12$vm1MH2S8jFcBDx9f5/pq5OrgcBSdnOO59vvBZSldH1aD5ZuHaSDHu', true, 'ef23556d-9896-4891b1bb-63a84771abeb');
INSERT INTO user_role (user_id, role_id) VALUES (1, 2);













