
CREATE USER gateway WITH ENCRYPTED PASSWORD 'gatewaypass';
CREATE DATABASE gateway;
GRANT ALL PRIVILEGES ON DATABASE gateway TO gateway;
\connect gateway
-- Scheme

CREATE TABLE device_cache (
	uuid UUID PRIMARY KEY,
  user_uuid UUID NOT NULL,
  refresh_secret varchar(128) NOT NULL,
  resource_secret varchar(128) NOT NULL
);

GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public to gateway;
GRANT USAGE, SELECT ON ALL SEQUENCES IN SCHEMA public to gateway;
