/*
 * Copyright (c) 2018. This file is subject to the terms and conditions defined in file 'LICENSE.txt' which is part of this source code package.
 */

-- User & Database

CREATE USER subject WITH ENCRYPTED PASSWORD 'subjectpass';
CREATE DATABASE subject;
GRANT ALL PRIVILEGES ON DATABASE subject TO subject;
\connect subject
-- Scheme

CREATE TABLE subject (
	id BIGSERIAL PRIMARY KEY,
  uuid UUID NOT NULL UNIQUE,
	name VARCHAR(60) NOT NULL,
  description VARCHAR(512),
  manufacturer VARCHAR(60),
  creator_uuid UUID,
  rate real,
  rates BIGINT NOT NULL DEFAULT 0
);

CREATE TABLE subject_photo (
  subject_id BIGSERIAL REFERENCES subject(id) ON UPDATE CASCADE,
  photo_uuid UUID NOT NULL,
  child_safe BOOLEAN NOT NULL DEFAULT false,
  is_primary BOOLEAN NOT NULL DEFAULT false,
  dominant_color VARCHAR(7) NOT NULL,
  PRIMARY KEY(subject_id, photo_uuid)
);

CREATE TABLE subject_rate (
  user_uuid UUID NOT NULL,
  subject_id BIGSERIAL REFERENCES subject(id) ON UPDATE CASCADE,
  rate real NOT NULL,
  PRIMARY KEY(user_uuid, subject_id)
);

GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO subject;
GRANT USAGE, SELECT ON ALL SEQUENCES IN SCHEMA public to subject;
