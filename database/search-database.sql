/*
 * Copyright (c) 2019. This file is subject to the terms and conditions defined in file 'LICENSE.txt' which is part of this source code package.
 */

-- User & Database

CREATE USER search WITH ENCRYPTED PASSWORD 'searchpass';
CREATE DATABASE search;
GRANT ALL PRIVILEGES ON DATABASE search TO search;
\connect search
-- Scheme

CREATE TABLE subject (
	id BIGSERIAL PRIMARY KEY,
  uuid UUID NOT NULL UNIQUE,
  rate real NOT NULL,
  child_safe BOOLEAN,
  photo_uuid UUID,
  photo_dominant_color varchar(7)
);

GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO search;
GRANT USAGE, SELECT ON ALL SEQUENCES IN SCHEMA public to search;
