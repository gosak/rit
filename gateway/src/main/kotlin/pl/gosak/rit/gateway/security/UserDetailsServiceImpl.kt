package pl.gosak.rit.gateway.security

import org.springframework.context.annotation.Primary
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.stereotype.Service

@Service
@Primary
class UserDetailsServiceImpl : UserDetailsService {
    override fun loadUserByUsername(username: String): UserDetails {
        throw NotImplementedError("It never should be called, auto configuration needs it.")
    }
}