package pl.gosak.rit.gateway.security

import org.springframework.cloud.netflix.zuul.filters.ZuulProperties
import org.springframework.context.annotation.Primary
import org.springframework.stereotype.Component
import pl.gosak.rit.lib.auth.AuthEndpoints
import pl.gosak.rit.microbase.security.SecurityProperties
import pl.gosak.rit.microbase.util.WhiteListPathsResolver
import java.util.*


@Primary
@Component
class WhiteListPathsResolverImpl(private val securityProperties: SecurityProperties,
                                 private val zuulProperties: ZuulProperties) : WhiteListPathsResolver {
    override fun resolve() =
            HashSet<String>(securityProperties.whitelist).apply {
                add("/${AuthEndpoints.REGISTER}")
                add("/${AuthEndpoints.LOGIN}")
                zuulProperties.routes.forEach {
                    add(it.value.path.replace("**", "v2/api-docs"))
                }
            }
}