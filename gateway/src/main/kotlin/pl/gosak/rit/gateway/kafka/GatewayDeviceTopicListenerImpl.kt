package pl.gosak.rit.gateway.kafka

import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Profile
import org.springframework.kafka.annotation.KafkaHandler
import org.springframework.kafka.annotation.KafkaListener
import org.springframework.messaging.handler.annotation.Payload
import org.springframework.stereotype.Component
import pl.gosak.rit.gateway.service.DeviceCacheService
import pl.gosak.rit.microbase.kafka.KafkaConstants.DEVICE_TOPIC
import pl.gosak.rit.microbase.kafka.KafkaConstants.DEVICE_TOPIC_GATEWAY_GROUP
import pl.gosak.rit.microbase.kafka.listener.DeviceTopicListener
import pl.gosak.rit.microbase.kafka.message.device.DeviceLoggedMsg
import pl.gosak.rit.microbase.kafka.message.device.DeviceLoggedOutMsg
import pl.gosak.rit.microbase.kafka.message.device.UserDevicesLoggedOutMsg

@Component
@Profile("local", "docker")
@KafkaListener(topics = [DEVICE_TOPIC], groupId = DEVICE_TOPIC_GATEWAY_GROUP)
class GatewayDeviceTopicListenerImpl(private val deviceCacheService: DeviceCacheService) : DeviceTopicListener {
    private val logger = LoggerFactory.getLogger(GatewayDeviceTopicListenerImpl::class.java)

    @KafkaHandler
    override fun handle(@Payload msg: DeviceLoggedMsg) {
        logger.info("KAFKA_GATEWAY: received DeviceLoggedMsg: {}", msg)
        deviceCacheService.deviceLogged(msg)
    }

    @KafkaHandler
    override fun handle(@Payload msg: DeviceLoggedOutMsg) {
        logger.info("KAFKA_GATEWAY: received DeviceLoggedOutMsg: {}", msg)
        deviceCacheService.deviceLoggedOut(msg)
    }

    @KafkaHandler
    override fun handle(@Payload msg: UserDevicesLoggedOutMsg) {
        logger.info("KAFKA_GATEWAY: received UserDevicesLoggedOutMsg: {}", msg)
        deviceCacheService.userDevicesLoggedOut(msg)
    }
}