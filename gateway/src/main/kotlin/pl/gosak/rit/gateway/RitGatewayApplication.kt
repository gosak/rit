package pl.gosak.rit.gateway

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.cloud.netflix.zuul.EnableZuulProxy
import org.springframework.context.annotation.Import
import pl.gosak.rit.microbase.database.DatabaseModule
import pl.gosak.rit.microbase.eureka.EurekaModule
import pl.gosak.rit.microbase.kafka.KafkaModule
import pl.gosak.rit.microbase.swagger.SwaggerModule
import pl.gosak.rit.microbase.util.RitUtilsModule

@EnableZuulProxy
@SpringBootApplication
@Import(RitUtilsModule::class,
        SwaggerModule::class,
        DatabaseModule::class,
        KafkaModule::class,
        EurekaModule::class)
class RitGatewayApplication {
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            SpringApplication.run(RitGatewayApplication::class.java, *args)
        }
    }
}