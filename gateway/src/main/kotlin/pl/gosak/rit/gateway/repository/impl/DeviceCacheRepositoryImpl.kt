package pl.gosak.rit.gateway.repository.impl

import org.springframework.stereotype.Repository
import pl.gosak.rit.gateway.model.DeviceCache
import pl.gosak.rit.gateway.repository.DeviceCacheRepository
import pl.gosak.rit.microbase.util.findBy
import pl.gosak.rit.microbase.util.remove
import java.util.*
import javax.persistence.EntityManager
import javax.persistence.EntityNotFoundException
import javax.persistence.PersistenceContext

@Repository
class DeviceCacheRepositoryImpl : DeviceCacheRepository {
    @PersistenceContext
    private lateinit var entityManager: EntityManager

    override fun persist(deviceCache: DeviceCache) = entityManager.persist(deviceCache)

    override fun delete(uuid: UUID): Boolean = entityManager.remove<DeviceCache, UUID>(DeviceCache.UUID, uuid)

    override fun update(deviceCache: DeviceCache) {
        entityManager.merge(deviceCache)
    }

    override fun load(uuid: UUID): DeviceCache = entityManager.findBy(DeviceCache.UUID, uuid)
            ?: throw EntityNotFoundException("device with uuid: $uuid not found")
}