package pl.gosak.rit.gateway.repository

import pl.gosak.rit.gateway.model.DeviceCache
import java.util.*

interface DeviceCacheRepository {
    fun persist(deviceCache: DeviceCache)

    fun delete(uuid: UUID): Boolean

    fun update(deviceCache: DeviceCache)

    fun load(uuid: UUID): DeviceCache
}