/*
 * Copyright (c) 2018. This file is subject to the terms and conditions defined in file 'LICENSE.txt' which is part of this source code package.
 */

package pl.gosak.rit.gateway.config

import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary
import org.springframework.http.HttpMethod
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter
import org.springframework.web.cors.CorsConfiguration
import org.springframework.web.cors.UrlBasedCorsConfigurationSource
import pl.gosak.rit.gateway.repository.DeviceCacheRepository
import pl.gosak.rit.gateway.security.filter.JwtAuthorizationFilter
import pl.gosak.rit.microbase.security.SecurityProperties
import pl.gosak.rit.microbase.util.WhiteListPathsResolver
import java.util.*


@Primary
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
@EnableConfigurationProperties(SecurityProperties::class)
class WebSecurityConfig(
        val deviceCacheRepository: DeviceCacheRepository,
        val base64Decoder: Base64.Decoder,
        val whiteListPathsResolver: WhiteListPathsResolver) : WebSecurityConfigurerAdapter() {
    override fun configure(http: HttpSecurity) {
        val authorizationFilter = JwtAuthorizationFilter(authenticationManager(), base64Decoder, deviceCacheRepository)
        http.csrf().disable().cors().and().authorizeRequests().antMatchers(*whiteListPathsResolver.resolve().toTypedArray()).permitAll()
                .anyRequest().authenticated()
                .and()
                .addFilterAfter(authorizationFilter, UsernamePasswordAuthenticationFilter::class.java)
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
    }

    @Bean
    fun authManager(): AuthenticationManager = authenticationManager()

    @Bean
    fun corsConfigurationSource() = UrlBasedCorsConfigurationSource().apply {
        registerCorsConfiguration("/**", CorsConfiguration().applyPermitDefaultValues().apply {
            allowedMethods = mutableListOf(HttpMethod.GET, HttpMethod.POST, HttpMethod.PUT,
                    HttpMethod.DELETE, HttpMethod.HEAD).map(HttpMethod::name)
        })
    }
}