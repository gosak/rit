package pl.gosak.rit.gateway.controller

import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.cloud.netflix.zuul.filters.ZuulProperties
import org.springframework.context.annotation.Primary
import org.springframework.stereotype.Component
import springfox.documentation.swagger.web.SwaggerResource
import springfox.documentation.swagger.web.SwaggerResourcesProvider
import java.util.*

@Primary
@Component
@EnableAutoConfiguration
class DocumentationController(private val zuulProperties: ZuulProperties) : SwaggerResourcesProvider {
    override fun get() = ArrayList<SwaggerResource>().apply {
        zuulProperties.routes.forEach {
            add(swaggerResource(it.key, serviceLocation(it.value.path), "undefined"))
        }
    }

    private fun swaggerResource(name: String, location: String, version: String) =
            SwaggerResource().apply {
                this.name = name
                this.location = location
                this.swaggerVersion = version
            }

    private fun serviceLocation(path: String) = path.replace("**", "v2/api-docs")
}