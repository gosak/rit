package pl.gosak.rit.gateway.service.impl

import org.springframework.stereotype.Service
import pl.gosak.rit.gateway.model.DeviceCache
import pl.gosak.rit.gateway.repository.DeviceCacheRepository
import pl.gosak.rit.gateway.service.DeviceCacheService
import pl.gosak.rit.microbase.kafka.message.device.DeviceLoggedMsg
import pl.gosak.rit.microbase.kafka.message.device.DeviceLoggedOutMsg
import pl.gosak.rit.microbase.kafka.message.device.UserDevicesLoggedOutMsg
import javax.transaction.Transactional


@Service
class DeviceCacheServiceImpl(private val deviceCacheRepository: DeviceCacheRepository) : DeviceCacheService {
    @Transactional
    override fun deviceLogged(deviceLoggedMsg: DeviceLoggedMsg) {
        runCatching {
            deviceCacheRepository.load(deviceLoggedMsg.deviceUuid).apply {
                refreshSecret = deviceLoggedMsg.refreshSecret
                resourceSecret = deviceLoggedMsg.resourceSecret
                deviceCacheRepository.update(this)
                return
            }
        }

        deviceCacheRepository.persist(DeviceCache(deviceLoggedMsg.deviceUuid,
                deviceLoggedMsg.userUuid,
                deviceLoggedMsg.refreshSecret,
                deviceLoggedMsg.resourceSecret))
    }

    @Transactional
    override fun deviceLoggedOut(deviceLoggedOutMsg: DeviceLoggedOutMsg) {
        deviceCacheRepository.delete(deviceLoggedOutMsg.deviceUuid)
    }

    override fun userDevicesLoggedOut(userDevicesLoggedOutMsg: UserDevicesLoggedOutMsg) {
        throw NotImplementedError()
    }
}