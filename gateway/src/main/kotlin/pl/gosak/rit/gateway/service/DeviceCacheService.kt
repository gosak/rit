package pl.gosak.rit.gateway.service

import pl.gosak.rit.microbase.kafka.message.device.DeviceLoggedMsg
import pl.gosak.rit.microbase.kafka.message.device.DeviceLoggedOutMsg
import pl.gosak.rit.microbase.kafka.message.device.UserDevicesLoggedOutMsg

interface DeviceCacheService {
    fun deviceLogged(deviceLoggedMsg: DeviceLoggedMsg)

    fun deviceLoggedOut(deviceLoggedOutMsg: DeviceLoggedOutMsg)

    fun userDevicesLoggedOut(userDevicesLoggedOutMsg: UserDevicesLoggedOutMsg)
}