package pl.gosak.rit.gateway.model

import pl.gosak.rit.gateway.model.DeviceCache.Companion.TABLE
import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table


@Entity
@Table(name = TABLE)
class DeviceCache(
        @Id
        @Column(name = UUID)
        val uuid: UUID,

        @Column(name = DB_USER_UUID)
        val userUuid: UUID,

        @Column(name = DB_REFRESH_SECRET)
        var refreshSecret: String,

        @Column(name = DB_RESOURCE_SECRET)
        var resourceSecret: String
) {
    companion object {
        const val TABLE = "device_cache"
        const val UUID = "uuid"
        const val USER_UUID = "userUuid"
        const val DB_USER_UUID = "user_uuid"
        const val REFRESH_SECRET = "refreshSecret"
        const val DB_REFRESH_SECRET = "refresh_secret"
        const val RESOURCE_SECRET = "resourceSecret"
        const val DB_RESOURCE_SECRET = "resource_secret"
    }
}