package pl.gosak.rit.auth.controller

import org.springframework.dao.DataIntegrityViolationException
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import pl.gosak.rit.microbase.util.responseEntity

@ControllerAdvice
class DataIntegrityViolationExceptionAdvice {
    @ExceptionHandler(DataIntegrityViolationException::class)
    fun handle() = HttpStatus.CONFLICT.responseEntity()
}