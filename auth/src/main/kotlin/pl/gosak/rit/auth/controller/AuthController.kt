/*
 * Copyright (c) 2018. This file is subject to the terms and conditions defined in file 'LICENSE.txt' which is part of this source code package.
 */

package pl.gosak.rit.auth.controller

import org.modelmapper.ModelMapper
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.security.core.Authentication
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestHeader
import org.springframework.web.bind.annotation.RestController
import pl.gosak.rit.auth.security.User
import pl.gosak.rit.auth.security.service.JwtTokenService
import pl.gosak.rit.auth.service.DeviceService
import pl.gosak.rit.auth.service.UserService
import pl.gosak.rit.lib.SecurityConstants.Roles.REFRESH_TOKEN
import pl.gosak.rit.lib.SecurityConstants.Roles.USER
import pl.gosak.rit.lib.auth.AuthEndpoints.LOGIN
import pl.gosak.rit.lib.auth.AuthEndpoints.LOGOUT
import pl.gosak.rit.lib.auth.AuthEndpoints.LOGOUT_ALL
import pl.gosak.rit.lib.auth.AuthEndpoints.REGISTER
import pl.gosak.rit.lib.auth.AuthEndpoints.RESET_PASSWORD
import pl.gosak.rit.lib.auth.AuthEndpoints.REVOKE
import pl.gosak.rit.lib.auth.AuthEndpoints.TEST
import pl.gosak.rit.lib.auth.dto.AuthenticationRequest
import pl.gosak.rit.lib.auth.dto.AuthenticationResponse
import pl.gosak.rit.lib.auth.dto.RegisterRequest
import pl.gosak.rit.lib.auth.dto.RegisterResponse
import pl.gosak.rit.lib.auth.dto.TestResponse
import pl.gosak.rit.microbase.util.postResponse
import pl.gosak.rit.microbase.util.putResponse
import pl.gosak.rit.microbase.util.responseEntity

@RestController
class AuthController(private var userService: UserService,
                     private var jwtTokenService: JwtTokenService,
                     private var deviceService: DeviceService,
                     private var modelMapper: ModelMapper) {
    @PostMapping(REGISTER)
    fun register(@RequestBody registerRequest: RegisterRequest) =
            modelMapper.postResponse<RegisterResponse>(userService.register(registerRequest))

    @PostMapping(LOGIN)
    fun login(@RequestBody authenticationRequest: AuthenticationRequest) {
    }

    @GetMapping(TEST)
    fun test(@RequestHeader Authorization: String, authentication: Authentication): ResponseEntity<TestResponse> {
        val user = (authentication.principal as User)
        return HttpStatus.OK.responseEntity(TestResponse(user.deviceUuid, user.userUuid))
    }

    @GetMapping(REVOKE)
    @PreAuthorize("hasRole('$REFRESH_TOKEN')")
    fun revoke(@RequestHeader Authorization: String, authentication: Authentication): ResponseEntity<AuthenticationResponse> {
        val tokens = jwtTokenService.generateTokens(authentication)
        return ResponseEntity(AuthenticationResponse(tokens.first, tokens.second), HttpStatus.CREATED)
    }

    @PutMapping(LOGOUT)
    @PreAuthorize("hasRole('$USER')")
    fun logout(@RequestHeader Authorization: String, authentication: Authentication) =
            putResponse(deviceService.logout((authentication.principal as User).deviceUuid))

    @PutMapping(LOGOUT_ALL)
    @PreAuthorize("hasRole('$USER')")
    fun logoutAll(@RequestHeader Authorization: String, authentication: Authentication) =
            putResponse(deviceService.logoutAllDevices((authentication.principal as User).userUuid))

    @PostMapping(RESET_PASSWORD)
    fun resetPassword(mail: String) {
    }
}