/*
 * Copyright (c) 2018. This file is subject to the terms and conditions defined in file 'LICENSE.txt' which is part of this source code package.
 */

package pl.gosak.rit.auth.model

import org.springframework.security.core.GrantedAuthority
import pl.gosak.rit.auth.model.DeviceEntity.Companion.DB_USER_ID
import pl.gosak.rit.auth.model.UserEntity.Companion.USER_TABLE
import pl.gosak.rit.microbase.util.DatabaseConstants.SCHEMA
import java.util.*
import javax.persistence.*

@Entity
@Table(name = USER_TABLE, schema = SCHEMA)
class UserEntity(
        @Id
        @Column(name = ID)
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long,

        @Column(name = EMAIL, unique = true, nullable = false)
        var email: String,

        @Column(name = NAME, unique = true, nullable = false)
        val name: String,

        @Column(name = DB_HASH, nullable = false)
        var passwordHash: String,

        @Column(name = ENABLED, nullable = false)
        var enabled: Boolean,

        @Column(name = DB_UUID, nullable = false)
        val uuid: UUID,

        @ManyToMany(cascade = [CascadeType.ALL], fetch = FetchType.EAGER)
        @JoinTable(name = "user_role",
                joinColumns = [JoinColumn(name = "user_id")],
                inverseJoinColumns = [JoinColumn(name = "role_id")])
        val roles: MutableSet<RoleEntity>,

        @OneToMany(fetch = FetchType.LAZY, orphanRemoval = true)
        @JoinColumn(name = DB_USER_ID)
        val devices: MutableSet<DeviceEntity>
) {
    constructor(username: String, email: String) : this(0, email, username, "", true, UUID.randomUUID(), HashSet<RoleEntity>(), HashSet<DeviceEntity>())

    fun getAuthorities() = HashSet<GrantedAuthority>().apply {
        roles.forEach {
            add(it)
            addAll(it.privileges)
        }
    }

    companion object {
        const val USER_TABLE = "_user"
        const val ID = "id"
        const val EMAIL = "email"
        const val NAME = "name"
        const val HASH = "passwordHash"
        const val DB_HASH = "password_hash"
        const val ENABLED = "enabled"
        const val DB_UUID = "uuid"
    }
}