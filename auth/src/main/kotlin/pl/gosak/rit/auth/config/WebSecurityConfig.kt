/*
 * Copyright (c) 2018. This file is subject to the terms and conditions defined in file 'LICENSE.txt' which is part of this source code package.
 */

package pl.gosak.rit.auth.config

import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary
import org.springframework.http.HttpMethod
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.dao.DaoAuthenticationProvider
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter
import org.springframework.web.cors.CorsConfiguration
import org.springframework.web.cors.UrlBasedCorsConfigurationSource
import pl.gosak.rit.auth.repository.DeviceRepository
import pl.gosak.rit.auth.repository.UserRepository
import pl.gosak.rit.auth.security.filter.JwtAuthenticationFilter
import pl.gosak.rit.auth.security.filter.JwtAuthorizationFilter
import pl.gosak.rit.auth.security.service.JwtTokenService
import pl.gosak.rit.microbase.security.SecurityProperties
import pl.gosak.rit.microbase.util.WhiteListPathsResolver
import java.util.*


@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
@EnableConfigurationProperties(SecurityProperties::class)
class WebSecurityConfig(val jwtTokenService: JwtTokenService,
                        val userDetailsService: UserDetailsService,
                        val passwordEncoder: PasswordEncoder,
                        val deviceRepository: DeviceRepository,
                        val userRepository: UserRepository,
                        val base64Decoder: Base64.Decoder,
                        val whiteListPathsResolver: WhiteListPathsResolver) : WebSecurityConfigurerAdapter() {
    override fun configure(http: HttpSecurity) {
        val authorizationFilter = JwtAuthorizationFilter(authenticationManager(), deviceRepository, userRepository, base64Decoder)
        val authenticationFilter = JwtAuthenticationFilter(authenticationManager(), jwtTokenService)
        http.csrf().disable().cors().and().authorizeRequests().antMatchers(*whiteListPathsResolver.resolve().toTypedArray()).permitAll()
                .anyRequest().authenticated()
                .and()
                .addFilterBefore(authenticationFilter, UsernamePasswordAuthenticationFilter::class.java)
                .addFilterAfter(authorizationFilter, UsernamePasswordAuthenticationFilter::class.java)
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
    }

    override fun configure(auth: AuthenticationManagerBuilder) {
        auth.authenticationProvider(authenticationProvider())
    }

    @Bean
    @Primary
    fun authenticationProvider() = DaoAuthenticationProvider().apply {
        setUserDetailsService(userDetailsService)
        setPasswordEncoder(passwordEncoder)
    }

    @Bean
    fun authManager(): AuthenticationManager = authenticationManager()

    @Bean
    fun corsConfigurationSource() = UrlBasedCorsConfigurationSource().apply {
        registerCorsConfiguration("/**", CorsConfiguration().applyPermitDefaultValues().apply {
            allowedMethods = mutableListOf(HttpMethod.GET, HttpMethod.POST, HttpMethod.PUT,
                    HttpMethod.DELETE, HttpMethod.HEAD).map(HttpMethod::name)
        })
    }
}