/*
 * Copyright (c) 2018. This file is subject to the terms and conditions defined in file 'LICENSE.txt' which is part of this source code package.
 */

package pl.gosak.rit.auth.model

import org.springframework.security.core.GrantedAuthority
import pl.gosak.rit.auth.model.PrivilegeEntity.Companion.PRIVILEGE_TABLE
import pl.gosak.rit.microbase.util.DatabaseConstants.SCHEMA
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = PRIVILEGE_TABLE, schema = SCHEMA)
data class PrivilegeEntity(
        @Id
        @Column(name = ID)
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long,

        @Column(name = NAME, unique = true, nullable = false)
        val name: String
) : GrantedAuthority {
    companion object {
        const val PRIVILEGE_TABLE = "privilege"
        const val ID = "id"
        const val NAME = "name"
    }

    override fun getAuthority() = name

    override fun toString() = authority
}