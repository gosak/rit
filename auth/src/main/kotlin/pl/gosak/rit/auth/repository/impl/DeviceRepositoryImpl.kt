/*
 * Copyright (c) 2018 This file is subject to the terms and conditions defined in file 'LICENSE.txt' which is part of this source code package.
 */

package pl.gosak.rit.auth.repository.impl

import org.springframework.stereotype.Repository
import pl.gosak.rit.auth.model.DeviceEntity
import pl.gosak.rit.auth.repository.DeviceRepository
import pl.gosak.rit.microbase.util.findBy
import pl.gosak.rit.microbase.util.remove
import java.util.*
import javax.persistence.EntityManager
import javax.persistence.EntityNotFoundException
import javax.persistence.PersistenceContext

@Repository
class DeviceRepositoryImpl : DeviceRepository {
    @PersistenceContext
    lateinit var entityManager: EntityManager

    override fun add(deviceEntity: DeviceEntity) = deviceEntity.apply {
        entityManager.persist(this)
    }

    override fun find(uuid: UUID): DeviceEntity = entityManager.findBy(DeviceEntity.DB_UUID, uuid)
            ?: throw EntityNotFoundException("Device with id=$uuid not found")

    override fun delete(userUuid: UUID) = entityManager.remove<DeviceEntity, UUID>(DeviceEntity.DB_UUID, userUuid)

    override fun update(deviceEntity: DeviceEntity): DeviceEntity = entityManager.merge(deviceEntity)

    override fun delete(deviceEntity: DeviceEntity) = entityManager.contains(deviceEntity).also {
        entityManager.remove(if (it) deviceEntity else entityManager.merge(deviceEntity))
    }
}