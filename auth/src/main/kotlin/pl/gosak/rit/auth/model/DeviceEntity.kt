/*
 * Copyright (c) 2018. This file is subject to the terms and conditions defined in file 'LICENSE.txt' which is part of this source code package.
 */

package pl.gosak.rit.auth.model

import pl.gosak.rit.auth.model.DeviceEntity.Companion.DEVICE_TABLE
import pl.gosak.rit.auth.security.generateSecret
import pl.gosak.rit.microbase.util.DatabaseConstants.SCHEMA
import java.util.*
import javax.persistence.*

@Entity
@Table(name = DEVICE_TABLE, schema = SCHEMA)
class DeviceEntity(
        @Id
        @Column(name = ID)
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long,

        @Column(name = DB_USER_ID)
        val userId: Long,

        @Column(name = DB_UUID, unique = true, nullable = false)
        var uuid: UUID,

        @Column(name = DB_REFRESH_SECRET, unique = true, nullable = false)
        var refreshSecret: String,

        @Column(name = DB_RESOURCE_SECRET, unique = true, nullable = false)
        var resourceSecret: String
) {
    constructor(userId: Long) : this(0, userId, UUID.randomUUID(), generateSecret(), generateSecret())

    companion object {
        const val DEVICE_TABLE = "device"
        const val ID = "id"
        const val DB_UUID = "uuid"
        const val USER_ID = "userId"
        const val DB_USER_ID = "user_id"
        const val RESOURCE_SECRET = "resourceSecret"
        const val DB_RESOURCE_SECRET = "resource_secret"
        const val REFRESH_SECRET = "refreshSecret"
        const val DB_REFRESH_SECRET = "refresh_secret"
    }
}