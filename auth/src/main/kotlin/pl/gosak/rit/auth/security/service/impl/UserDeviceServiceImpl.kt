/*
 * Copyright (c) 2018 This file is subject to the terms and conditions defined in file 'LICENSE.txt' which is part of this source code package.
 */

package pl.gosak.rit.auth.security.service.impl

import org.springframework.context.annotation.Primary
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.stereotype.Service
import pl.gosak.rit.auth.model.DeviceEntity
import pl.gosak.rit.auth.repository.DeviceRepository
import pl.gosak.rit.auth.repository.UserRepository
import pl.gosak.rit.auth.security.User
import javax.transaction.Transactional


@Service
@Primary
class UserDeviceServiceImpl(private val userRepository: UserRepository,
                            private val deviceRepository: DeviceRepository) : UserDetailsService {
    /**
     * It's used only on login with username and password
     * DeviceEntity is created here
     */
    @Transactional
    override fun loadUserByUsername(email: String): User {
        val userEntity = userRepository.find(email)
        val deviceEntity = DeviceEntity(userEntity.id)
        deviceRepository.add(deviceEntity)
        return User(userEntity, deviceEntity.uuid)
    }
}