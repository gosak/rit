/*
 * Copyright (c) 2018. This file is subject to the terms and conditions defined in file 'LICENSE.txt' which is part of this source code package.
 */

package pl.gosak.rit.auth.factory.impl

import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Component
import pl.gosak.rit.auth.factory.UserEntityFactory
import pl.gosak.rit.auth.model.UserEntity
import pl.gosak.rit.auth.repository.RoleRepository
import pl.gosak.rit.lib.SecurityConstants.Roles.ROLE_USER
import pl.gosak.rit.lib.auth.dto.RegisterRequest

@Component
class UserEntityFactoryImpl(private val roleRepository: RoleRepository,
                            private val passwordEncoder: PasswordEncoder) : UserEntityFactory {
    override fun create(username: String, email: String, password: String) = runBlocking {
        UserEntity(username, email).apply {
            launch { passwordHash = passwordEncoder.encode(password) }
            launch { roles.add(roleRepository.find(ROLE_USER)) }
        }
    }

    override fun create(registerRequest: RegisterRequest) = registerRequest.run { create(name, email, password) }
}