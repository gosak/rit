/*
 * Copyright (c) 2018. This file is subject to the terms and conditions defined in file 'LICENSE.txt' which is part of this source code package.
 */

package pl.gosak.rit.auth

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.Import
import pl.gosak.rit.microbase.database.DatabaseModule
import pl.gosak.rit.microbase.eureka.EurekaModule
import pl.gosak.rit.microbase.kafka.KafkaModule
import pl.gosak.rit.microbase.swagger.SwaggerModule
import pl.gosak.rit.microbase.util.RitUtilsModule

@SpringBootApplication
@Import(RitUtilsModule::class,
        SwaggerModule::class,
        DatabaseModule::class,
        KafkaModule::class,
        EurekaModule::class)
class RitAuthApplication {
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            SpringApplication.run(RitAuthApplication::class.java, *args)
        }
    }
}