/*
 * Copyright (c) 2018. This file is subject to the terms and conditions defined in file 'LICENSE.txt' which is part of this source code package.
 */

package pl.gosak.rit.auth.security

import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import pl.gosak.rit.auth.model.UserEntity
import java.util.*

class User(val userUuid: UUID,
           val deviceUuid: UUID,
           var enabled: Boolean = false,
           private val grantedAuthorities: List<GrantedAuthority>) : UserDetails {
    var name: String = ""
    var passwordHash: String = ""

    constructor(userEntity: UserEntity, deviceUuid: UUID) : this(userEntity.uuid, deviceUuid, userEntity.enabled, userEntity.getAuthorities().toList()) {
        name = userEntity.name
        passwordHash = userEntity.passwordHash
    }

    override fun getUsername() = name

    override fun isCredentialsNonExpired() = true

    override fun isAccountNonExpired() = true

    override fun isAccountNonLocked() = true

    override fun isEnabled() = enabled

    override fun getPassword() = passwordHash

    override fun getAuthorities() = grantedAuthorities
}