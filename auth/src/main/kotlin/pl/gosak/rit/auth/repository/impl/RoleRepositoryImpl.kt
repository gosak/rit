/*
 * Copyright (c) 2018. This file is subject to the terms and conditions defined in file 'LICENSE.txt' which is part of this source code package.
 */

package pl.gosak.rit.auth.repository.impl

import org.springframework.stereotype.Repository
import pl.gosak.rit.auth.model.RoleEntity
import pl.gosak.rit.auth.repository.RoleRepository
import pl.gosak.rit.microbase.util.findBy
import javax.persistence.EntityManager
import javax.persistence.EntityNotFoundException
import javax.persistence.PersistenceContext

@Repository
class RoleRepositoryImpl : RoleRepository {
    @PersistenceContext
    lateinit var entityManager: EntityManager

    override fun find(username: String) = findEntity(username)

    private fun findEntity(username: String): RoleEntity = entityManager.findBy(RoleEntity.NAME, username)
            ?: throw EntityNotFoundException("Role $username not found")
}