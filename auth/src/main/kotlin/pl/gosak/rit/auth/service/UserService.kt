/*
 * Copyright (c) 2018. This file is subject to the terms and conditions defined in file 'LICENSE.txt' which is part of this source code package.
 */

package pl.gosak.rit.auth.service

import pl.gosak.rit.auth.model.UserEntity
import pl.gosak.rit.lib.auth.dto.RegisterRequest

interface UserService {
    fun resetPassword(mail: String)

    fun register(request: RegisterRequest): UserEntity
}