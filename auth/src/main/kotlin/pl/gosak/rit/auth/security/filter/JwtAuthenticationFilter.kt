/*
 * Copyright (c) 2018. This file is subject to the terms and conditions defined in file 'LICENSE.txt' which is part of this source code package.
 */

package pl.gosak.rit.auth.security.filter

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import org.springframework.http.HttpStatus
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter
import org.springframework.security.web.util.matcher.AntPathRequestMatcher
import pl.gosak.rit.auth.security.service.JwtTokenService
import pl.gosak.rit.lib.auth.AuthEndpoints.LOGIN
import pl.gosak.rit.lib.auth.dto.AuthenticationRequest
import pl.gosak.rit.lib.auth.dto.AuthenticationResponse
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

class JwtAuthenticationFilter(authManager: AuthenticationManager,
                              private val jwtTokenService: JwtTokenService) : AbstractAuthenticationProcessingFilter(AntPathRequestMatcher("/$LOGIN")) {
    init {
        authenticationManager = authManager
    }

    override fun attemptAuthentication(req: HttpServletRequest, res: HttpServletResponse): Authentication {
        val request: AuthenticationRequest = jacksonObjectMapper().readValue(req.inputStream)
        return authenticationManager.authenticate(UsernamePasswordAuthenticationToken(request.email, request.password, null))
    }

    override fun successfulAuthentication(req: HttpServletRequest, res: HttpServletResponse, chain: FilterChain, auth: Authentication) {
        val tokens = jwtTokenService.generateTokens(auth)
        res.status = HttpStatus.CREATED.value()
        res.addHeader("content-type", "application/json;charset=UTF-8")
        res.writer.apply {
            write(jacksonObjectMapper().writeValueAsString(AuthenticationResponse(tokens.first, tokens.second)))
            flush()
            close()
        }
    }
}