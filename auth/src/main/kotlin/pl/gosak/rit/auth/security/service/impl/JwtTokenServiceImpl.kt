/*
 * Copyright (c) 2018 This file is subject to the terms and conditions defined in file 'LICENSE.txt' which is part of this source code package.
 */

package pl.gosak.rit.auth.security.service.impl

import org.springframework.security.core.Authentication
import org.springframework.security.core.GrantedAuthority
import org.springframework.stereotype.Service
import pl.gosak.rit.auth.repository.DeviceRepository
import pl.gosak.rit.auth.repository.UserRepository
import pl.gosak.rit.auth.security.SecurityConstants
import pl.gosak.rit.auth.security.SecurityConstants.REFRESH_TOKEN_EXPIRATION_TIME
import pl.gosak.rit.auth.security.User
import pl.gosak.rit.auth.security.generateToken
import pl.gosak.rit.auth.security.service.JwtTokenService
import pl.gosak.rit.auth.service.DeviceService
import pl.gosak.rit.lib.SecurityConstants.Roles.ROLE_REFRESH_TOKEN

@Service
class JwtTokenServiceImpl(private val deviceRepository: DeviceRepository,
                          private val deviceService: DeviceService,
                          private val userRepository: UserRepository) : JwtTokenService {
    override fun generateTokens(authentication: Authentication): Pair<String, String> {
        val user = authentication.principal as User

        return deviceRepository.find(user.deviceUuid).run {
            val secrets = deviceService.generateSecrets(user.userUuid, this)
            val userAuthorities = userRepository.find(user.userUuid).getAuthorities()

            val resourceToken = generateToken(user.userUuid, uuid,
                    SecurityConstants.RESOURCE_TOKEN_EXPIRATION_TIME,
                    userAuthorities.map(GrantedAuthority::getAuthority).toTypedArray(),
                    secrets.first)
            val refreshToken = generateToken(user.userUuid, uuid, REFRESH_TOKEN_EXPIRATION_TIME, arrayOf(ROLE_REFRESH_TOKEN), secrets.second)
            Pair(resourceToken, refreshToken)
        }
    }
}