/*
 * Copyright (c) 2018. This file is subject to the terms and conditions defined in file 'LICENSE.txt' which is part of this source code package.
 */

package pl.gosak.rit.auth.repository.impl

import org.springframework.stereotype.Repository
import pl.gosak.rit.auth.model.UserEntity
import pl.gosak.rit.auth.repository.UserRepository
import pl.gosak.rit.microbase.util.findBy
import java.util.*
import javax.persistence.EntityManager
import javax.persistence.EntityNotFoundException
import javax.persistence.PersistenceContext

@Repository
class UserRepositoryImpl : UserRepository {
    @PersistenceContext
    lateinit var entityManager: EntityManager

    override fun find(email: String) = findEntity(email)

    private fun findEntity(email: String): UserEntity = entityManager.findBy(UserEntity.EMAIL, email)
            ?: throw EntityNotFoundException("User $email not found")

    override fun find(uuid: UUID) = findEntity(uuid)

    private fun findEntity(uuid: UUID): UserEntity = entityManager.findBy(UserEntity.DB_UUID, uuid)
            ?: throw EntityNotFoundException("User with uuid=$uuid not found")

    override fun add(user: UserEntity) = entityManager.persist(user)

    override fun update(user: UserEntity): UserEntity = entityManager.merge(user)
}
