package pl.gosak.rit.auth.security

import org.springframework.stereotype.Component
import pl.gosak.rit.lib.auth.AuthEndpoints
import pl.gosak.rit.microbase.security.SecurityProperties
import pl.gosak.rit.microbase.util.WhiteListPathsResolver
import java.util.*


@Component
class WhiteListPathsResolverImpl(private val securityProperties: SecurityProperties) : WhiteListPathsResolver {
    override fun resolve() =
            HashSet<String>(securityProperties.whitelist).apply {
                add("/${AuthEndpoints.REGISTER}")
                add("/${AuthEndpoints.LOGIN}")
            }
}