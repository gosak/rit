/*
 * Copyright (c) 2018. This file is subject to the terms and conditions defined in file 'LICENSE.txt' which is part of this source code package.
 */

package pl.gosak.rit.auth.factory

import pl.gosak.rit.auth.model.UserEntity
import pl.gosak.rit.lib.auth.dto.RegisterRequest

interface UserEntityFactory {
    fun create(username: String, email: String, password: String): UserEntity

    fun create(registerRequest: RegisterRequest): UserEntity
}