/*
 * Copyright (c) 2018 This file is subject to the terms and conditions defined in file 'LICENSE.txt' which is part of this source code package.
 */

package pl.gosak.rit.auth.repository

import pl.gosak.rit.auth.model.DeviceEntity
import java.util.*

interface DeviceRepository {
    fun add(deviceEntity: DeviceEntity): DeviceEntity

    fun update(deviceEntity: DeviceEntity): DeviceEntity

    fun delete(deviceEntity: DeviceEntity): Boolean

    fun delete(userUuid: UUID): Boolean

    fun find(uuid: UUID): DeviceEntity
}