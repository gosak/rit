/*
 * Copyright (c) 2018. This file is subject to the terms and conditions defined in file 'LICENSE.txt' which is part of this source code package.
 */

package pl.gosak.rit.auth.model

import org.springframework.security.core.GrantedAuthority
import pl.gosak.rit.auth.model.RoleEntity.Companion.ROLE_TABLE
import pl.gosak.rit.microbase.util.DatabaseConstants.SCHEMA
import javax.persistence.*

@Entity
@Table(name = ROLE_TABLE, schema = SCHEMA)
data class RoleEntity(
        @Id
        @Column(name = ID)
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long,

        @Column(name = NAME, unique = true, nullable = false)
        val name: String,

        @ManyToMany(cascade = [CascadeType.ALL], fetch = FetchType.EAGER)
        @JoinTable(name = "role_privilege",
                joinColumns = [JoinColumn(name = "role_id")],
                inverseJoinColumns = [JoinColumn(name = "privilege_id")])
        val privileges: MutableSet<PrivilegeEntity>
) : GrantedAuthority {
    companion object {
        const val ROLE_TABLE = "role"
        const val ID = "id"
        const val NAME = "name"
    }

    override fun getAuthority() = name

    override fun toString() = authority
}
