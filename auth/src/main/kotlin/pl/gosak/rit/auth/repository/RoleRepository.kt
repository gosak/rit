/*
 * Copyright (c) 2018. This file is subject to the terms and conditions defined in file 'LICENSE.txt' which is part of this source code package.
 */

package pl.gosak.rit.auth.repository

import pl.gosak.rit.auth.model.RoleEntity

interface RoleRepository {
    fun find(username: String): RoleEntity
}