/*
 * Copyright (c) 2018. This file is subject to the terms and conditions defined in file 'LICENSE.txt' which is part of this source code package.
 */

package pl.gosak.rit.auth.service.impl

import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import pl.gosak.rit.auth.factory.UserEntityFactory
import pl.gosak.rit.auth.repository.UserRepository
import pl.gosak.rit.auth.service.UserService
import pl.gosak.rit.lib.auth.dto.RegisterRequest
import pl.gosak.rit.microbase.kafka.KafkaConstants
import pl.gosak.rit.microbase.kafka.message.user.NewUserMsg
import pl.gosak.rit.microbase.kafka.service.KafkaSender

@Service
class UserServiceImpl(private val userRepository: UserRepository,
                      private val userEntityFactory: UserEntityFactory,
                      private val kafkaSender: KafkaSender) : UserService {
    override fun resetPassword(mail: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    @Transactional
    override fun register(request: RegisterRequest) = userEntityFactory.create(request).apply {
        userRepository.add(this)
        kafkaSender.send(KafkaConstants.USER_TOPIC, NewUserMsg(this.uuid))
    }
}