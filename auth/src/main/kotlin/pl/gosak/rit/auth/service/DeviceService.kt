/*
 * Copyright (c) 2018 This file is subject to the terms and conditions defined in file 'LICENSE.txt' which is part of this source code package.
 */

package pl.gosak.rit.auth.service

import pl.gosak.rit.auth.model.DeviceEntity
import pl.gosak.rit.auth.security.User
import java.util.*

interface DeviceService {
    /**
     * <resourceSecret, refreshSecret>
     */
    fun generateSecrets(userUuid: UUID, deviceEntity: DeviceEntity): Pair<String, String>

    fun logout(uuid: UUID): Boolean

    fun logoutAllDevices(userUuid: UUID): Boolean
}