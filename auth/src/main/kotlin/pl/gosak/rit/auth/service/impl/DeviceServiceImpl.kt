/*
 * Copyright (c) 2018 This file is subject to the terms and conditions defined in file 'LICENSE.txt' which is part of this source code package.
 */

package pl.gosak.rit.auth.service.impl

import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import pl.gosak.rit.auth.model.DeviceEntity
import pl.gosak.rit.auth.repository.DeviceRepository
import pl.gosak.rit.auth.security.generateSecret
import pl.gosak.rit.auth.service.DeviceService
import pl.gosak.rit.microbase.kafka.KafkaConstants.DEVICE_TOPIC
import pl.gosak.rit.microbase.kafka.message.device.DeviceLoggedMsg
import pl.gosak.rit.microbase.kafka.message.device.DeviceLoggedOutMsg
import pl.gosak.rit.microbase.kafka.message.device.UserDevicesLoggedOutMsg
import pl.gosak.rit.microbase.kafka.service.KafkaSender
import java.util.*

@Service
class DeviceServiceImpl(private val deviceRepository: DeviceRepository,
                        private val kafkaSender: KafkaSender) : DeviceService {
    @Transactional
    override fun generateSecrets(userUuid: UUID, deviceEntity: DeviceEntity) =
            deviceEntity.run {
                refreshSecret = generateSecret()
                resourceSecret = generateSecret()
                deviceRepository.update(this)
                kafkaSender.send(DEVICE_TOPIC, DeviceLoggedMsg(userUuid, deviceEntity.uuid, refreshSecret, resourceSecret))
                Pair(resourceSecret, refreshSecret)
            }

    @Transactional
    override fun logoutAllDevices(userUuid: UUID): Boolean {
        SecurityContextHolder.clearContext()
        kafkaSender.send(DEVICE_TOPIC, UserDevicesLoggedOutMsg(userUuid))
        return deviceRepository.delete(userUuid)
    }

    @Transactional
    override fun logout(uuid: UUID): Boolean {
        SecurityContextHolder.clearContext()
        kafkaSender.send(DEVICE_TOPIC, DeviceLoggedOutMsg(uuid))
        return deviceRepository.delete(deviceRepository.find(uuid))
    }
}