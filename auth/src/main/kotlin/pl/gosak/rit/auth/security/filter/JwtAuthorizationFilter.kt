/*
 * Copyright (c) 2018. This file is subject to the terms and conditions defined in file 'LICENSE.txt' which is part of this source code package.
 */

package pl.gosak.rit.auth.security.filter

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import org.springframework.http.HttpStatus
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter
import pl.gosak.rit.auth.repository.DeviceRepository
import pl.gosak.rit.auth.repository.UserRepository
import pl.gosak.rit.auth.security.User
import pl.gosak.rit.lib.SecurityConstants.AUTHORIZATION_HEADER
import pl.gosak.rit.lib.SecurityConstants.Roles.ROLE_REFRESH_TOKEN
import pl.gosak.rit.lib.SecurityConstants.TOKEN_PREFIX
import pl.gosak.rit.microbase.security.JwtTokenBody
import java.util.*
import javax.crypto.spec.SecretKeySpec
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

class JwtAuthorizationFilter(authManager: AuthenticationManager,
                             private val deviceRepository: DeviceRepository,
                             private val userRepository: UserRepository,
                             private val base64Decoder: Base64.Decoder) : BasicAuthenticationFilter(authManager) {
    override fun doFilterInternal(request: HttpServletRequest, response: HttpServletResponse, chain: FilterChain) {
        val header = request.getHeader(AUTHORIZATION_HEADER).takeIf { it != null && it.startsWith(TOKEN_PREFIX) }
        if (header == null) {
            chain.doFilter(request, response)
            return
        }

        try {
            SecurityContextHolder.getContext().authentication = getAuthentication(header)
            chain.doFilter(request, response)
        } catch (ex: Exception) { // need every exception log
            logger.error(ex)
            response.status = HttpStatus.UNAUTHORIZED.value()
            SecurityContextHolder.clearContext()
        }
    }

    private fun getAuthentication(header: String): UsernamePasswordAuthenticationToken {
        val token = header.replace(TOKEN_PREFIX, "")
        val untrustedTokenBody = untrustedJwtTokenBody(token) // it's only way to get proper secret using in parsing
        val device = deviceRepository.find(untrustedTokenBody.deviceUuid)
        val signingKey = if (untrustedTokenBody.roles.contains(ROLE_REFRESH_TOKEN)) device.refreshSecret else device.resourceSecret

        Jwts.parser() // if there wasn't any exceptions then parsed token is trusted
                .setSigningKey(SecretKeySpec(signingKey.toByteArray(), SignatureAlgorithm.HS512.jcaName))
                .parseClaimsJws(token)

        val enabled = userRepository.find(untrustedTokenBody.userUuid).enabled
        val roles = untrustedTokenBody.roles.map { SimpleGrantedAuthority(it) }
        val user = User(untrustedTokenBody.userUuid, untrustedTokenBody.deviceUuid, enabled, roles)
        return UsernamePasswordAuthenticationToken(user, null, roles)
    }

    private fun untrustedJwtTokenBody(jwtToken: String): JwtTokenBody {
        val payload = jwtToken.split('.')[1]
        return jacksonObjectMapper().readValue(base64Decoder.decode(payload), JwtTokenBody::class.java)
    }
}