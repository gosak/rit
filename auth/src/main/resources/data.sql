INSERT INTO role (id, name) VALUES
(1, 'ROLE_USER'),
(2, 'ROLE_RECOMME_ADMIN');

INSERT INTO privilege (id, name) VALUES
(1, 'ADD_PRODUCT_PRIVILEGE'),
(2, 'RATE_PRODUCT_PRIVILEGE'),
(3, 'CLEAR_RECOMME_DATABASE_PRIVILEGE'),
(4, 'DELETE_RECOMME_ITEM');

INSERT INTO role_privilege (role_id, privilege_id) VALUES
(1, 1),
(1, 2),
(2, 3),
(2, 4);




