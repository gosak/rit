package pl.gosak.rit.search.service

import pl.gosak.rit.lib.subject.dto.SubjectPreviewDto

interface FullSubjectService {
    fun search(name: String, count: Int, from: Int): List<SubjectPreviewDto>
}