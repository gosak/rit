package pl.gosak.rit.search.database.repository

import pl.gosak.rit.search.database.model.Subject
import java.util.*

interface SubjectRepository {
    fun persist(subject: Subject): Subject

    fun find(uuid: UUID): Subject

    fun update(subject: Subject): Subject
}