package pl.gosak.rit.search.database.service

import pl.gosak.rit.search.database.model.Subject
import java.util.*

interface SubjectService {
    fun create(subject: Subject): Subject

    fun load(uuid: UUID): Subject

    fun setPrimaryPhotoUuid(subjectUuid: UUID, primaryPhotoUuid: UUID, photoDominantColor: String, childSafe: Boolean)

    fun updateRate(subjectUuid: UUID, rate: Double)
}