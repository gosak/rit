package pl.gosak.rit.search.database.model

import pl.gosak.rit.microbase.util.DatabaseConstants.SCHEMA
import pl.gosak.rit.search.database.model.Subject.Names.TABLE
import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = TABLE, schema = SCHEMA)
class Subject(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = ID)
        val id: Long,

        @Column(name = UUID)
        val uuid: UUID,

        @Column(name = RATE)
        var rate: Double,

        @Column(name = DB_CHILD_SAFE, nullable = true)
        var childSafe: Boolean?,

        @Column(name = DB_PHOTO_UUID, nullable = true)
        var photoUuid: UUID?,

        @Column(name = DB_PHOTO_DOMINANT_COLOR, nullable = true)
        var photoDominantColor: String?) {
    constructor(uuid: UUID, rate: Double) : this(0, uuid, rate, null, null, null)

    companion object Names {
        const val TABLE = "subject"
        const val ID = "id"
        const val UUID = "uuid"
        const val RATE = "rate"
        const val CHILD_SAFE = "childSafe"
        const val DB_CHILD_SAFE = "child_safe"
        const val PHOTO_UUID = "photoUuid"
        const val DB_PHOTO_UUID = "photo_uuid"
        const val PHOTO_DOMINANT_COLOR = "photoDominantColor"
        const val DB_PHOTO_DOMINANT_COLOR = "photo_dominant_color"
    }
}