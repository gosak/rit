package pl.gosak.rit.search.elasticsearch.service

import pl.gosak.rit.search.elasticsearch.model.SearchSubject

interface SubjectSearchService {
    fun findByNameAndDescription(name: String, count: Int, from: Int): List<SearchSubject>
}