package pl.gosak.rit.search.service.impl

import org.springframework.stereotype.Service
import pl.gosak.rit.lib.subject.dto.SubjectPreviewDto
import pl.gosak.rit.search.database.service.SubjectService
import pl.gosak.rit.search.elasticsearch.service.SubjectSearchService
import pl.gosak.rit.search.service.FullSubjectService


@Service
class FullSubjectServiceImpl(private val searchService: SubjectSearchService,
                             private val databaseService: SubjectService) : FullSubjectService {
    override fun search(name: String, count: Int, from: Int) =
            searchService.findByNameAndDescription(name, count, from).map {
                Pair(it, databaseService.load(it.uuid))
            }.filter {
                it.second.photoUuid != null && it.second.photoDominantColor != null && it.second.childSafe != null
            }.map {
                it.second.run {
                    SubjectPreviewDto(uuid = uuid,
                            name = it.first.name,
                            description = it.first.description,
                            manufacturer = it.first.manufacturer,
                            rate = rate,
                            photo = photoUuid!!,
                            photoDominantColor = photoDominantColor!!,
                            childSafe = childSafe!!)
                }
            }
}
