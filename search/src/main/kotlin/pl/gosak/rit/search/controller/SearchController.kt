package pl.gosak.rit.search.controller

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestHeader
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import pl.gosak.rit.lib.SecurityConstants.AUTHORIZATION_HEADER
import pl.gosak.rit.lib.search.SearchEndpoints.Params.COUNT
import pl.gosak.rit.lib.search.SearchEndpoints.Params.FROM
import pl.gosak.rit.lib.search.SearchEndpoints.Params.QUERY
import pl.gosak.rit.lib.search.SearchEndpoints.SEARCH_SUBJECTS
import pl.gosak.rit.lib.search.dto.SearchSubjectsResponse
import pl.gosak.rit.microbase.util.responseEntity
import pl.gosak.rit.search.service.FullSubjectService

@RestController
class SearchController(private val fullSubjectService: FullSubjectService) {
    @GetMapping(SEARCH_SUBJECTS)
    fun searchSubjects(@RequestHeader(AUTHORIZATION_HEADER) jwt: String,
                       @RequestParam(FROM) from: Int,
                       @RequestParam(COUNT) count: Int,
                       @RequestParam(QUERY) query: String): ResponseEntity<SearchSubjectsResponse> {
        return HttpStatus.OK.responseEntity(SearchSubjectsResponse(fullSubjectService.search(query, count, from)))
    }
}