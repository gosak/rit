package pl.gosak.rit.search.elasticsearch.service

import pl.gosak.rit.search.elasticsearch.model.SearchSubject

interface SubjectIndexService {
    fun create(searchSubject: SearchSubject): String
}