package pl.gosak.rit.search.kafka.impl

import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Profile
import org.springframework.kafka.annotation.KafkaHandler
import org.springframework.kafka.annotation.KafkaListener
import org.springframework.messaging.handler.annotation.Payload
import org.springframework.stereotype.Component
import pl.gosak.rit.microbase.kafka.KafkaConstants.PHOTO_TOPIC
import pl.gosak.rit.microbase.kafka.KafkaConstants.PHOTO_TOPIC_SEARCH_GROUP
import pl.gosak.rit.microbase.kafka.listener.PhotoTopicListener
import pl.gosak.rit.microbase.kafka.message.photo.NewPhotoMsg
import pl.gosak.rit.microbase.kafka.message.photo.NewPrimaryPhotoMsg
import pl.gosak.rit.search.database.service.SubjectService


@Component
@Profile("local", "docker")
@KafkaListener(topics = [PHOTO_TOPIC], groupId = PHOTO_TOPIC_SEARCH_GROUP)
class SearchPhotoTopicListenerImpl(private val subjectService: SubjectService) : PhotoTopicListener {
    private val logger = LoggerFactory.getLogger(SearchPhotoTopicListenerImpl::class.java)

    @KafkaHandler
    override fun handle(@Payload msg: NewPrimaryPhotoMsg) {
        logger.info("KAFKA_SEARCH received NewPrimaryPhotoMsg: {}", msg)
        subjectService.setPrimaryPhotoUuid(msg.subjectUuid, msg.photoUuid, msg.photoDominantColor, msg.childSafe)
    }

    @KafkaHandler
    override fun handle(@Payload msg: NewPhotoMsg) {
        logger.info("KAFKA_SEARCH ignore NewPhotoMsg: {}", msg)
    }
}