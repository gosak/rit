package pl.gosak.rit.search.elasticsearch.model

import pl.gosak.rit.microbase.kafka.message.subject.NewSubjectMsg
import java.util.*

data class SearchSubject(
        var uuid: UUID = UUID.randomUUID(),
        var name: String = "",
        var description: String = "",
        var manufacturer: String = "") {
    companion object {
        const val INDEX = "subject-index"
        const val TYPE = "subject"
        const val NAME = "name"
        const val DESCRIPTION = "description"

        fun from(newSubjectMsg: NewSubjectMsg) = newSubjectMsg.run { SearchSubject(subjectUuid, name, description, manufacturer) }
    }
}