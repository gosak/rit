package pl.gosak.rit.search.elasticsearch

import org.springframework.boot.context.properties.ConfigurationProperties

@ConfigurationProperties("elasticsearch")
class ElasticSearchProperties(var host: String = "",
                              var port: Int = 2137)