package pl.gosak.rit.search.kafka

import pl.gosak.rit.microbase.kafka.message.subject.NewSubjectMsg
import pl.gosak.rit.microbase.kafka.message.subject.NewSubjectRateMsg

interface KafkaSubjectListener {
    fun handle(msg: NewSubjectMsg)

    fun handle(msg: NewSubjectRateMsg)
}