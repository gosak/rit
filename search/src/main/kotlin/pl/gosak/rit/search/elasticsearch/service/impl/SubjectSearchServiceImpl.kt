package pl.gosak.rit.search.elasticsearch.service.impl

import com.fasterxml.jackson.databind.ObjectMapper
import org.elasticsearch.action.search.SearchRequest
import org.elasticsearch.action.search.SearchResponse
import org.elasticsearch.client.RequestOptions
import org.elasticsearch.client.RestHighLevelClient
import org.elasticsearch.index.query.Operator
import org.elasticsearch.index.query.QueryBuilders
import org.elasticsearch.search.builder.SearchSourceBuilder
import org.springframework.stereotype.Service
import pl.gosak.rit.search.elasticsearch.model.SearchSubject
import pl.gosak.rit.search.elasticsearch.service.SubjectSearchService


@Service
class SubjectSearchServiceImpl(private val restHighLevelClient: RestHighLevelClient,
                               private val jacksonObjectMapper: ObjectMapper) : SubjectSearchService {
    override fun findByNameAndDescription(name: String, count: Int, from: Int): List<SearchSubject> {
        return SearchRequest().apply {
            indices(SearchSubject.INDEX)
            types(SearchSubject.TYPE)
        }.let {
            SearchSourceBuilder().apply {
                val matchQueryBuilder = QueryBuilders
                        .multiMatchQuery(name, SearchSubject.NAME, SearchSubject.DESCRIPTION)
                        .operator(Operator.OR)
                        .analyzer("english")
                query(matchQueryBuilder)
                size(count)
                from(from)
                it.source(this)
            }
            getSearchResult(restHighLevelClient.search(it, RequestOptions.DEFAULT))
        }
    }

    private fun getSearchResult(response: SearchResponse) = response.hits.hits.run {
        map { jacksonObjectMapper.convertValue(it.sourceAsMap, SearchSubject::class.java) }
    }
}