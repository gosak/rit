package pl.gosak.rit.search.elasticsearch.service.impl

import com.fasterxml.jackson.databind.ObjectMapper
import org.elasticsearch.action.index.IndexRequest
import org.elasticsearch.client.RequestOptions
import org.elasticsearch.client.RestHighLevelClient
import org.springframework.stereotype.Service
import pl.gosak.rit.search.elasticsearch.model.SearchSubject
import pl.gosak.rit.search.elasticsearch.model.SearchSubject.Companion.INDEX
import pl.gosak.rit.search.elasticsearch.model.SearchSubject.Companion.TYPE
import pl.gosak.rit.search.elasticsearch.service.SubjectIndexService


@Service
class SubjectIndexServiceImpl(private val restHighLevelClient: RestHighLevelClient,
                              private val jacksonObjectMapper: ObjectMapper) : SubjectIndexService {
    override fun create(searchSubject: SearchSubject): String {
        val documentMapper = jacksonObjectMapper.convertValue(searchSubject, Map::class.java)
        val indexRequest = IndexRequest(INDEX, TYPE, searchSubject.uuid.toString()).source(documentMapper)
        return restHighLevelClient.index(indexRequest, RequestOptions.DEFAULT).result.name
    }
}