package pl.gosak.rit.search.elasticsearch

import org.apache.http.HttpHost
import org.elasticsearch.client.RestClient
import org.elasticsearch.client.RestHighLevelClient
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration


@Configuration
@EnableConfigurationProperties(ElasticSearchProperties::class)
class ElasticSearchConfig(private val elasticSearchProperties: ElasticSearchProperties) {
    @Bean(destroyMethod = "close")
    fun client(): RestHighLevelClient {
        //TODO embedded elasticsearch in standalone profile
        //TODO create basic indexes
        return RestHighLevelClient(RestClient.builder(HttpHost(elasticSearchProperties.host, elasticSearchProperties.port)))
    }
}