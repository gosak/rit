package pl.gosak.rit.search.kafka.impl

import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Profile
import org.springframework.kafka.annotation.KafkaHandler
import org.springframework.kafka.annotation.KafkaListener
import org.springframework.messaging.handler.annotation.Payload
import org.springframework.stereotype.Component
import pl.gosak.rit.microbase.kafka.KafkaConstants.SUBJECT_TOPIC
import pl.gosak.rit.microbase.kafka.KafkaConstants.SUBJECT_TOPIC_SEARCH_GROUP
import pl.gosak.rit.microbase.kafka.message.subject.NewSubjectMsg
import pl.gosak.rit.microbase.kafka.message.subject.NewSubjectRateMsg
import pl.gosak.rit.search.database.model.Subject
import pl.gosak.rit.search.database.service.SubjectService
import pl.gosak.rit.search.elasticsearch.model.SearchSubject
import pl.gosak.rit.search.elasticsearch.service.SubjectIndexService
import pl.gosak.rit.search.kafka.KafkaSubjectListener


@Component
@Profile("local", "docker")
@KafkaListener(topics = [SUBJECT_TOPIC], groupId = SUBJECT_TOPIC_SEARCH_GROUP)
class KafkaSubjectListenerImpl(private val subjectIndexService: SubjectIndexService,
                               private val subjectService: SubjectService) : KafkaSubjectListener {
    private val logger = LoggerFactory.getLogger(KafkaSubjectListenerImpl::class.java)

    @KafkaHandler
    override fun handle(@Payload msg: NewSubjectMsg) {
        logger.info("KAFKA_SEARCH received NewSubjectMsg with subject uuid: ${msg.subjectUuid}")
        subjectIndexService.create(SearchSubject.from(msg))
        subjectService.create(Subject(msg.subjectUuid, msg.rate))
    }

    @KafkaHandler
    override fun handle(@Payload msg: NewSubjectRateMsg) {
        logger.info("KAFKA_SEARCH: received NewSubjectRateMsg with uuid: {}", msg.uuid)
        subjectService.updateRate(msg.uuid, msg.rate)
    }
}