package pl.gosak.rit.search.database.repository.impl

import org.springframework.stereotype.Repository
import pl.gosak.rit.microbase.util.findBy
import pl.gosak.rit.search.database.model.Subject
import pl.gosak.rit.search.database.repository.SubjectRepository
import java.util.*
import javax.persistence.EntityManager
import javax.persistence.EntityNotFoundException
import javax.persistence.PersistenceContext


@Repository
class SubjectRepositoryImpl : SubjectRepository {
    @PersistenceContext
    private lateinit var entityManager: EntityManager

    override fun persist(subject: Subject) = subject.apply {
        entityManager.persist(this)
    }

    override fun find(uuid: UUID): Subject = entityManager.findBy(Subject.UUID, uuid)
            ?: throw EntityNotFoundException("Subject with uuid=$uuid not found")

    override fun update(subject: Subject): Subject = entityManager.merge(subject)
}