package pl.gosak.rit.search

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.Import
import pl.gosak.rit.microbase.database.DatabaseModule
import pl.gosak.rit.microbase.eureka.EurekaModule
import pl.gosak.rit.microbase.kafka.KafkaModule
import pl.gosak.rit.microbase.security.WebSecurityModule
import pl.gosak.rit.microbase.swagger.SwaggerModule
import pl.gosak.rit.microbase.util.RitUtilsModule

@SpringBootApplication
@Import(RitUtilsModule::class,
        SwaggerModule::class,
        WebSecurityModule::class,
        DatabaseModule::class,
        KafkaModule::class,
        EurekaModule::class)
class RitSearchApplication {
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            SpringApplication.run(RitSearchApplication::class.java, *args)
        }
    }
}