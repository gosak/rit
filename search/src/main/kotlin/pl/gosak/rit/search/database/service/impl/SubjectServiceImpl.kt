package pl.gosak.rit.search.database.service.impl

import org.springframework.stereotype.Service
import pl.gosak.rit.search.database.model.Subject
import pl.gosak.rit.search.database.repository.SubjectRepository
import pl.gosak.rit.search.database.service.SubjectService
import java.util.*
import javax.transaction.Transactional

@Service
class SubjectServiceImpl(private val subjectRepository: SubjectRepository) : SubjectService {
    @Transactional
    override fun create(subject: Subject) = subjectRepository.persist(subject)

    override fun load(uuid: UUID) = subjectRepository.find(uuid)

    @Transactional
    override fun setPrimaryPhotoUuid(subjectUuid: UUID, primaryPhotoUuid: UUID, photoDominantColor: String, childSafe: Boolean) {
        subjectRepository.find(subjectUuid).also {
            it.photoUuid = primaryPhotoUuid
            it.photoDominantColor = photoDominantColor
            it.childSafe = childSafe
            subjectRepository.update(it)
        }
    }

    @Transactional
    override fun updateRate(subjectUuid: UUID, rate: Double) {
        subjectRepository.find(subjectUuid).also {
            it.rate = rate
            subjectRepository.update(it)
        }
    }
}