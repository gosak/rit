package pl.gosak.rit.subject.service

import pl.gosak.rit.subject.model.Subject
import pl.gosak.rit.lib.subject.dto.CreateSubjectRequest
import pl.gosak.rit.microbase.security.User
import java.util.*

interface SubjectService {
    fun create(user: User, createSubjectRequest: CreateSubjectRequest): Subject

    fun load(uuid: UUID): Subject
}