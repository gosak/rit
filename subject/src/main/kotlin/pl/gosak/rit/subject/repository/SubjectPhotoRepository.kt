package pl.gosak.rit.subject.repository

import pl.gosak.rit.subject.model.SubjectPhoto


interface SubjectPhotoRepository {
    fun persist(subjectPhoto: SubjectPhoto): SubjectPhoto

    fun load(subjectPhotoKey: SubjectPhoto.Key): SubjectPhoto

    fun exists(subjectId: Long): Boolean
}