package pl.gosak.rit.subject.repository

import pl.gosak.rit.subject.model.SubjectRate

interface SubjectRateRepository {
    fun load(key: SubjectRate.Key): SubjectRate?

    fun update(subjectRate: SubjectRate): SubjectRate

    fun create(subjectRate: SubjectRate): SubjectRate
}