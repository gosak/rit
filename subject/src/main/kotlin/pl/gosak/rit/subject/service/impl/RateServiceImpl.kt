package pl.gosak.rit.subject.service.impl

import org.springframework.stereotype.Service
import pl.gosak.rit.microbase.kafka.KafkaConstants.SUBJECT_TOPIC
import pl.gosak.rit.microbase.kafka.message.subject.NewSubjectRateMsg
import pl.gosak.rit.microbase.kafka.service.KafkaSender
import pl.gosak.rit.microbase.security.User
import pl.gosak.rit.subject.model.Subject
import pl.gosak.rit.subject.model.SubjectRate
import pl.gosak.rit.subject.repository.SubjectRepository
import pl.gosak.rit.subject.service.RateService
import pl.gosak.rit.subject.service.SubjectRateService
import java.util.*
import javax.transaction.Transactional

@Service
class RateServiceImpl(private val subjectRepository: SubjectRepository,
                      private val subjectRateService: SubjectRateService,
                      private val kafkaSender: KafkaSender) : RateService {
    @Transactional
    override fun rate(user: User, subjectUuid: UUID, rate: Double): Subject {
        val subject = subjectRepository.find(subjectUuid)
        val subjectReview = subjectRateService.load(SubjectRate.Key(user.userUuid, subject.id))

        if (subjectReview == null) {
            var rateSum = subject.rate
            rateSum *= subject.rates
            rateSum += rate
            subject.rate = rateSum / ++subject.rates
        } else {
            val oldUserReview = subjectReview.rate
            subjectReview.also {
                it.rate = rate
                subjectRateService.updateRate(subject.uuid, it)
            }
            var rateSum = subject.rate * subject.rates
            rateSum -= oldUserReview
            rateSum += rate
            subject.rate = rateSum / subject.rates
        }
        subjectRepository.update(subject)
        kafkaSender.send(SUBJECT_TOPIC, NewSubjectRateMsg(subject.uuid, user.userUuid, rate, subject.rate))
        return subject
    }
}