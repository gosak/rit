package pl.gosak.rit.subject.service.impl

import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import pl.gosak.rit.lib.subject.dto.CreateSubjectRequest
import pl.gosak.rit.microbase.kafka.KafkaConstants.SUBJECT_TOPIC
import pl.gosak.rit.microbase.kafka.message.subject.NewSubjectMsg
import pl.gosak.rit.microbase.kafka.service.KafkaSender
import pl.gosak.rit.microbase.security.User
import pl.gosak.rit.subject.model.Subject
import pl.gosak.rit.subject.model.SubjectRate
import pl.gosak.rit.subject.repository.SubjectRepository
import pl.gosak.rit.subject.repository.SubjectRateRepository
import pl.gosak.rit.subject.service.SubjectService
import java.util.*

@Service
class SubjectServiceImpl(private val subjectRepository: SubjectRepository,
                         private val subjectRateRepository: SubjectRateRepository,
                         private val kafkaSender: KafkaSender) : SubjectService {
    @Transactional
    override fun create(user: User, createSubjectRequest: CreateSubjectRequest) =
            subjectRepository.add(
                    Subject(name = createSubjectRequest.name,
                            description = createSubjectRequest.description,
                            manufacturer = createSubjectRequest.manufacturer,
                            creatorUuid = user.userUuid,
                            rate = createSubjectRequest.rate))
                    .apply {
                        val msg = NewSubjectMsg(uuid, manufacturer, name, description, rate, user.userUuid)
                        kafkaSender.send(SUBJECT_TOPIC, msg)
                        createSubjectRequest.rate?.also {
                            subjectRateRepository.create(SubjectRate(user.userUuid, id, it))
                        }
                    }

    override fun load(uuid: UUID) = subjectRepository.find(uuid)
}