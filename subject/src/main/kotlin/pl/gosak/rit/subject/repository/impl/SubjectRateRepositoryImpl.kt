package pl.gosak.rit.subject.repository.impl

import org.springframework.stereotype.Service
import pl.gosak.rit.subject.model.SubjectRate
import pl.gosak.rit.subject.repository.SubjectRateRepository
import javax.persistence.EntityManager
import javax.persistence.PersistenceContext


@Service
class SubjectRateRepositoryImpl : SubjectRateRepository {
    @PersistenceContext
    private lateinit var entityManager: EntityManager

    override fun create(subjectRate: SubjectRate) = subjectRate.apply {
        entityManager.persist(this)
    }

    override fun load(key: SubjectRate.Key): SubjectRate? = entityManager.find(SubjectRate::class.java, key)

    override fun update(subjectRate: SubjectRate) =
            subjectRate.apply {
                entityManager.merge(this)
            }
}