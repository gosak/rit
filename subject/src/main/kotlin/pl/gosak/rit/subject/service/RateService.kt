package pl.gosak.rit.subject.service

import pl.gosak.rit.microbase.security.User
import pl.gosak.rit.subject.model.Subject
import java.util.*

interface RateService {
    fun rate(user: User, subjectUuid: UUID, rate: Double): Subject
}