package pl.gosak.rit.subject.controller

import org.modelmapper.ModelMapper
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.core.Authentication
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestHeader
import org.springframework.web.bind.annotation.RestController
import pl.gosak.rit.lib.SecurityConstants.AUTHORIZATION_HEADER
import pl.gosak.rit.lib.subject.SubjectEndpoints.CREATE
import pl.gosak.rit.lib.subject.SubjectEndpoints.Paths.SUBJECT_UUID
import pl.gosak.rit.lib.subject.SubjectEndpoints.SUBJECT
import pl.gosak.rit.lib.subject.dto.CreateSubjectRequest
import pl.gosak.rit.lib.subject.dto.CreateSubjectResponse
import pl.gosak.rit.lib.subject.dto.SubjectDto
import pl.gosak.rit.microbase.security.User
import pl.gosak.rit.microbase.util.responseEntity
import pl.gosak.rit.subject.service.SubjectService
import java.util.*


@RestController
class SubjectController(private val subjectService: SubjectService,
                        private val modelMapper: ModelMapper) {
    @PostMapping(CREATE)
    fun createEntity(authentication: Authentication,
                     @RequestBody(required = true) createSubjectRequest: CreateSubjectRequest,
                     @RequestHeader(AUTHORIZATION_HEADER) jwt: String): ResponseEntity<CreateSubjectResponse> {
        val subject = subjectService.create(authentication.principal as User, createSubjectRequest)
        return HttpStatus.CREATED.responseEntity(modelMapper.map(subject, CreateSubjectResponse::class.java))
    }

    @GetMapping(SUBJECT)
    fun getEntity(authentication: Authentication,
                  @PathVariable(SUBJECT_UUID) subjectUuid: UUID,
                  @RequestHeader(AUTHORIZATION_HEADER) jwt: String): ResponseEntity<SubjectDto> {
        val subject = subjectService.load(subjectUuid)
        return HttpStatus.OK.responseEntity(modelMapper.map(subject, SubjectDto::class.java))
    }
}