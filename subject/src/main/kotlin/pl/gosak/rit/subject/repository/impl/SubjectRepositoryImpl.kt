/*
 * Copyright (c) 2018 This file is subject to the terms and conditions defined in file 'LICENSE.txt' which is part of this source code package.
 */

package pl.gosak.rit.subject.repository.impl

import org.springframework.stereotype.Repository
import pl.gosak.rit.microbase.util.findBy
import pl.gosak.rit.subject.model.Subject
import pl.gosak.rit.subject.repository.SubjectRepository
import java.util.*
import javax.persistence.EntityManager
import javax.persistence.EntityNotFoundException
import javax.persistence.PersistenceContext

@Repository
class SubjectRepositoryImpl : SubjectRepository {
    @PersistenceContext
    lateinit var entityManager: EntityManager

    override fun add(subject: Subject) = subject.apply {
        entityManager.persist(this)
    }

    override fun find(uuid: UUID): Subject = entityManager.findBy(Subject.DB_UUID, uuid)
            ?: throw EntityNotFoundException("Subject with uuid=$uuid not found")

    override fun update(subject: Subject): Subject = entityManager.merge(subject)
}