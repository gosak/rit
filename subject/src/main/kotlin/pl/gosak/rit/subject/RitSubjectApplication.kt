package pl.gosak.rit.subject

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.Import
import pl.gosak.rit.microbase.database.DatabaseModule
import pl.gosak.rit.microbase.eureka.EurekaModule
import pl.gosak.rit.microbase.kafka.KafkaModule
import pl.gosak.rit.microbase.security.WebSecurityModule
import pl.gosak.rit.microbase.swagger.SwaggerModule
import pl.gosak.rit.microbase.util.RitUtilsModule

@SpringBootApplication
@Import(RitUtilsModule::class,
        SwaggerModule::class,
        WebSecurityModule::class,
        DatabaseModule::class,
        KafkaModule::class,
        EurekaModule::class)
class RitSubjectApplication {
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            SpringApplication.run(RitSubjectApplication::class.java, *args)
        }
    }
}