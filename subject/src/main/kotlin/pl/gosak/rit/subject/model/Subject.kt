package pl.gosak.rit.subject.model

import pl.gosak.rit.microbase.util.DatabaseConstants.SCHEMA
import pl.gosak.rit.subject.model.Subject.Names.TABLE
import pl.gosak.rit.subject.model.SubjectPhoto.Names.DB_SUBJECT_ID
import java.util.*
import javax.persistence.*


@Entity
@Table(name = TABLE, schema = SCHEMA)
class Subject(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = ID)
        val id: Long,

        @Column(name = DB_UUID, nullable = false)
        val uuid: UUID,

        @Column(name = NAME, nullable = false)
        var name: String,

        @Column(name = DESCRIPTION)
        var description: String,

        @Column(name = MANUFACTURER)
        var manufacturer: String,

        @Column(name = DB_CREATOR_UUID)
        val creatorUuid: UUID,

        @Column(name = RATE, nullable = true)
        var rate: Double,

        @Column(name = RATES, nullable = false)
        var rates: Long,

        @OneToMany(fetch = FetchType.LAZY, orphanRemoval = true)
        @JoinColumn(name = DB_SUBJECT_ID)
        var subjectPhotos: List<SubjectPhoto>) {
    constructor(name: String, description: String, manufacturer: String, creatorUuid: UUID, rate: Double?) :
            this(0, UUID.randomUUID(), name, description, manufacturer, creatorUuid, rate ?: 0.0, 1, LinkedList<SubjectPhoto>())

    companion object Names {
        const val TABLE = "subject"
        const val ID = "id"
        const val DB_UUID = "uuid"
        const val NAME = "name"
        const val DESCRIPTION = "description"
        const val CREATOR_UUID = "creatorUuid"
        const val DB_CREATOR_UUID = "creator_uuid"
        const val MANUFACTURER = "manufacturer"
        const val RATE = "rate"
        const val RATES = "rates"
    }
}