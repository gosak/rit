package pl.gosak.rit.subject.service.impl

import org.springframework.stereotype.Service
import pl.gosak.rit.subject.model.SubjectRate
import pl.gosak.rit.subject.repository.SubjectRateRepository
import pl.gosak.rit.subject.service.SubjectRateService
import java.util.*

@Service
class SubjectRateServiceImpl(private val subjectRateRepository: SubjectRateRepository) : SubjectRateService {
    override fun load(subjectRateKey: SubjectRate.Key) = subjectRateRepository.load(subjectRateKey)

    override fun rate(subjectUuid: UUID, subjectRate: SubjectRate) = subjectRate.apply {
        subjectRateRepository.create(subjectRate)
    }

    override fun updateRate(subjectUuid: UUID, subjectRate: SubjectRate) = subjectRate.apply {
        subjectRateRepository.update(subjectRate)
    }
}