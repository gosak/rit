package pl.gosak.rit.subject.controller

import org.modelmapper.ModelMapper
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.core.Authentication
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestHeader
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import pl.gosak.rit.lib.SecurityConstants.AUTHORIZATION_HEADER
import pl.gosak.rit.lib.subject.SubjectEndpoints.Params.USER_RATE
import pl.gosak.rit.lib.subject.SubjectEndpoints.Paths.SUBJECT_UUID
import pl.gosak.rit.lib.subject.SubjectEndpoints.RATE
import pl.gosak.rit.lib.subject.dto.RateSubjectResponse
import pl.gosak.rit.microbase.security.ritUser
import pl.gosak.rit.microbase.util.responseEntity
import pl.gosak.rit.subject.service.RateService
import java.util.*

@RestController
class RateController(val modelMapper: ModelMapper,
                     val rateService: RateService) {
    @PostMapping(RATE)
    fun rate(authentication: Authentication,
             @PathVariable(SUBJECT_UUID) subjectUuid: UUID,
             @RequestParam(USER_RATE) userRate: Double,
             @RequestHeader(AUTHORIZATION_HEADER) jwt: String): ResponseEntity<RateSubjectResponse> {
        val subject = rateService.rate(authentication.ritUser(), subjectUuid, userRate)
        return HttpStatus.ACCEPTED.responseEntity(modelMapper.map(subject, RateSubjectResponse::class.java))
    }
}
