package pl.gosak.rit.subject.service

import pl.gosak.rit.subject.model.SubjectRate
import java.util.*

interface SubjectRateService {
    fun load(subjectRateKey: SubjectRate.Key): SubjectRate?

    fun rate(subjectUuid: UUID, subjectRate: SubjectRate): SubjectRate

    fun updateRate(subjectUuid: UUID, subjectRate: SubjectRate): SubjectRate
}