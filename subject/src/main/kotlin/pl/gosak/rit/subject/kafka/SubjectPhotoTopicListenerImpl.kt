package pl.gosak.rit.subject.kafka

import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Profile
import org.springframework.kafka.annotation.KafkaHandler
import org.springframework.kafka.annotation.KafkaListener
import org.springframework.messaging.handler.annotation.Payload
import org.springframework.stereotype.Component
import pl.gosak.rit.microbase.kafka.KafkaConstants.PHOTO_TOPIC
import pl.gosak.rit.microbase.kafka.KafkaConstants.PHOTO_TOPIC_SUBJECT_GROUP
import pl.gosak.rit.microbase.kafka.listener.PhotoTopicListener
import pl.gosak.rit.microbase.kafka.message.photo.NewPhotoMsg
import pl.gosak.rit.microbase.kafka.message.photo.NewPrimaryPhotoMsg
import pl.gosak.rit.microbase.kafka.service.KafkaSender
import pl.gosak.rit.subject.model.SubjectPhoto
import pl.gosak.rit.subject.repository.SubjectPhotoRepository
import pl.gosak.rit.subject.repository.SubjectRepository
import javax.persistence.EntityNotFoundException
import javax.transaction.Transactional

@Component
@Profile("local", "docker")
@KafkaListener(topics = [PHOTO_TOPIC], groupId = PHOTO_TOPIC_SUBJECT_GROUP)
class SubjectPhotoTopicListenerImpl(private val subjectPhotoRepository: SubjectPhotoRepository,
                                    private val subjectRepository: SubjectRepository,
                                    private val kafkaSender: KafkaSender) : PhotoTopicListener {
    private val logger = LoggerFactory.getLogger(SubjectPhotoTopicListenerImpl::class.java)

    @Transactional
    @KafkaHandler
    override fun handle(@Payload msg: NewPhotoMsg) {
        logger.info("KAFKA_SUBJECT: received NewPhotoMsg: {}", msg)
        try {
            val subject = subjectRepository.find(msg.subjectUuid)
            val exists = subjectPhotoRepository.exists(subject.id)
            subjectPhotoRepository.persist(SubjectPhoto(subject.id, msg.uuid, msg.childSafe, !exists, msg.dominantColor))
            // first pushed photo is primary
            if (!exists) {
                kafkaSender.send(PHOTO_TOPIC, NewPrimaryPhotoMsg(subject.uuid, msg.uuid, msg.dominantColor, msg.childSafe))
            }
        } catch (ex: EntityNotFoundException) {
            logger.error(ex.message)
            logger.error("Subject with uuid {} not exists, possible lose of data consistency. Service that send NewPhotoMsg has cached subject that not " +
                    "exists in his owner(subject) database", msg.subjectUuid)
        }
    }

    @KafkaHandler
    override fun handle(@Payload msg: NewPrimaryPhotoMsg) {
        logger.info("KAFKA_SUBJECT: ignore NewPrimaryPhotoMsg: {}", msg)
    }
}