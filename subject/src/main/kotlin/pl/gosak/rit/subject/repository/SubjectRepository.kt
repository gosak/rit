/*
 * Copyright (c) 2018 This file is subject to the terms and conditions defined in file 'LICENSE.txt' which is part of this source code package.
 */

package pl.gosak.rit.subject.repository

import pl.gosak.rit.subject.model.Subject
import java.util.*

interface SubjectRepository {
    fun add(subject: Subject): Subject

    fun update(subject: Subject): Subject

    fun find(uuid: UUID): Subject
}