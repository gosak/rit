package pl.gosak.rit.subject.model

import pl.gosak.rit.subject.model.SubjectPhoto.Names.TABLE
import java.io.Serializable
import java.util.*
import javax.persistence.*

@Entity
@Table(name = TABLE)
@IdClass(SubjectPhoto.Key::class)
class SubjectPhoto
constructor(
        @Id
        @Column(name = DB_SUBJECT_ID, nullable = false)
        val subjectId: Long,

        @Id
        @Column(name = DB_PHOTO_UUID, nullable = false)
        val photoUuid: UUID,

        @Column(name = DB_CHILD_SAFE, nullable = false)
        var childSafe: Boolean,

        @Column(name = DB_PRIMARY, nullable = false)
        var primary: Boolean,

        @Column(name = DB_DOMINANT_COLOR, nullable = false)
        var dominantColor: String
) {
    data class Key(val subjectId: Long = -1, val photoUuid: UUID = UUID(0, 0)) : Serializable

    companion object Names {
        const val TABLE = "subject_photo"
        const val SUBJECT_ID = "subjectId"
        const val DB_SUBJECT_ID = "subject_id"
        const val PHOTO_UUID = "photoUuid"
        const val DB_PHOTO_UUID = "photo_uuid"
        const val CHILD_SAFE = "childSafe"
        const val DB_CHILD_SAFE = "child_safe"
        const val PRIMARY = "primary"
        const val DB_PRIMARY = "is_primary"
        const val DB_DOMINANT_COLOR = "dominant_color"
        const val DOMINANT_COLOR = "dominantColor"
    }
}