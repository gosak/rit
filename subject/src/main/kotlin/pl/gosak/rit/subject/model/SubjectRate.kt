package pl.gosak.rit.subject.model

import pl.gosak.rit.microbase.util.DatabaseConstants.SCHEMA
import pl.gosak.rit.subject.model.SubjectRate.Names.TABLE
import java.io.Serializable
import java.util.*
import javax.persistence.*

@Entity
@Table(name = TABLE, schema = SCHEMA)
@IdClass(SubjectRate.Key::class)
class SubjectRate(
        @Id
        @Column(name = DB_USER_UUID)
        val userUuid: UUID,

        @Id
        @Column(name = DB_SUBJECT_ID)
        val subjectId: Long,

        @Column(name = RATE, nullable = false)
        var rate: Double
) {
    data class Key(val userUuid: UUID = UUID(0, 0), val subjectId: Long = -1) : Serializable

    companion object Names {
        const val TABLE = "subject_rate"
        const val USER_UUID = "userUuid"
        const val DB_USER_UUID = "user_uuid"
        const val SUBJECT_ID = "subjectId"
        const val DB_SUBJECT_ID = "subject_id"
        const val RATE = "rate"
    }
}