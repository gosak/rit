package pl.gosak.rit.subject.repository.impl

import org.springframework.stereotype.Repository
import pl.gosak.rit.microbase.util.findBy
import pl.gosak.rit.subject.model.SubjectPhoto
import pl.gosak.rit.subject.repository.SubjectPhotoRepository
import javax.persistence.EntityManager
import javax.persistence.EntityNotFoundException
import javax.persistence.PersistenceContext

@Repository
class SubjectPhotoRepositoryImpl : SubjectPhotoRepository {
    @PersistenceContext
    private lateinit var entityManager: EntityManager

    override fun persist(subjectPhoto: SubjectPhoto) = subjectPhoto.apply {
        entityManager.persist(this)
    }

    override fun exists(subjectId: Long): Boolean = loadBySubjectId(subjectId) != null

    private fun loadBySubjectId(subjectId: Long): SubjectPhoto? = entityManager.findBy(SubjectPhoto.SUBJECT_ID, subjectId)

    override fun load(subjectPhotoKey: SubjectPhoto.Key): SubjectPhoto = entityManager.find(SubjectPhoto::class.java, subjectPhotoKey)
            ?: throw EntityNotFoundException("subjectPhoto with key: $subjectPhotoKey not found")
}