package pl.gosak.rit.client.ui.main.exhibit

import pl.gosak.rit.client.ui.main.exhibit.model.SubjectViewHolder
import pl.gosak.rit.lib.subject.dto.SubjectPreviewDto

interface SubjectOnClickListener {
    fun onItemClick(viewHolder: SubjectViewHolder, subject: SubjectPreviewDto)
}