package pl.gosak.rit.client.ui.main.account

import androidx.databinding.Bindable
import pl.gosak.rit.client.BR
import pl.gosak.rit.client.utils.ui.ObservableViewModel

class AccountViewModel(private var childSafe: Boolean = false,
                       private var userId: String = "",
                       private var deviceId: String = "") : ObservableViewModel() {
    @Bindable
    fun isChildSafe() = childSafe

    @Bindable
    fun getUserId() = userId

    @Bindable
    fun getDeviceId() = deviceId

    fun setChildSafe(value: Boolean) {
        childSafe = value
        notifyPropertyChanged(BR.childSafe)
    }

    fun setUserId(value: String) {
        userId = value
        notifyPropertyChanged(BR.userId)
    }

    fun setDeviceId(value: String) {
        deviceId = value
        notifyPropertyChanged(BR.deviceId)
    }
}