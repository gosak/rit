package pl.gosak.rit.client.core

import android.content.SharedPreferences
import pl.gosak.rit.lib.auth.dto.AuthenticationResponse

class JwtTokenManagerImpl(private val sharedPreferences: SharedPreferences) : JwtTokenManager {
    @Volatile
    private var revokeSuspended = false

    override fun logged(authenticationResponse: AuthenticationResponse) {
        sharedPreferences.edit().apply {
            putString(JWT_RESOURCE_TOKEN, authenticationResponse.resourceToken)
            putString(JWT_REFRESH_TOKEN, authenticationResponse.refreshToken)
        }.apply()
    }

    override fun suspendRevoking() {
        revokeSuspended = true
    }

    override fun openRevoking() {
        revokeSuspended = false
    }

    override fun isRevokingSuspended() = revokeSuspended

    override fun currentRefreshToken() = sharedPreferences.getString(JWT_REFRESH_TOKEN, "") ?: ""

    override fun currentResourceToken() = sharedPreferences.getString(JWT_RESOURCE_TOKEN, "") ?: ""

    companion object {
        const val JWT_RESOURCE_TOKEN = "jwtResourceToken"
        const val JWT_REFRESH_TOKEN = "jwtRefreshToken"
    }
}