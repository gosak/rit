package pl.gosak.rit.client.core.network

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import okhttp3.Authenticator
import okhttp3.Request
import okhttp3.Response
import okhttp3.Route
import pl.gosak.rit.client.core.JwtTokenManager
import pl.gosak.rit.lib.SecurityConstants.AUTHORIZATION_HEADER
import pl.gosak.rit.lib.SecurityConstants.TOKEN_PREFIX
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create
import java.util.concurrent.TimeUnit

class JwtTokenAuthenticator(private val jwtTokenManager: JwtTokenManager) : Authenticator {
    private val okHttpClient = getUnsafeOkHttpClientBuilder()
            .connectTimeout(15L, TimeUnit.SECONDS)
            .readTimeout(15L, TimeUnit.SECONDS)
            .addInterceptor(FixedHttpLogger("REVOKE_HTTP"))
            .build()

    private val retrofit = Retrofit.Builder()
            .baseUrl(URL)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(CoroutineCallAdapterFactory.invoke())
            .build()

    private val revokeApi = retrofit.create<RevokeApi>()

    override fun authenticate(route: Route?, response: Response): Request? {
        if (response.code() == 401 && !jwtTokenManager.isRevokingSuspended()) {
            jwtTokenManager.suspendRevoking()
            val newTokens = revokeApi.revoke("$TOKEN_PREFIX${jwtTokenManager.currentRefreshToken()}").execute()
            newTokens.body()?.let { jwtTokenManager.logged(it) }
            jwtTokenManager.openRevoking()

            return if (newTokens.code() == 201) {
                response.request().newBuilder()
                        .header(AUTHORIZATION_HEADER, "$TOKEN_PREFIX${jwtTokenManager.currentResourceToken()}")
                        .build()
            } else {
                null
            }
        }
        return null
    }
}