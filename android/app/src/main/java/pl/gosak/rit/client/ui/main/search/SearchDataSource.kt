package pl.gosak.rit.client.ui.main.search

import androidx.paging.PageKeyedDataSource
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import pl.gosak.rit.client.core.api.search.service.SearchService
import pl.gosak.rit.client.ui.main.account.service.ChildSafeService
import pl.gosak.rit.lib.subject.dto.SubjectPreviewDto


class SearchDataSource(var query: String,
                       private val searchService: SearchService,
                       private val childSafeService: ChildSafeService) : PageKeyedDataSource<Int, SubjectPreviewDto>() {
    override fun loadInitial(params: LoadInitialParams<Int>, callback: LoadInitialCallback<Int, SubjectPreviewDto>) {
        if (query.isBlank()) {
            return
        }
        GlobalScope.launch {
            runCatching {
                val response = searchService.search(0, PAGE_SIZE, query).await()
                val body = response.body()
                if (response.isSuccessful && body != null) {
                    var subjects = body.subjects
                    if (!childSafeService.isNotSafeContentEnabled()) {
                        subjects = subjects.filter { it.childSafe }
                    }
                    callback.onResult(subjects, null, 1)
                }
            }.onFailure {
                it.printStackTrace()
            }
        }
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, SubjectPreviewDto>) {
        val currentPage = params.key
        val nextPage = currentPage + 1

        GlobalScope.launch {
            runCatching {
                val response = searchService.search(currentPage, params.requestedLoadSize, query).await()
                val body = response.body()
                if (response.isSuccessful && body != null) {
                    var subjects = body.subjects
                    if (!childSafeService.isNotSafeContentEnabled()) {
                        subjects = subjects.filter { it.childSafe }
                    }
                    callback.onResult(subjects, nextPage)
                }
            }.onFailure {
                it.printStackTrace()
            }
        }
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, SubjectPreviewDto>) {
    }

    companion object {
        const val PAGE_SIZE = 6
    }
}