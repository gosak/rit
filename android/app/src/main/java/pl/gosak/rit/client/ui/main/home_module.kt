package pl.gosak.rit.client.ui.main

import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module
import pl.gosak.rit.client.ui.main.account.service.AccountIdsResolver
import pl.gosak.rit.client.ui.main.account.service.AccountIdsResolverImpl
import pl.gosak.rit.client.ui.main.account.service.ChildSafeService
import pl.gosak.rit.client.ui.main.account.service.ChildSafeServiceImpl
import pl.gosak.rit.client.ui.main.exhibit.model.ExhibitViewModel
import pl.gosak.rit.client.ui.main.search.SearchViewModel

val homeModule = module {
    viewModel { ExhibitViewModel(get(), get()) }
    viewModel { SearchViewModel(get(), get()) }
    single { ChildSafeServiceImpl(get()) as ChildSafeService }
    single { AccountIdsResolverImpl(get()) as AccountIdsResolver }
}