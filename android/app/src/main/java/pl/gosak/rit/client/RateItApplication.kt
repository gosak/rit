package pl.gosak.rit.client

import android.app.Application
import org.koin.android.ext.android.startKoin

class RateItApplication : Application() {
    init {
        RateItApplication.instance = this
    }

    override fun onCreate() {
        super.onCreate()

        startKoin(this, ritAppModules)
    }

    companion object {
        lateinit var instance: RateItApplication
    }
}