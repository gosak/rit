package pl.gosak.rit.client.core.api.subject

import org.koin.dsl.module.module
import pl.gosak.rit.client.core.api.subject.service.SubjectService
import pl.gosak.rit.client.core.api.subject.service.SubjectServiceImpl
import pl.gosak.rit.client.core.network.URL
import pl.gosak.rit.client.core.network.createWebService

val subjectApiModule = module {
    single { createWebService<SubjectApi>(get(), URL) }
    single { SubjectServiceImpl(get()) as SubjectService }
}