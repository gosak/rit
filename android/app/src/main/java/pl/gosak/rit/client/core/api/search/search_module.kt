package pl.gosak.rit.client.core.api.search

import org.koin.dsl.module.module
import pl.gosak.rit.client.core.api.search.service.SearchService
import pl.gosak.rit.client.core.api.search.service.SearchServiceImpl
import pl.gosak.rit.client.core.network.URL
import pl.gosak.rit.client.core.network.createWebService

val searchApiModule = module {
    single { createWebService<SearchApi>(get(), URL) }
    single { SearchServiceImpl(get()) as SearchService }
}