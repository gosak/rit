package pl.gosak.rit.client.ui.start

import android.os.Bundle
import androidx.core.view.ViewCompat
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_start.*
import kotlinx.android.synthetic.main.fragment_start.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import org.koin.android.ext.android.inject
import pl.gosak.rit.client.R
import pl.gosak.rit.client.core.JwtTokenManager
import pl.gosak.rit.client.core.api.auth.AuthApi
import pl.gosak.rit.client.ui.main.HomeActivity
import pl.gosak.rit.client.ui.start.login.LoginFragment
import pl.gosak.rit.client.ui.start.register.RegisterFragment
import pl.gosak.rit.client.utils.launchActivity
import pl.gosak.rit.client.utils.ui.AbstractFragmentActivity
import pl.gosak.rit.lib.auth.dto.AuthenticationResponse
import java.io.IOException
import java.net.InetAddress
import java.util.logging.Level
import java.util.logging.LogRecord
import java.util.logging.Logger


class StartActivity : AbstractFragmentActivity(), StartExecutor {
    private val jwtTokenManager: JwtTokenManager by inject()
    private val authApi: AuthApi by inject()

    private val logger = Logger.getLogger("AUTO_LOGIN")

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_start)

        tryAutoLogin()

        val startFragment = supportFragmentManager.findFragmentByTag(START_TAG) ?: StartFragment.instance()
        swapFragment(startFragment, START_TAG)
                .setPrimaryNavigationFragment(startFragment)
                .commit()

    }

    private fun log(msg: String) {
        val logRecord = LogRecord(Level.INFO, msg)
        logRecord.loggerName = "AUTO_LOGIN"
        logger.log(logRecord)
    }

    override fun tryAutoLogin() {
        log("trying auto login")
        if (isInternetAvailable() && jwtTokenManager.currentRefreshToken().isNotBlank()) {
            log("internet available")
            log("current refreshToken: ${jwtTokenManager.currentRefreshToken()}")
            if (testApi().isNotBlank()) {
                goToHome()
            } else {
                Snackbar.make(root, R.string.error_auto_login_fail, Snackbar.LENGTH_SHORT)
            }
        }
    }

    private fun isInternetAvailable() =
            runBlocking {
                withContext(Dispatchers.IO) {
                    try {
                        !InetAddress.getByName("google.com").equals("")
                    } catch (e: IOException) {
                        log("internet not available")
                        false
                    }
                }
            }

    private fun testApi() =
            runBlocking {
                var token = jwtTokenManager.currentResourceToken()
                runCatching {
                    withContext(Dispatchers.IO) {
                        val response = authApi.test().await()
                        if (response.code() != 200) {
                            log("api tested, need to login again")
                            token = ""
                            jwtTokenManager.logged(AuthenticationResponse())
                        } else {
                            log("auto login successful")
                        }
                    }
                }.onFailure {
                    log("could not test api")
                    jwtTokenManager.logged(AuthenticationResponse())
                }
                token
            }


    override fun goToLogin() {
        swapFragment(supportFragmentManager.findFragmentByTag(LOGIN_TAG) ?: LoginFragment.instance(), LOGIN_TAG)
                .addToBackStack(LOGIN_TAG)
                .addSharedElement(login, ViewCompat.getTransitionName(login)!!)
                .commit()
    }

    override fun goToRegister() {
        swapFragment(supportFragmentManager.findFragmentByTag(REGISTER_TAG) ?: RegisterFragment.instance(), REGISTER_TAG)
                .addToBackStack(REGISTER_TAG)
                .commit()
    }

    override fun goToHome() {
        launchActivity<HomeActivity>()
        finish()
    }

    private fun swapFragment(fragment: Fragment, tag: String) =
            supportFragmentManager
                    .beginTransaction()
                    .setCustomAnimations(R.anim.shade_in, R.anim.shade_out, R.anim.shade_in, R.anim.shade_out)
                    .replace(R.id.fragment_container, fragment, tag)

    companion object {
        const val START_TAG = "startTag"
        const val LOGIN_TAG = "loginTag"
        const val REGISTER_TAG = "registerTag"
    }
}