package pl.gosak.rit.client.core.api.auth

import org.koin.dsl.module.module
import pl.gosak.rit.client.core.api.auth.service.AuthService
import pl.gosak.rit.client.core.api.auth.service.AuthServiceImpl
import pl.gosak.rit.client.core.network.URL
import pl.gosak.rit.client.core.network.createWebService

val authApiModule = module {
    single { createWebService<AuthApi>(get(), URL) }
    single { AuthServiceImpl(get()) as AuthService }
}