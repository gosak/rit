package pl.gosak.rit.client.core.network

import pl.gosak.rit.lib.SecurityConstants.AUTHORIZATION_HEADER
import pl.gosak.rit.lib.auth.AuthEndpoints
import pl.gosak.rit.lib.auth.dto.AuthenticationResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Header

interface RevokeApi {
    @GET(AuthEndpoints.REVOKE)
    fun revoke(@Header(AUTHORIZATION_HEADER) jwtRefreshToken: String): Call<AuthenticationResponse>
}