package pl.gosak.rit.client.utils.validator.readable

import androidx.annotation.StringRes
import pl.gosak.rit.client.utils.validator.simple.Validator

abstract class AbstractReadableValidator
constructor(private val validator: Validator,
            @StringRes private val idRes: Int) : ReadableValidator {
    override fun validate(string: String) = if (validator.validate(string)) null else idRes
}