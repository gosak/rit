package pl.gosak.rit.client.core

import pl.gosak.rit.lib.auth.dto.AuthenticationResponse

interface JwtTokenManager {
    fun logged(authenticationResponse: AuthenticationResponse)

    fun suspendRevoking()

    fun openRevoking()

    fun isRevokingSuspended(): Boolean

    fun currentRefreshToken(): String

    fun currentResourceToken(): String
}