package pl.gosak.rit.client.core.api.auth.service

import kotlinx.coroutines.Deferred
import pl.gosak.rit.lib.auth.dto.AuthenticationResponse
import pl.gosak.rit.lib.auth.dto.RegisterResponse
import pl.gosak.rit.lib.auth.dto.TestResponse
import retrofit2.Response

interface AuthService {
    fun login(email: String, password: String): Deferred<Response<AuthenticationResponse>>

    fun register(name: String, email: String, password: String): Deferred<Response<RegisterResponse>>

    fun test(): Deferred<Response<TestResponse>>
}