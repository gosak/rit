package pl.gosak.rit.client.core.network

import okhttp3.Interceptor
import okhttp3.Response
import pl.gosak.rit.client.core.JwtTokenManager
import pl.gosak.rit.lib.SecurityConstants.AUTHORIZATION_HEADER
import pl.gosak.rit.lib.SecurityConstants.TOKEN_PREFIX

class JwtTokenHeaderInterceptor(private val jwtTokenManager: JwtTokenManager) : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        return chain.proceed(chain.request().newBuilder().addHeader(AUTHORIZATION_HEADER, "$TOKEN_PREFIX${jwtTokenManager.currentResourceToken()}").build())
    }
}