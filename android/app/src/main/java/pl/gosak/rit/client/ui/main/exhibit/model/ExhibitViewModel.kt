package pl.gosak.rit.client.ui.main.exhibit.model

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.paging.DataSource
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import pl.gosak.rit.client.core.api.recomme.service.RecommeService
import pl.gosak.rit.client.ui.main.account.service.ChildSafeService
import pl.gosak.rit.client.ui.main.exhibit.SubjectsDataSource
import pl.gosak.rit.client.ui.main.exhibit.SubjectsDataSource.Companion.SUBJECT_PAGE_SIZE
import pl.gosak.rit.lib.subject.dto.SubjectPreviewDto

class ExhibitViewModel(private val recommeService: RecommeService,
                       private val childSafeService: ChildSafeService) : ViewModel() {
    private var subjectsLiveData: LiveData<PagedList<SubjectPreviewDto>>

    init {
        val config = PagedList.Config.Builder()
                .setPageSize(SUBJECT_PAGE_SIZE)
                .setPrefetchDistance(SUBJECT_PAGE_SIZE)
                .setEnablePlaceholders(true)
                .build()
        subjectsLiveData = initializedPagedListBuilder(config).build()
    }

    fun getSubjects(): LiveData<PagedList<SubjectPreviewDto>> = subjectsLiveData

    private fun initializedPagedListBuilder(config: PagedList.Config):
            LivePagedListBuilder<String, SubjectPreviewDto> {

        val dataSourceFactory = object : DataSource.Factory<String, SubjectPreviewDto>() {
            override fun create(): DataSource<String, SubjectPreviewDto> {
                return SubjectsDataSource(recommeService, childSafeService)
            }
        }
        return LivePagedListBuilder<String, SubjectPreviewDto>(dataSourceFactory, config)
    }
}