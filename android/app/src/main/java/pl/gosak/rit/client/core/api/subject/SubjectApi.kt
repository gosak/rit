package pl.gosak.rit.client.core.api.subject

import kotlinx.coroutines.Deferred
import pl.gosak.rit.lib.subject.SubjectEndpoints.Params.USER_RATE
import pl.gosak.rit.lib.subject.SubjectEndpoints.Paths.SUBJECT_UUID
import pl.gosak.rit.lib.subject.SubjectEndpoints.RATE
import pl.gosak.rit.lib.subject.SubjectEndpoints.SUBJECT
import pl.gosak.rit.lib.subject.dto.RateSubjectResponse
import pl.gosak.rit.lib.subject.dto.SubjectDto
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path
import retrofit2.http.Query
import java.util.*

interface SubjectApi {
    @GET(SUBJECT)
    fun get(@Path(SUBJECT_UUID) subjectUuid: UUID): Deferred<Response<SubjectDto>>

    @POST(RATE)
    fun rate(@Path(SUBJECT_UUID) subjectUuid: UUID, @Query(USER_RATE) userRate: Double): Deferred<Response<RateSubjectResponse>>
}