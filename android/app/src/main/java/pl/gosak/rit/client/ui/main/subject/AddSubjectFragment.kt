package pl.gosak.rit.client.ui.main.subject

import android.os.Bundle
import pl.gosak.rit.client.utils.ui.AbstractFragment

class AddSubjectFragment : AbstractFragment() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    companion object {
        fun instance() = AddSubjectFragment()
    }
}