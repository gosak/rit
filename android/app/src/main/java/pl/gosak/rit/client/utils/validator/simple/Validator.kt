package pl.gosak.rit.client.utils.validator.simple

interface Validator {
    fun validate(string: String): Boolean
}