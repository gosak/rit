package pl.gosak.rit.client.utils.validator.readable

import androidx.annotation.StringRes

open class GroupReadableValidator(vararg val validators: ReadableValidator) : ReadableValidator {
    @StringRes
    override fun validate(string: String): Int? {
        validators.forEach {
            val resId = it.validate(string)
            if (resId != null) {
                return resId
            }
        }
        return null
    }
}