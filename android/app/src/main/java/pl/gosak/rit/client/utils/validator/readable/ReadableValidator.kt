package pl.gosak.rit.client.utils.validator.readable

import androidx.annotation.StringRes

interface ReadableValidator {
    @StringRes
    fun validate(string: String): Int?
}