package pl.gosak.rit.client.core.api.recomme.service

import kotlinx.coroutines.Deferred
import pl.gosak.rit.client.core.api.recomme.RecommeApi
import pl.gosak.rit.lib.recomme.dto.RecommendedSubjects
import retrofit2.Response
import java.util.*

class RecommeServiceImpl(private val recommeApi: RecommeApi) : RecommeService {
    override fun recommend(count: Int): Deferred<Response<RecommendedSubjects>> {
        return recommeApi.recommend(count)
    }

    override fun viewSubject(subjectUuid: UUID): Deferred<Response<String>> {
        return recommeApi.viewSubject(subjectUuid)
    }
}