package  pl.gosak.rit.client.ui.main

import android.os.Bundle
import android.view.MenuItem
import androidx.fragment.app.Fragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_home.*
import pl.gosak.rit.client.R
import pl.gosak.rit.client.ui.main.account.AccountFragment
import pl.gosak.rit.client.ui.main.exhibit.ExhibitFragment
import pl.gosak.rit.client.ui.main.exhibit.subject.SubjectFragment
import pl.gosak.rit.client.ui.main.ratings.RatingsFragment
import pl.gosak.rit.client.ui.main.search.SearchFragment
import pl.gosak.rit.client.ui.main.subject.AddSubjectFragment
import pl.gosak.rit.client.utils.ui.AbstractFragmentActivity
import pl.gosak.rit.lib.subject.dto.SubjectPreviewDto


class HomeActivity : AbstractFragmentActivity(), HomeNavigator {
    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme_NoActionBar)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        navigation.setOnNavigationItemSelectedListener(object : BottomNavigationView.OnNavigationItemSelectedListener {
            override fun onNavigationItemSelected(item: MenuItem): Boolean {
                when (item.itemId) {
                    R.id.home -> exhibit()
                    R.id.search -> search()
                    R.id.addSubject -> addSubject()
                    R.id.ratings -> ratings()
                    R.id.account -> account()
                }
                return true
            }
        })

        swapFragment(supportFragmentManager.findFragmentByTag(EXHIBIT_TAG) ?: ExhibitFragment.instance(), EXHIBIT_TAG).commit()
    }

    override fun exhibit() {
        swapFragment(supportFragmentManager.findFragmentByTag(EXHIBIT_TAG) ?: ExhibitFragment.instance(), EXHIBIT_TAG)
                .addToBackStack(EXHIBIT_TAG)
                .commit()
    }

    override fun account() {
        swapFragment(supportFragmentManager.findFragmentByTag(ACCOUNT_TAG) ?: AccountFragment.instance(), ACCOUNT_TAG)
                .addToBackStack(ACCOUNT_TAG)
                .commit()
    }

    override fun addSubject() {
        swapFragment(supportFragmentManager.findFragmentByTag(ADD_SUBJECT_TAG) ?: AddSubjectFragment.instance(), ADD_SUBJECT_TAG)
                .addToBackStack(ADD_SUBJECT_TAG)
                .commit()
    }

    override fun ratings() {
        swapFragment(supportFragmentManager.findFragmentByTag(RATINGS_TAG) ?: RatingsFragment.instance(), RATINGS_TAG)
                .addToBackStack(RATINGS_TAG)
                .commit()
    }

    override fun search() {
        swapFragment(supportFragmentManager.findFragmentByTag(SEARCH_TAG) ?: SearchFragment.instance(), SEARCH_TAG)
                .addToBackStack(SEARCH_TAG)
                .commit()
    }

    override fun showSubject(subject: SubjectPreviewDto, tag: String) {
        val fragment = SubjectFragment.instance(subject)
        supportFragmentManager
                .beginTransaction()
                .addToBackStack(tag)
                .setCustomAnimations(R.anim.slide_in_right, 0, 0, R.anim.slide_out_right)
                .add(R.id.fragment_container, fragment, tag)
                .commit()
    }

    private fun swapFragment(fragment: Fragment, tag: String) =
            supportFragmentManager
                    .beginTransaction()
                    .setCustomAnimations(R.anim.shade_in, 0, 0, 0)
                    .replace(R.id.fragment_container, fragment, tag)

    override fun onBackPressed() {
        super.onBackPressed()
        //TODO change navigation bar current checked item
    }

    companion object {
        const val ACCOUNT_TAG = "accountTag"
        const val ADD_SUBJECT_TAG = "addSubjectTag"
        const val EXHIBIT_TAG = "exhibitTag"
        const val SEARCH_TAG = "searchTag"
        const val RATINGS_TAG = "ratingsTag"
    }
}