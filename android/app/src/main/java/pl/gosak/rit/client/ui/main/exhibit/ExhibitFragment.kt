package pl.gosak.rit.client.ui.main.exhibit

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import kotlinx.android.synthetic.main.fragment_exhibit.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel
import pl.gosak.rit.client.R
import pl.gosak.rit.client.core.api.recomme.service.RecommeService
import pl.gosak.rit.client.databinding.FragmentExhibitBinding
import pl.gosak.rit.client.ui.main.HomeNavigator
import pl.gosak.rit.client.ui.main.exhibit.model.ExhibitViewModel
import pl.gosak.rit.client.ui.main.exhibit.model.SubjectViewHolder
import pl.gosak.rit.client.utils.ui.AbstractFragment
import pl.gosak.rit.lib.subject.dto.SubjectPreviewDto


class ExhibitFragment : AbstractFragment() {
    private val recommeService: RecommeService by inject()
    private val exhibitViewModel: ExhibitViewModel by viewModel()
    private lateinit var homeNavigator: HomeNavigator

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        homeNavigator = (activity as HomeNavigator)
        val view = DataBindingUtil.inflate<FragmentExhibitBinding>(layoutInflater, R.layout.fragment_exhibit, container, false).also {
            it.navigator = homeNavigator
        }.root
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val subjectsAdapter = SubjectsAdapter(object : SubjectOnClickListener {
            override fun onItemClick(viewHolder: SubjectViewHolder, subject: SubjectPreviewDto) {
                showSubject(subject)
            }
        })

        exhibitViewModel.getSubjects().observe(this, Observer {
            subjectsAdapter.submitList(it)
        })

        products.apply {
            adapter = subjectsAdapter
            layoutManager = GridLayoutManager(context, 2)
        }
    }

    private fun showSubject(subject: SubjectPreviewDto) {
        launch {
            withContext(Dispatchers.IO) {
                recommeService.viewSubject(subject.uuid).await()
            }
        }
        homeNavigator.showSubject(subject, SUBJECT_TAG)
    }

    companion object {
        const val SUBJECT_TAG = "exhibitSubjectTag"

        fun instance() = ExhibitFragment()
    }
}