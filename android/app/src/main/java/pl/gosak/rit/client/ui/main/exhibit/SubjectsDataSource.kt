package pl.gosak.rit.client.ui.main.exhibit

import androidx.paging.PageKeyedDataSource
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import pl.gosak.rit.client.core.api.recomme.service.RecommeService
import pl.gosak.rit.client.ui.main.account.service.ChildSafeService
import pl.gosak.rit.lib.subject.dto.SubjectPreviewDto


class SubjectsDataSource(private val recommeService: RecommeService,
                         private val childSafeService: ChildSafeService) : PageKeyedDataSource<String, SubjectPreviewDto>() {
    override fun loadInitial(params: LoadInitialParams<String>, callback: LoadInitialCallback<String, SubjectPreviewDto>) {
        GlobalScope.launch {
            runCatching {
                val response = recommeService.recommend(SUBJECT_PAGE_SIZE).await()
                val body = response.body()
                if (response.isSuccessful && body != null) {
                    var subjects = listOf(*body.subjects)
                    if (!childSafeService.isNotSafeContentEnabled()) {
                        subjects = subjects.filter { it.childSafe }
                    }
                    callback.onResult(subjects, "", "")
                }
            }.onFailure {
                it.printStackTrace()
            }
        }
    }

    override fun loadAfter(params: LoadParams<String>, callback: LoadCallback<String, SubjectPreviewDto>) {
        GlobalScope.launch {
            runCatching {
                val response = recommeService.recommend(SUBJECT_PAGE_SIZE).await()
                val body = response.body()
                if (response.isSuccessful && body != null) {
                    var subjects = listOf(*body.subjects)
                    if (!childSafeService.isNotSafeContentEnabled()) {
                        subjects = subjects.filter { it.childSafe }
                    }
                    callback.onResult(subjects, "")
                }
            }.onFailure {
                it.printStackTrace()
            }
        }
    }

    override fun loadBefore(params: LoadParams<String>, callback: LoadCallback<String, SubjectPreviewDto>) {
    }

    companion object {
        const val SUBJECT_PAGE_SIZE = 6
    }
}