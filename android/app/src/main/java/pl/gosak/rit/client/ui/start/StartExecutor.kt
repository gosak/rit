package pl.gosak.rit.client.ui.start

interface StartExecutor {
    fun tryAutoLogin()

    fun goToLogin()

    fun goToRegister()

    fun goToHome()
}