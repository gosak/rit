package pl.gosak.rit.client.ui.main.account

interface AccountExecutor {
    fun logout()

    fun toggleChildSafe()
}