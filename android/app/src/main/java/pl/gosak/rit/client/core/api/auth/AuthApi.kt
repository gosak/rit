package pl.gosak.rit.client.core.api.auth

import kotlinx.coroutines.Deferred
import pl.gosak.rit.lib.auth.AuthEndpoints.LOGIN
import pl.gosak.rit.lib.auth.AuthEndpoints.REGISTER
import pl.gosak.rit.lib.auth.AuthEndpoints.TEST
import pl.gosak.rit.lib.auth.dto.*
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface AuthApi {
    @POST(LOGIN)
    fun login(@Body loginRequest: AuthenticationRequest): Deferred<Response<AuthenticationResponse>>

    @POST(REGISTER)
    fun register(@Body registerRequest: RegisterRequest): Deferred<Response<RegisterResponse>>

    @GET(TEST)
    fun test(): Deferred<Response<TestResponse>>
}