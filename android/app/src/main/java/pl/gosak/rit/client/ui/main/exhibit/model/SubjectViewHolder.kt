package pl.gosak.rit.client.ui.main.exhibit.model

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.subject_card.view.*

class SubjectViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    val photoView = view.photo
    val rate = view.rate
}