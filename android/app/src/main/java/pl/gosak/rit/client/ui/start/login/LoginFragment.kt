package pl.gosak.rit.client.ui.start.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.transition.TransitionInflater
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_login.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.koin.android.ext.android.inject
import pl.gosak.rit.client.R
import pl.gosak.rit.client.core.JwtTokenManager
import pl.gosak.rit.client.core.api.auth.service.AuthService
import pl.gosak.rit.client.databinding.FragmentLoginBinding
import pl.gosak.rit.client.ui.start.StartExecutor
import pl.gosak.rit.client.utils.ui.AbstractFragment


class LoginFragment : AbstractFragment(), LoginExecutor {
    private val authService: AuthService by inject()
    private val jwtTokenManager: JwtTokenManager by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sharedElementEnterTransition = TransitionInflater.from(context).inflateTransition(android.R.transition.move)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val loginModel = ViewModelProviders.of(activity!!).get(LoginModel::class.java)

        val view = DataBindingUtil.inflate<FragmentLoginBinding>(layoutInflater, R.layout.fragment_login, container, false).also {
            it.model = loginModel
            it.executor = this
        }.root

        return view
    }

    override fun login(loginModel: LoginModel) {
        loginModel.setLoading(true)
        launch {
            runCatching {
                withContext(Dispatchers.IO) {
                    val response = authService.login(loginModel.getName(), loginModel.getPassword()).await()
                    when (response.code()) {
                        403 -> Snackbar.make(layout, R.string.auth_email_or_password_invalid, Snackbar.LENGTH_LONG).show()
                        201 -> response.body()?.let {
                            jwtTokenManager.logged(it)
                            loginModel.setLoading(false)
                            (activity as StartExecutor).goToHome()
                        }
                        else -> Snackbar.make(layout, R.string.error_unexpected, Snackbar.LENGTH_LONG).show()
                    }
                    loginModel.setLoading(false)
                }
            }.onFailure {
                loginModel.setLoading(false)
                Snackbar.make(layout, R.string.api_503, Snackbar.LENGTH_LONG).show()
            }
        }
    }

    companion object {
        fun instance() = LoginFragment()
    }
}