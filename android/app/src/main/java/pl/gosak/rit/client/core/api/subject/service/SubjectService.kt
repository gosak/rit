package pl.gosak.rit.client.core.api.subject.service

import kotlinx.coroutines.Deferred
import pl.gosak.rit.lib.subject.dto.RateSubjectResponse
import pl.gosak.rit.lib.subject.dto.SubjectDto
import retrofit2.Response
import java.util.*

interface SubjectService {
    fun get(uuid: UUID): Deferred<Response<SubjectDto>>

    fun rate(uuid: UUID, value: Double): Deferred<Response<RateSubjectResponse>>
}