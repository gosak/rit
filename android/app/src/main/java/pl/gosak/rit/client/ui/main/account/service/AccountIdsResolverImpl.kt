package pl.gosak.rit.client.ui.main.account.service

import android.util.Base64
import org.json.JSONObject
import pl.gosak.rit.client.core.JwtTokenManager
import pl.gosak.rit.lib.JwtTokenCustomFields.DEVICE_UUID
import pl.gosak.rit.lib.JwtTokenCustomFields.USER_UUID

class AccountIdsResolverImpl(private val jwtTokenManager: JwtTokenManager) : AccountIdsResolver {
    override fun userId() =
            JSONObject(String(Base64.decode(jwtTokenManager.currentResourceToken().split('.')[1], Base64.DEFAULT))).getString(USER_UUID)


    override fun deviceId() =
            JSONObject(String(Base64.decode(jwtTokenManager.currentResourceToken().split('.')[1], Base64.DEFAULT))).getString(DEVICE_UUID)

}