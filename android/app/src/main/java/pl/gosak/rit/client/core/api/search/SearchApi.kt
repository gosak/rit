package pl.gosak.rit.client.core.api.search

import kotlinx.coroutines.Deferred
import pl.gosak.rit.lib.search.SearchEndpoints.Params.COUNT
import pl.gosak.rit.lib.search.SearchEndpoints.Params.FROM
import pl.gosak.rit.lib.search.SearchEndpoints.Params.QUERY
import pl.gosak.rit.lib.search.SearchEndpoints.SEARCH_SUBJECTS
import pl.gosak.rit.lib.search.dto.SearchSubjectsResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface SearchApi {
    @GET(SEARCH_SUBJECTS)
    fun search(@Query(FROM) from: Int, @Query(COUNT) count: Int, @Query(QUERY) string: String): Deferred<Response<SearchSubjectsResponse>>
}