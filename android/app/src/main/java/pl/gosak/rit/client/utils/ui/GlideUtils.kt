package pl.gosak.rit.client.utils.ui

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.widget.ImageView
import androidx.core.graphics.ColorUtils
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import pl.gosak.rit.client.core.network.URL
import pl.gosak.rit.client.utils.ui.GlideConstants.DOMINANT_COLOR_LIGHTNESS
import java.util.*


fun ImageView.fitCenterPhoto(photoUuid: UUID, placeholderColor: Int) {
    Glide.with(context)
            .load("${URL}photo/$photoUuid")
            .apply(RequestOptions()
                    .placeholder(ColorDrawable(placeholderColor))
                    .fitCenter())
            .into(this)
}

fun resolveDominantColor(dominantColor: String) =
        runCatching {
            Color.parseColor(dominantColor)
        }.getOrDefault(Color.DKGRAY).let {
            FloatArray(3).apply {
                ColorUtils.RGBToHSL(Color.red(it), Color.green(it), Color.blue(it), this)
            }.apply {
                this[2] = DOMINANT_COLOR_LIGHTNESS
            }.run {
                ColorUtils.HSLToColor(this)
            }
        }

object GlideConstants {
    const val DOMINANT_COLOR_LIGHTNESS = 0.6f
}