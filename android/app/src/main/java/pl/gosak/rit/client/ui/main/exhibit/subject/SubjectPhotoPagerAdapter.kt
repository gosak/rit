package pl.gosak.rit.client.ui.main.exhibit.subject

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import pl.gosak.rit.client.R
import pl.gosak.rit.client.utils.ui.fitCenterPhoto
import pl.gosak.rit.lib.subject.dto.SubjectPhotoDto
import java.util.*

class SubjectPhotoPagerAdapter(context: Context, firstSubjectPhotoDto: SubjectPhotoDto) : PagerAdapter() {
    private val layoutInflater: LayoutInflater = LayoutInflater.from(context)
    private val photos = mutableListOf(firstSubjectPhotoDto)

    override fun isViewFromObject(view: View, any: Any): Boolean {
        return view == any
    }

    override fun getCount() = photos.size

    fun setItems(vararg items: SubjectPhotoDto) {
        photos.clear()
        photos.addAll(items)
        notifyDataSetChanged()
    }

    override fun instantiateItem(view: ViewGroup, position: Int): Any {
        val photo = layoutInflater.inflate(R.layout.image_view, view, false) as ImageView
        photos[position].let {
            val photoUuid = it.uuid.toString()
            if (photoUuid.isNotBlank()) {
                photo.fitCenterPhoto(UUID.fromString(photoUuid), Color.WHITE)
            }
        }
        view.addView(photo)
        return photo
    }

    override fun destroyItem(container: View, position: Int, item: Any) {
        (container as ViewPager).removeView(item as View)
    }
}