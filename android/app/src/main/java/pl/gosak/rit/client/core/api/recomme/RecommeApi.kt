package pl.gosak.rit.client.core.api.recomme

import kotlinx.coroutines.Deferred
import pl.gosak.rit.lib.recomme.RecommeEndpoints.Params.COUNT
import pl.gosak.rit.lib.recomme.RecommeEndpoints.Paths.SUBJECT_UUID
import pl.gosak.rit.lib.recomme.RecommeEndpoints.RECOMMEND_SUBJECTS_TO_USER
import pl.gosak.rit.lib.recomme.RecommeEndpoints.VIEW_SUBJECT
import pl.gosak.rit.lib.recomme.dto.RecommendedSubjects
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path
import retrofit2.http.Query
import java.util.*

interface RecommeApi {
    @GET(RECOMMEND_SUBJECTS_TO_USER)
    fun recommend(@Query(COUNT) count: Int): Deferred<Response<RecommendedSubjects>>

    @POST(VIEW_SUBJECT)
    fun viewSubject(@Path(SUBJECT_UUID) uuid: UUID): Deferred<Response<String>>
}