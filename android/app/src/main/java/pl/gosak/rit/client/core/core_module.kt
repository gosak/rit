package pl.gosak.rit.client.core

import android.content.Context
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module.module

val coreModules = module {
    single { androidContext().getSharedPreferences("pl.gosak.rit.android", Context.MODE_PRIVATE) }
    single { JwtTokenManagerImpl(get()) as JwtTokenManager }
}