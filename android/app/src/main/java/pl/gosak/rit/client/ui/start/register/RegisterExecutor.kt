package pl.gosak.rit.client.ui.start.register

interface RegisterExecutor {
    fun register(registerModel: RegisterModel)
}