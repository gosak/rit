package pl.gosak.rit.client.ui.start.login

interface LoginExecutor {
    fun login(loginModel: LoginModel)
}