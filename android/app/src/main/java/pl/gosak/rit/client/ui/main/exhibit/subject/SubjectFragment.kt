package pl.gosak.rit.client.ui.main.exhibit.subject

import android.content.res.ColorStateList
import android.graphics.PorterDuff
import android.os.Bundle
import android.view.*
import android.view.animation.AnimationUtils
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_subject.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.koin.android.ext.android.inject
import pl.gosak.rit.client.R
import pl.gosak.rit.client.core.api.subject.service.SubjectService
import pl.gosak.rit.client.ui.main.exhibit.subject.RateDialogFragment.Companion.RATE_DIALOG_FRAGMENT_TAG
import pl.gosak.rit.client.utils.ratingBarValueToRitRate
import pl.gosak.rit.client.utils.ui.AbstractFragment
import pl.gosak.rit.client.utils.ui.resolveDominantColor
import pl.gosak.rit.lib.subject.dto.SubjectDto
import pl.gosak.rit.lib.subject.dto.SubjectPhotoDto
import pl.gosak.rit.lib.subject.dto.SubjectPreviewDto
import java.util.*


class SubjectFragment : AbstractFragment(), SubjectExecutor {
    private val subjectService: SubjectService by inject()

    private lateinit var photoUuid: String
    private lateinit var subjectUuid: String
    private lateinit var photoDominantColor: String
    private lateinit var subjectDescription: String
    private lateinit var subjectManufacturer: String
    private lateinit var subjectName: String
    private var subjectRate = -1.0

    private lateinit var subject: SubjectDto
    private lateinit var photoAdapter: SubjectPhotoPagerAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        photoUuid = arguments?.getString(PHOTO_UUID) ?: ""
        subjectUuid = arguments?.getString(SUBJECT_UUID) ?: ""
        photoDominantColor = arguments?.getString(PHOTO_DOMINANT_COLOR) ?: ""
        subjectDescription = arguments?.getString(SUBJECT_DESCRIPTION) ?: ""
        subjectManufacturer = arguments?.getString(SUBJECT_MANUFACTURER) ?: ""
        subjectName = arguments?.getString(SUBJECT_NAME) ?: ""
        subjectRate = arguments?.getDouble(SUBJECT_RATE) ?: -1.0
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(pl.gosak.rit.client.R.layout.fragment_subject, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        layout.setOnKeyListener(object : View.OnKeyListener {
            override fun onKey(v: View, keyCode: Int, event: KeyEvent): Boolean {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    // TODO exhbition fragment should take care about new subject rate
                }
                return true
            }
        })

        photoAdapter = SubjectPhotoPagerAdapter(context!!, SubjectPhotoDto(UUID.fromString(photoUuid), true, photoDominantColor))
        pager.adapter = photoAdapter

        loadSubject()

        val dominantColor = resolveDominantColor(photoDominantColor)

        val rate = (subjectRate.plus(1)).times(25)
        rateBar.progress = rate.toInt()
        rateBar.setOnTouchListener { _, motionEvent ->
            if (motionEvent.action == MotionEvent.ACTION_UP) {
                RateDialogFragment.newInstance(dominantColor).show(childFragmentManager, RATE_DIALOG_FRAGMENT_TAG)
            }
            true
        }

        rateBar.progressDrawable.setColorFilter(dominantColor, PorterDuff.Mode.SRC_ATOP)
        addPhoto.backgroundTintList = ColorStateList.valueOf(dominantColor)
        manufacturer.text = subjectManufacturer
        name.text = subjectName
    }

    private fun refreshRate() {
        val rate = subject.rate.plus(1).times(25)
        rateBar.progress = rate.toInt()
    }

    private fun animateRate(userRate: Double, previousRate: Double) {
        refreshRate()

        val animation = AnimationUtils.loadAnimation(context, if (userRate < previousRate) pl.gosak.rit.client.R.anim.shrink_grow else pl.gosak.rit.client.R.anim.grow_shrink)
        rateBar.startAnimation(animation)
    }

    override fun userRate(value: Double) {
        subjectRate = value
        val rate = ratingBarValueToRitRate(value)

        launch(Dispatchers.IO) {
            runCatching {
                val response = subjectService.rate(subject.uuid, rate).await()
                when (response.code()) {
                    202 -> {
                        val body = response.body()
                        subject.rate = body?.rate ?: 0.0
                        withContext(Dispatchers.Main) {
                            animateRate(rate, subject.rate)
                            userRating.text = resources.getString(R.string.subject_rate_your_rate, String.format("%.1f", value))
                        }
                    }
                    else -> throw Exception() //TODO do sth with that
                }
            }.onFailure {
                it.printStackTrace()
                withContext(Dispatchers.Main) { Snackbar.make(layout, R.string.error_subject_rating, Snackbar.LENGTH_SHORT).show() }
            }
        }
    }

    private fun loadSubject() {
        launch(Dispatchers.IO) {
            runCatching {
                val response = subjectService.get(UUID.fromString(subjectUuid)).await()
                when (response.code()) {
                    200 -> {
                        withContext(Dispatchers.Main) {
                            subject = response.body() ?: throw Exception()
                            refreshRate()
                            photoAdapter.setItems(*subject.subjectPhotos)
                            description.text = subject.description
                            manufacturer.text = subjectManufacturer
                        }
                    }
                    else -> throw Exception() //TODO do sth with that
                }
            }.onFailure {
                it.printStackTrace()
                withContext(Dispatchers.Main) { Snackbar.make(layout, R.string.error_photo_loading, Snackbar.LENGTH_SHORT).show() }
            }
        }
    }


    companion object {
        const val PHOTO_UUID = "photoUuid"
        const val SUBJECT_UUID = "subjectUuid"
        const val PHOTO_DOMINANT_COLOR = "photoDominantColor"
        const val SUBJECT_DESCRIPTION = "subjectDescription"
        const val SUBJECT_MANUFACTURER = "subjectManufacturer"
        const val SUBJECT_NAME = "subjectName"
        const val SUBJECT_RATE = "subjectRate"

        fun instance(subject: SubjectPreviewDto) = SubjectFragment().apply {
            arguments = Bundle().apply {
                putString(PHOTO_UUID, subject.photo.toString())
                putString(PHOTO_DOMINANT_COLOR, subject.photoDominantColor)
                putString(SUBJECT_UUID, subject.uuid.toString())
                putString(SUBJECT_DESCRIPTION, subject.description)
                putString(SUBJECT_MANUFACTURER, subject.manufacturer)
                putString(SUBJECT_NAME, subject.name)
                putDouble(SUBJECT_RATE, subject.rate)
            }
        }
    }
}