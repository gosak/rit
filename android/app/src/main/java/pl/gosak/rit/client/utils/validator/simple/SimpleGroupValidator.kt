package pl.gosak.rit.client.utils.validator.simple

open class SimpleGroupValidator(vararg validators: Validator) : Validator {
    private var validators = validators.asList()

    override fun validate(string: String) = validators.all { it.validate(string) }
}