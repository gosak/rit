package pl.gosak.rit.client.ui.main.account

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import org.koin.android.ext.android.inject
import pl.gosak.rit.client.R
import pl.gosak.rit.client.core.JwtTokenManager
import pl.gosak.rit.client.databinding.FragmentAccountBinding
import pl.gosak.rit.client.ui.main.account.service.AccountIdsResolver
import pl.gosak.rit.client.ui.main.account.service.ChildSafeService
import pl.gosak.rit.client.utils.ui.AbstractFragment
import pl.gosak.rit.lib.auth.dto.AuthenticationResponse

class AccountFragment : AbstractFragment(), AccountExecutor {
    private val jwtTokenManager: JwtTokenManager by inject()
    private val childSafeService: ChildSafeService by inject()
    private val accountIdsResolver: AccountIdsResolver by inject()

    private lateinit var model: AccountViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        model = ViewModelProviders.of(activity!!).get(AccountViewModel::class.java)
        val childSafe = childSafeService.isNotSafeContentEnabled()
        model.setChildSafe(childSafe)
        model.setUserId(accountIdsResolver.userId())
        model.setDeviceId(accountIdsResolver.deviceId())

        val view = DataBindingUtil.inflate<FragmentAccountBinding>(layoutInflater, R.layout.fragment_account, container, false).also {
            it.model = model
            it.executor = this
        }.root

        return view
    }

    override fun logout() {
        jwtTokenManager.logged(AuthenticationResponse())
        activity?.finish()
    }

    override fun toggleChildSafe() {
        childSafeService.toggleChildSafe()
    }

    companion object {
        fun instance() = AccountFragment()
    }
}