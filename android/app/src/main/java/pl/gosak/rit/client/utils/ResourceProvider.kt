package pl.gosak.rit.client.utils

import android.content.Context
import android.content.res.Resources
import pl.gosak.rit.client.RateItApplication

object BindingUtils {
    fun context(): Context = RateItApplication.instance.baseContext

    fun resources(): Resources = RateItApplication.instance.resources

    fun theme(): Resources.Theme = RateItApplication.instance.theme
}