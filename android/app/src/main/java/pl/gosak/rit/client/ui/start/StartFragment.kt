package pl.gosak.rit.client.ui.start

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import pl.gosak.rit.client.R
import pl.gosak.rit.client.databinding.FragmentStartBinding

class StartFragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) =
            DataBindingUtil.inflate<FragmentStartBinding>(inflater, R.layout.fragment_start, container, false).let {
                it.executor = activity as StartExecutor
                it.root
            }

    companion object {
        fun instance() = StartFragment()
    }
}