package pl.gosak.rit.client.ui.start.login

import android.view.View
import androidx.databinding.Bindable
import androidx.databinding.library.baseAdapters.BR
import pl.gosak.rit.client.utils.BindingUtils
import pl.gosak.rit.client.utils.ui.ObservableViewModel
import pl.gosak.rit.client.utils.validator.readable.RequiredMaxLengthReadableValidator


class LoginModel(private var name: String = "",
                 private var password: String = "",
                 private var nameError: String? = null,
                 private var passwordError: String? = null,
                 private var loading: Boolean = false,
                 private var loaderVisibility: Int = View.INVISIBLE,
                 private var loginEnabled: Boolean = false): ObservableViewModel() {
    private val requiredMaxLengthValidator = RequiredMaxLengthReadableValidator(60)

    @Bindable
    fun getName() = name

    @Bindable
    fun getPassword() = password

    @Bindable
    fun isLoginEnabled() = loginEnabled

    @Bindable
    fun getNameError() = nameError

    @Bindable
    fun getPasswordError() = passwordError

    @Bindable
    fun getLoaderVisibility() = loaderVisibility

    @Bindable
    fun isEditEnabled() = !loading

    fun setName(value: String) {
        name = value
        notifyPropertyChanged(BR.name)
        validateName()
    }

    fun setPassword(value: String) {
        password = value
        notifyPropertyChanged(BR.password)
        validatePassword()
    }

    fun setLoading(value: Boolean) {
        loading = value
        resolveLoaderVisibility()
        resolveLoginPermission()
        notifyPropertyChanged(BR.editEnabled)
    }

    private fun validateName() {
        nameError = name
                .let { requiredMaxLengthValidator.validate(it) }
                ?.let { BindingUtils.resources().getString(it) }

        notifyPropertyChanged(BR.nameError)
        resolveLoginPermission()
    }

    private fun validatePassword() {
        passwordError = password
                .let { requiredMaxLengthValidator.validate(it) }
                ?.let { BindingUtils.resources().getString(it) }

        notifyPropertyChanged(BR.passwordError)
        resolveLoginPermission()
    }

    private fun resolveLoaderVisibility() {
        loaderVisibility = if (loading) View.VISIBLE else View.INVISIBLE
        notifyPropertyChanged(BR.loaderVisibility)
    }

    private fun resolveLoginPermission() {
        val newLoginEnabled = nameError == null && passwordError == null && !name.isBlank() && !password.isBlank() && !loading
        loginEnabled = newLoginEnabled
        notifyPropertyChanged(BR.loginEnabled)
    }
}