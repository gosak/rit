package pl.gosak.rit.client.core.api.recomme

import org.koin.dsl.module.module
import pl.gosak.rit.client.core.api.recomme.service.RecommeService
import pl.gosak.rit.client.core.api.recomme.service.RecommeServiceImpl
import pl.gosak.rit.client.core.network.URL
import pl.gosak.rit.client.core.network.createWebService

val recommeApiModule = module {
    single { createWebService<RecommeApi>(get(), URL) }
    single { RecommeServiceImpl(get()) as RecommeService }
}