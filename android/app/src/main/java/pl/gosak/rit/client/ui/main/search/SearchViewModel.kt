package pl.gosak.rit.client.ui.main.search

import androidx.databinding.Bindable
import androidx.lifecycle.LiveData
import androidx.paging.DataSource
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import pl.gosak.rit.client.BR
import pl.gosak.rit.client.core.api.search.service.SearchService
import pl.gosak.rit.client.ui.main.account.service.ChildSafeService
import pl.gosak.rit.client.utils.ui.ObservableViewModel
import pl.gosak.rit.lib.subject.dto.SubjectPreviewDto


class SearchViewModel(searchService: SearchService,
                      childSafeService: ChildSafeService) : ObservableViewModel() {
    private val dataSourceFactory = object : DataSource.Factory<Int, SubjectPreviewDto>() {
        override fun create(): DataSource<Int, SubjectPreviewDto> {
            return SearchDataSource(query, searchService, childSafeService)
        }
    }

    private var query: String = ""

    @Bindable
    fun getQuery() = query

    fun setQuery(string: String) {
        query = string
        notifyPropertyChanged(BR.query)
    }

    private val config = PagedList.Config.Builder()
            .setPageSize(SearchDataSource.PAGE_SIZE)
            .setPrefetchDistance(SearchDataSource.PAGE_SIZE)
            .setEnablePlaceholders(true)
            .build()

    fun getSubjects(): LiveData<PagedList<SubjectPreviewDto>> = initializedPagedListBuilder(config).build()

    private fun initializedPagedListBuilder(config: PagedList.Config): LivePagedListBuilder<Int, SubjectPreviewDto> {
        return LivePagedListBuilder<Int, SubjectPreviewDto>(dataSourceFactory, config)
    }
}