package pl.gosak.rit.client.ui.main.account.service

import android.content.SharedPreferences

class ChildSafeServiceImpl(private val sharedPreferences: SharedPreferences) : ChildSafeService {
    override fun isNotSafeContentEnabled() = sharedPreferences.getBoolean(NOT_SAFE_CONTENT_ENABLED_KEY, true)

    override fun toggleChildSafe() {
        sharedPreferences.edit().apply {
            val current = sharedPreferences.getBoolean(NOT_SAFE_CONTENT_ENABLED_KEY, true)
            putBoolean(NOT_SAFE_CONTENT_ENABLED_KEY, !current)
        }.apply()
    }

    companion object {
        const val NOT_SAFE_CONTENT_ENABLED_KEY = "childSafeKey"
    }
}