package pl.gosak.rit.client.ui.main.exhibit.subject

interface SubjectExecutor {
    fun userRate(value: Double)
}