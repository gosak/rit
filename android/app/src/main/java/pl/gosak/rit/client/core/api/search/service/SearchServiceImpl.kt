package pl.gosak.rit.client.core.api.search.service

import kotlinx.coroutines.Deferred
import pl.gosak.rit.client.core.api.search.SearchApi
import pl.gosak.rit.lib.search.dto.SearchSubjectsResponse
import retrofit2.Response

class SearchServiceImpl(private val searchApi: SearchApi) : SearchService {
    override fun search(from: Int, count: Int, query: String): Deferred<Response<SearchSubjectsResponse>> {
        return searchApi.search(from, count, query)
    }
}