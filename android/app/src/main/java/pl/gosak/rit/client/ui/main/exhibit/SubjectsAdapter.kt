package pl.gosak.rit.client.ui.main.exhibit

import android.graphics.PorterDuff
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import pl.gosak.rit.client.R
import pl.gosak.rit.client.ui.main.exhibit.model.SubjectViewHolder
import pl.gosak.rit.client.utils.rateBarReview
import pl.gosak.rit.client.utils.ui.fitCenterPhoto
import pl.gosak.rit.client.utils.ui.resolveDominantColor
import pl.gosak.rit.lib.subject.dto.SubjectPreviewDto


class SubjectsAdapter(private val subjectOnClickListener: SubjectOnClickListener) : PagedListAdapter<SubjectPreviewDto, SubjectViewHolder>(DiffUtilCallBack()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SubjectViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.subject_card, parent, false)
        return SubjectViewHolder(view)
    }

    override fun onBindViewHolder(holder: SubjectViewHolder, position: Int) {
        getItem(position)?.let {
            holder.photoView.setOnClickListener { _ -> subjectOnClickListener.onItemClick(holder, it) }
            holder.rate.progress = it.rateBarReview().toInt()

            val color = resolveDominantColor(it.photoDominantColor)

            holder.rate.progressDrawable.setColorFilter(color, PorterDuff.Mode.SRC_ATOP)
            holder.photoView.fitCenterPhoto(it.photo, color)
        }
    }
}