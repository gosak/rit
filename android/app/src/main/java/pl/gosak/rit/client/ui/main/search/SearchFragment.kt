package pl.gosak.rit.client.ui.main.search

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.SearchView
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import kotlinx.android.synthetic.main.fragment_search.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.koin.android.ext.android.inject
import pl.gosak.rit.client.R
import pl.gosak.rit.client.core.api.recomme.service.RecommeService
import pl.gosak.rit.client.databinding.FragmentSearchBinding
import pl.gosak.rit.client.ui.main.HomeNavigator
import pl.gosak.rit.client.ui.main.exhibit.SubjectOnClickListener
import pl.gosak.rit.client.ui.main.exhibit.SubjectsAdapter
import pl.gosak.rit.client.ui.main.exhibit.model.SubjectViewHolder
import pl.gosak.rit.client.utils.ui.AbstractFragment
import pl.gosak.rit.lib.subject.dto.SubjectPreviewDto

class SearchFragment : AbstractFragment() {
    private val recommeService: RecommeService by inject()
    private val searchModel: SearchViewModel by inject()

    private lateinit var homeNavigator: HomeNavigator

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        homeNavigator = (activity as HomeNavigator)
        val view = DataBindingUtil.inflate<FragmentSearchBinding>(layoutInflater, R.layout.fragment_search, container, false).also {
            it.model = searchModel
        }.root
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(string: String?): Boolean {
                searchModel.setQuery(string ?: "")
                searchForResults()
                return false
            }

            override fun onQueryTextChange(string: String?): Boolean {
                return false
            }
        })
        searchView.onActionViewExpanded()

        searchForResults()
        subjects.layoutManager = GridLayoutManager(context, 2)
    }

    fun searchForResults() {
        val subjectsAdapter = SubjectsAdapter(object : SubjectOnClickListener {
            override fun onItemClick(viewHolder: SubjectViewHolder, subject: SubjectPreviewDto) {
                showSubject(subject)
            }
        })

        searchModel.getSubjects().observe(this, Observer {
            subjectsAdapter.submitList(it)
        })

        subjects.adapter = subjectsAdapter
    }

    private fun showSubject(subject: SubjectPreviewDto) {
        launch {
            withContext(Dispatchers.IO) {
                recommeService.viewSubject(subject.uuid).await()
            }
        }
        homeNavigator.showSubject(subject, SUBJECT_TAG)
    }

    companion object {
        const val SUBJECT_TAG = "searchSubjectTag"

        fun instance() = SearchFragment()
    }
}