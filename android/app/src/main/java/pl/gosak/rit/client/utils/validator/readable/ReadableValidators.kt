package pl.gosak.rit.client.utils.validator.readable

import pl.gosak.rit.client.R
import pl.gosak.rit.client.utils.validator.simple.EmailValidator
import pl.gosak.rit.client.utils.validator.simple.MaxLengthValidator
import pl.gosak.rit.client.utils.validator.simple.MinLengthValidator
import pl.gosak.rit.client.utils.validator.simple.RequiredValidator


class RequiredReadableValidator : AbstractReadableValidator(RequiredValidator(), R.string.error_empty)

class MinLengthReadableValidator(minLength: Int) : AbstractReadableValidator(MinLengthValidator(minLength), R.string.error_too_short)

class MaxLengthReadableValidator(maxLength: Int) : AbstractReadableValidator(MaxLengthValidator(maxLength), R.string.error_too_long)

class EmailReadableValidator : AbstractReadableValidator(EmailValidator(), R.string.error_incorrect_email)

class RequiredMaxLengthReadableValidator(maxLength: Int)
    : GroupReadableValidator(RequiredReadableValidator(), MaxLengthReadableValidator(maxLength))

class PasswordReadableValidator
    : GroupReadableValidator(MinLengthReadableValidator(6), MaxLengthReadableValidator(60))