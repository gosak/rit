package pl.gosak.rit.client.core.api.recomme.service

import kotlinx.coroutines.Deferred
import pl.gosak.rit.lib.recomme.dto.RecommendedSubjects
import retrofit2.Response
import java.util.*

interface RecommeService {
    fun recommend(count: Int): Deferred<Response<RecommendedSubjects>>

    fun viewSubject(subjectUuid: UUID): Deferred<Response<String>>
}