package pl.gosak.rit.client

import pl.gosak.rit.client.core.api.auth.authApiModule
import pl.gosak.rit.client.core.api.recomme.recommeApiModule
import pl.gosak.rit.client.core.api.search.searchApiModule
import pl.gosak.rit.client.core.api.subject.subjectApiModule
import pl.gosak.rit.client.core.coreModules
import pl.gosak.rit.client.core.network.networkModule
import pl.gosak.rit.client.ui.main.homeModule
import pl.gosak.rit.client.ui.start.startModule

val ritAppModules = listOf(coreModules, networkModule, authApiModule, recommeApiModule, subjectApiModule, searchApiModule, startModule, homeModule)