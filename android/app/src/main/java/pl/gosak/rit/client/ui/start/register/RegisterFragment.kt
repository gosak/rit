package pl.gosak.rit.client.ui.start.register

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_register.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.koin.android.ext.android.inject
import pl.gosak.rit.client.R
import pl.gosak.rit.client.core.JwtTokenManager
import pl.gosak.rit.client.core.api.auth.service.AuthService
import pl.gosak.rit.client.databinding.FragmentRegisterBinding
import pl.gosak.rit.client.ui.start.StartExecutor
import pl.gosak.rit.client.utils.ui.AbstractFragment


class RegisterFragment : AbstractFragment(), RegisterExecutor {
    private val authService: AuthService by inject()
    private val jwtTokenManager: JwtTokenManager by inject()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val registerModel = ViewModelProviders.of(activity!!).get(RegisterModel::class.java)

        val view = DataBindingUtil.inflate<FragmentRegisterBinding>(layoutInflater, R.layout.fragment_register, container, false).let {
            it.model = registerModel
            it.executor = this
            it.root
        }

        return view
    }

    override fun register(registerModel: RegisterModel) {
        registerModel.setLoading(true)
        launch {
            runCatching {
                withContext(Dispatchers.IO) {
                    val response = authService.register(registerModel.getName(), registerModel.getEmail(), registerModel.getPassword()).await()
                    when (response.code()) {
                        409 -> Snackbar.make(layout, R.string.auth_email_already_used, Snackbar.LENGTH_LONG).show()
                        201 -> login(registerModel)
                        else -> Snackbar.make(layout, R.string.error_unexpected, Snackbar.LENGTH_LONG).show()
                    }
                }
            }.onFailure {
                registerModel.setLoading(false)
                Snackbar.make(layout, R.string.api_503, Snackbar.LENGTH_LONG).show()
            }
        }
    }

    fun login(registerModel: RegisterModel) = launch {
        Snackbar.make(layout, R.string.auth_logging, Snackbar.LENGTH_LONG).show()
        registerModel.setLoading(true)
        runCatching {
            withContext(Dispatchers.IO) {
                val response = authService.login(registerModel.getEmail(), registerModel.getPassword()).await()
                when (response.code()) {
                    403 -> Snackbar.make(layout, R.string.auth_email_or_password_invalid, Snackbar.LENGTH_LONG).show()
                    201 -> response.body()?.let {
                        jwtTokenManager.logged(it)
                        registerModel.setLoading(false)
                        (activity as StartExecutor).goToHome()
                    }
                    else -> Snackbar.make(layout, R.string.error_unexpected, Snackbar.LENGTH_LONG).show()
                }
                registerModel.setLoading(false)
            }
        }.onFailure {
            registerModel.setLoading(false)
            Snackbar.make(layout, R.string.api_503, Snackbar.LENGTH_LONG).show()
        }
    }

    companion object {
        fun instance() = RegisterFragment()
    }
}