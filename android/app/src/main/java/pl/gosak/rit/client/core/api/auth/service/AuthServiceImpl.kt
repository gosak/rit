package pl.gosak.rit.client.core.api.auth.service

import kotlinx.coroutines.Deferred
import pl.gosak.rit.client.core.api.auth.AuthApi
import pl.gosak.rit.lib.auth.dto.*
import retrofit2.Response

class AuthServiceImpl(private val authApi: AuthApi) : AuthService {
    override fun login(email: String, password: String): Deferred<Response<AuthenticationResponse>> {
        return authApi.login(AuthenticationRequest(email, password))
    }

    override fun register(name: String, email: String, password: String): Deferred<Response<RegisterResponse>> {
        return authApi.register(RegisterRequest(name, email, password))
    }

    override fun test(): Deferred<Response<TestResponse>> {
        return authApi.test()
    }
}