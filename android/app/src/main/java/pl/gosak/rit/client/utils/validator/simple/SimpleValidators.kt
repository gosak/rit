package pl.gosak.rit.client.utils.validator.simple

import android.text.TextUtils
import android.util.Patterns

class RequiredValidator : Validator {
    override fun validate(string: String) = string.isNotBlank()
}

class MinLengthValidator(private val minLength: Int) : Validator {
    override fun validate(string: String) = string.length >= minLength
}

class MaxLengthValidator(private val maxLength: Int) : Validator {
    override fun validate(string: String) = string.length <= maxLength
}

class EmailValidator : Validator {
    override fun validate(string: String) = !TextUtils.isEmpty(string) && Patterns.EMAIL_ADDRESS.matcher(string).matches()
}