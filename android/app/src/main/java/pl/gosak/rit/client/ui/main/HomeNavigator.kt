package pl.gosak.rit.client.ui.main

import pl.gosak.rit.lib.subject.dto.SubjectPreviewDto

interface HomeNavigator {
    fun account()

    fun addSubject()

    fun exhibit()

    fun search()

    fun ratings()

    fun showSubject(subject: SubjectPreviewDto, tag: String)
}