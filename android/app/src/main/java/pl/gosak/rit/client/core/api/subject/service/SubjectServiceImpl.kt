package pl.gosak.rit.client.core.api.subject.service

import kotlinx.coroutines.Deferred
import pl.gosak.rit.client.core.api.subject.SubjectApi
import pl.gosak.rit.lib.subject.dto.RateSubjectResponse
import pl.gosak.rit.lib.subject.dto.SubjectDto
import retrofit2.Response
import java.util.*

class SubjectServiceImpl(private val subjectApi: SubjectApi): SubjectService {
    override fun get(uuid: UUID): Deferred<Response<SubjectDto>> {
        return subjectApi.get(uuid)
    }

    override fun rate(uuid: UUID, value: Double): Deferred<Response<RateSubjectResponse>> {
        return subjectApi.rate(uuid, value)
    }
}