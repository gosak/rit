package pl.gosak.rit.client.ui.main.exhibit

import androidx.recyclerview.widget.DiffUtil
import pl.gosak.rit.lib.subject.dto.SubjectPreviewDto

class DiffUtilCallBack : DiffUtil.ItemCallback<SubjectPreviewDto>() {
    override fun areItemsTheSame(oldItem: SubjectPreviewDto, newItem: SubjectPreviewDto): Boolean {
        return oldItem.uuid == newItem.uuid
    }

    override fun areContentsTheSame(oldItem: SubjectPreviewDto, newItem: SubjectPreviewDto): Boolean {
        return oldItem.name == newItem.name
                && oldItem.uuid == newItem.uuid
                && oldItem.rate == newItem.rate
    }
}