package pl.gosak.rit.client.utils

import pl.gosak.rit.lib.subject.dto.SubjectPreviewDto


fun SubjectPreviewDto.rateBarReview(): Double {
    return rate.plus(1).times(25)
}

fun ratingBarValueToRitRate(value: Double): Double {
    return value.div(2.5) - 1
}