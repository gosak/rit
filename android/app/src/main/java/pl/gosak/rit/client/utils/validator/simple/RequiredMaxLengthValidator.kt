package pl.gosak.rit.client.utils.validator.simple

class RequiredMaxLengthValidator(maxLength: Int) : SimpleGroupValidator(RequiredValidator(), MaxLengthValidator(maxLength))