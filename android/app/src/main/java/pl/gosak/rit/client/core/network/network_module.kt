package pl.gosak.rit.client.core.network

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import okhttp3.OkHttpClient
import org.koin.dsl.module.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


const val URL = "https://rit.nazwa.pl:7001/"

val networkModule = module {
    single {
        getUnsafeOkHttpClientBuilder()
                .connectTimeout(15L, TimeUnit.SECONDS)
                .readTimeout(15L, TimeUnit.SECONDS)
                .authenticator(JwtTokenAuthenticator(get()))
                .addInterceptor(JwtTokenHeaderInterceptor(get()))
                .addInterceptor(FixedHttpLogger("HTTP")).build()
    }

    single { GlideConfiguration() }
}


inline fun <reified T> createWebService(okHttpClient: OkHttpClient, url: String): T {
    val retrofit = Retrofit.Builder()
            .baseUrl(url)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(CoroutineCallAdapterFactory.invoke()).build()
    return retrofit.create(T::class.java)
}



