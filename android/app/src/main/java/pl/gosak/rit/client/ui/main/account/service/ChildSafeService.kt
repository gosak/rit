package pl.gosak.rit.client.ui.main.account.service

interface ChildSafeService {
    fun isNotSafeContentEnabled(): Boolean

    fun toggleChildSafe()
}