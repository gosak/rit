package pl.gosak.rit.client.ui.main.ratings

import android.os.Bundle
import pl.gosak.rit.client.utils.ui.AbstractFragment

class RatingsFragment : AbstractFragment() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    companion object {
        fun instance() = RatingsFragment()
    }
}