package pl.gosak.rit.client.ui.main.exhibit.subject

import android.content.DialogInterface
import android.content.res.ColorStateList
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatDialogFragment
import androidx.appcompat.widget.AppCompatRatingBar
import kotlinx.android.synthetic.main.fragment_dialog_rate.*
import pl.gosak.rit.client.R


class RateDialogFragment : AppCompatDialogFragment() {
    private var ratingBar: AppCompatRatingBar? = null

    override fun onCreateDialog(savedInstanceState: Bundle?) =
            AlertDialog.Builder(context!!).apply {
                setView(LayoutInflater.from(context).inflate(R.layout.fragment_dialog_rate, null))
                setTitle(R.string.subject_rate_question)
                setPositiveButton(android.R.string.ok, DialogInterface.OnClickListener { _, _ ->
                    (parentFragment as SubjectExecutor).userRate(ratingBar?.rating?.toDouble() ?: 0.0)
                })
            }.create().apply {
                setOnShowListener {
                    val dominantColor = arguments?.getInt(DOMINANT_COLOR) ?: 0

                    ratingBar = rateBar.apply {
                        setOnRatingBarChangeListener { _, value, _ ->
                            rateText.text = String.format("%.1f", value)
                        }
                        progressTintList = ColorStateList.valueOf(dominantColor)
                    }
                    getButton(AlertDialog.BUTTON_POSITIVE).apply {
                        setTextColor(dominantColor)
                    }
                }
            }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

    }

    companion object {
        const val RATE_DIALOG_FRAGMENT_TAG = "rateDialogFragmentTag"
        const val DOMINANT_COLOR = "dominantColor"

        fun newInstance(dominantColor: Int) = RateDialogFragment().apply {
            val args = Bundle()
            args.putInt(DOMINANT_COLOR, dominantColor)
            arguments = args
        }
    }
}