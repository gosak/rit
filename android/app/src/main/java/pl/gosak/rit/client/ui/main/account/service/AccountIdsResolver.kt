package pl.gosak.rit.client.ui.main.account.service

interface AccountIdsResolver {
    fun userId(): String

    fun deviceId(): String
}