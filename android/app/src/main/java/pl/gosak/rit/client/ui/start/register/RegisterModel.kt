package pl.gosak.rit.client.ui.start.register

import android.view.View
import androidx.databinding.Bindable
import androidx.databinding.library.baseAdapters.BR
import pl.gosak.rit.client.utils.BindingUtils
import pl.gosak.rit.client.utils.ui.ObservableViewModel
import pl.gosak.rit.client.utils.validator.readable.EmailReadableValidator
import pl.gosak.rit.client.utils.validator.readable.PasswordReadableValidator
import pl.gosak.rit.client.utils.validator.readable.RequiredMaxLengthReadableValidator

class RegisterModel(private var name: String = "",
                    private var nameError: String? = null,
                    private var email: String = "",
                    private var password: String = "",
                    private var emailError: String? = null,
                    private var passwordError: String? = null,
                    private var loading: Boolean = false,
                    private var loaderVisibility: Int = View.INVISIBLE,
                    private var registerEnabled: Boolean = false) : ObservableViewModel() {
    private val emailReadableValidator = EmailReadableValidator()
    private val passwordReadableValidator = PasswordReadableValidator()
    private val requiredMaxLengthValidator = RequiredMaxLengthReadableValidator(60)

    @Bindable
    fun getName() = name

    @Bindable
    fun getNameError() = nameError

    @Bindable
    fun getEmail() = email

    @Bindable
    fun getPassword() = password

    @Bindable
    fun isRegisterEnabled() = registerEnabled

    @Bindable
    fun getEmailError() = emailError

    @Bindable
    fun getPasswordError() = passwordError

    fun isLoading() = loading

    @Bindable
    fun getLoaderVisibility() = loaderVisibility

    @Bindable
    fun isEditEnabled(): Boolean = !loading

    fun setName(value: String) {
        name = value
        notifyPropertyChanged(BR.name)
        validateName()
    }

    private fun validateName() {
        nameError = name
                .let { requiredMaxLengthValidator.validate(it) }
                ?.let { BindingUtils.resources().getString(it) }

        notifyPropertyChanged(BR.nameError)
        resolveRegisterPermission()
    }

    fun setEmail(value: String) {
        email = value
        notifyPropertyChanged(BR.email)
        validateEmail()
    }

    fun setPassword(value: String) {
        password = value
        notifyPropertyChanged(BR.password)
        validatePassword()
    }

    fun setLoading(value: Boolean) {
        loading = value
        notifyPropertyChanged(BR.editEnabled)
        resolveLoaderVisibility()
        resolveRegisterPermission()
    }

    private fun validateEmail() {
        emailError = email
                .let { emailReadableValidator.validate(it) }
                ?.let { BindingUtils.resources().getString(it) }

        notifyPropertyChanged(BR.emailError)
        resolveRegisterPermission()
    }

    private fun validatePassword() {
        passwordError = password
                .let { passwordReadableValidator.validate(it) }
                ?.let { BindingUtils.resources().getString(it) }

        notifyPropertyChanged(BR.passwordError)
        resolveRegisterPermission()
    }

    private fun resolveLoaderVisibility() {
        loaderVisibility = if (loading) View.VISIBLE else View.INVISIBLE
        notifyPropertyChanged(BR.loaderVisibility)
    }

    private fun resolveRegisterPermission() {
        val newRegisterEnabled = nameError == null && emailError == null && passwordError == null && !email.isBlank() && !password.isBlank() && !loading

        if (registerEnabled != newRegisterEnabled) {
            registerEnabled = newRegisterEnabled
            notifyPropertyChanged(BR.registerEnabled)
        }
    }
}