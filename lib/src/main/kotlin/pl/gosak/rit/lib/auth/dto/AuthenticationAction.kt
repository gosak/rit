/*
 * Copyright (c) 2018 This file is subject to the terms and conditions defined in file 'LICENSE.txt' which is part of this source code package.
 */

package pl.gosak.rit.lib.auth.dto

class AuthenticationRequest(var email: String = "", var password: String = "")

class AuthenticationResponse(var resourceToken: String = "", var refreshToken: String = "")