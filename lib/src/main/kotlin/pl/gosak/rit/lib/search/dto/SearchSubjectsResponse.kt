package pl.gosak.rit.lib.search.dto

import pl.gosak.rit.lib.subject.dto.SubjectPreviewDto

class SearchSubjectsResponse(val subjects: List<SubjectPreviewDto> = listOf())