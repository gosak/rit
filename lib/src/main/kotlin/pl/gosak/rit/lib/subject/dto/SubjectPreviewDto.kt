package pl.gosak.rit.lib.subject.dto

import java.util.*

data class SubjectPreviewDto(var uuid: UUID = UUID(0, 0),
                             var name: String = "",
                             var rate: Double = 0.0,
                             var description: String = "",
                             var manufacturer: String = "",
                             var photo: UUID = UUID(0, 0),
                             var photoDominantColor: String = "",
                             var childSafe: Boolean = false)