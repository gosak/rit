package pl.gosak.rit.lib.subject

import pl.gosak.rit.lib.photo.PhotoEndpoints.Parts.SUBJECT_UUID
import pl.gosak.rit.lib.subject.dto.CreateSubjectRequest
import pl.gosak.rit.lib.subject.dto.CreateSubjectResponse
import pl.gosak.rit.lib.subject.dto.RateSubjectResponse
import pl.gosak.rit.lib.subject.dto.SubjectDto

object SubjectEndpoints {
    const val PREFIX = "subject"

    /**
     * Post
     * @return 201 -> subject create
     * @see CreateSubjectRequest
     * @see CreateSubjectResponse
     */
    const val CREATE = "$PREFIX/create"


    /**
     * Get
     * @return 200 -> successful
     *         404 -> subject doesn't exist
     * @see SubjectDto
     */
    const val SUBJECT = "$PREFIX/{$SUBJECT_UUID}"

    /**
     * Get
     * @return 200 -> successful
     *         400 -> wrong parameters
     *
     * @see SubjectDto
     */
    const val USER_SUBJECTS = "$PREFIX/"

    /**
     * Post
     * Required parameter - userRate: Double <-1;1>
     *
     * @return 201 -> successful
     *         404 -> subject doesn't exist
     * @see RateSubjectResponse
     */
    const val RATE = "$PREFIX/{$SUBJECT_UUID}/rate"

    object Paths {
        const val SUBJECT_UUID = "subjectUuid"
    }

    object Params {
        const val USER_RATE = "userRate"
        const val PAGE = "page"
        const val COUNT = "count"
        const val ORDER_BY = "orderBy"
    }

    enum class UserSubjectsOrder {
        DATE_ASC, DATE_DESC, RATE_ASC, RATE_DESC
    }
}