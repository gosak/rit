package pl.gosak.rit.lib.subject.dto

import java.util.*

data class RateSubjectResponse(var uuid: UUID = UUID(0, 0), var rate: Double = 0.0, var rates: Long = 0)