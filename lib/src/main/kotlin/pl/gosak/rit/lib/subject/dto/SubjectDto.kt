package pl.gosak.rit.lib.subject.dto

import java.util.*

class SubjectDto(var uuid: UUID = UUID(0, 0),
                 var name: String = "",
                 var description: String = "",
                 var manufacturer: String = "",
                 var rate: Double = -1.0,
                 var rates: Long = 0,
                 var subjectPhotos: Array<SubjectPhotoDto> = arrayOf())