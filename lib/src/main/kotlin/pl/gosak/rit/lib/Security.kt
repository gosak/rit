/*
 * Copyright (c) 2018 This file is subject to the terms and conditions defined in file 'LICENSE.txt' which is part of this source code package.
 */

package pl.gosak.rit.lib

object SecurityConstants {
    const val TOKEN_PREFIX = "Bearer "
    const val AUTHORIZATION_HEADER = "Authorization"

    object Roles {
        const val USER = "USER"
        const val ROLE_USER = "ROLE_USER"
        const val REFRESH_TOKEN = "REFRESH_TOKEN"
        const val ROLE_REFRESH_TOKEN = "ROLE_REFRESH_TOKEN"
    }

    object Privileges {
        const val RATE_SUBJECT = "RATE_SUBJECT_PRIVILEGE"
        const val ADD_SUBJECT = "ADD_SUBJECT_PRIVILEGE"
    }
}

object JwtTokenCustomFields {
    const val USER_UUID = "userUuid"
    const val DEVICE_UUID = "deviceUuid"
    const val ROLES = "roles"
}