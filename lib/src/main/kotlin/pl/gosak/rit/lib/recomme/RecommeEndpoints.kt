package pl.gosak.rit.lib.recomme

import pl.gosak.rit.lib.recomme.RecommeEndpoints.Paths.SUBJECT_UUID
import pl.gosak.rit.lib.recomme.dto.RecommendedSubjects
import pl.gosak.rit.lib.recomme.dto.RecommendedSubjectsUuids

object RecommeEndpoints {
    const val PREFIX = "recomme"

    /**
     * Get
     * @return 200 -> successful
     * @see RecommendedSubjectsUuids
     */
    const val RECOMMEND_SUBJECTS_UUIDS_TO_USER = "$PREFIX/ids/" // get

    /**
     * Get
     * @return 200 -> successful
     * @see RecommendedSubjects
     */
    const val RECOMMEND_SUBJECTS_TO_USER = "$PREFIX/"

    /**
     * Post
     * @return 201 -> successful
     *         404 -> subject doesn't exist
     * @see String
     */
    const val VIEW_SUBJECT = "$PREFIX/{$SUBJECT_UUID}" // post

    object Params {
        const val COUNT = "count"
    }

    object Paths {
        const val SUBJECT_UUID = "subjectUuid"
    }
}
