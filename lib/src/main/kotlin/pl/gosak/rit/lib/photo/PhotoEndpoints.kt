package pl.gosak.rit.lib.photo

import pl.gosak.rit.lib.photo.PhotoEndpoints.Parts.SUBJECT_UUID
import pl.gosak.rit.lib.photo.PhotoEndpoints.Paths.PHOTO_UUID
import pl.gosak.rit.lib.photo.dto.UploadFileResponse

object PhotoEndpoints {
    const val PREFIX = "photo"

    /**
     * Post
     * Required parts: subjectUuid: UUID, photo: MultipartFile(jpeg, jpg, png)
     * @return 201 -> successful
     *         404 -> subject doesn't exist
     *         413 -> photo has too large size
     *         415 -> unsupported media type
     *         500 -> service has problems with children's safety assessment, please try later
     * @see UploadFileResponse
     */
    const val PHOTO_UPLOAD = "$PREFIX/{$SUBJECT_UUID}/upload"

    /**
     * Get
     * @return 200 -> successful
     *         404 -> photo doesn't exist
     * @see PhotoSizes
     */
    const val PHOTO_DOWNLOAD = "$PREFIX/{$PHOTO_UUID}"

    object Parts {
        const val SUBJECT_UUID = "subjectUuid"
        const val PHOTO = "photo"
    }

    object Paths {
        const val PHOTO_UUID = "photoUuid"
    }

    object Params {
        const val SIZE = "size"
    }
}

enum class PhotoSizes {
    ORIGIN, THUMBNAIL;
}