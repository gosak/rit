/*
 * Copyright (c) 2018 This file is subject to the terms and conditions defined in file 'LICENSE.txt' which is part of this source code package.
 */

package pl.gosak.rit.lib.auth

import pl.gosak.rit.lib.auth.dto.AuthenticationRequest
import pl.gosak.rit.lib.auth.dto.AuthenticationResponse
import pl.gosak.rit.lib.auth.dto.RegisterRequest
import pl.gosak.rit.lib.auth.dto.RegisterResponse
import pl.gosak.rit.lib.auth.dto.TestResponse

object AuthEndpoints {
    const val PREFIX = "auth"

    /**
     * Post
     * @return 201 -> successful
     *         409 -> email is already occupied
     * @see RegisterRequest
     * @see RegisterResponse
     */
    const val REGISTER = "$PREFIX/register"

    /**
     * Post
     * @return 201 -> successful
     *         403 -> refresh token is incorrect
     * @see AuthenticationRequest
     * @see AuthenticationResponse
     */
    const val LOGIN = "$PREFIX/login"

    /**
     * Get
     * Require refresh token in header
     * Authorization: Bearer refresh-token
     *
     * @return 201 -> successful
     *         403 -> refresh token is incorrect
     * @see AuthenticationRequest
     * @see AuthenticationResponse
     */
    const val REVOKE = "$PREFIX/revoke"

    /**
     * Put
     * @return 205 -> successful
     */
    const val LOGOUT = "$PREFIX/logout"

    /**
     * Put
     * @return 205 -> successful
     */
    const val LOGOUT_ALL = "$PREFIX/logoutAll"

    /**
     * Get
     * @return 200 -> successful
     * @see TestResponse
     */
    const val TEST = "$PREFIX/test"

    //TODO
    const val RESET_PASSWORD = "$PREFIX/resetPassword"
}
