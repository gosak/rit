package pl.gosak.rit.lib.subject.dto

import java.util.*

data class SubjectPhotoDto(var uuid: UUID = UUID(0, 0), var childSafe: Boolean = false, var dominantColor: String = "")