package pl.gosak.rit.lib.photo.dto

import java.util.*

class UploadFileResponse(var uuid: UUID = UUID(0, 0),
                         var childSafe: Boolean = false,
                         var dominantColor: String = "")