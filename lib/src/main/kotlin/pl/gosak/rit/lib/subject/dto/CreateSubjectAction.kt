package pl.gosak.rit.lib.subject.dto

import java.util.*

data class CreateSubjectRequest(var name: String = "", var description: String = "", var manufacturer: String = "", var rate: Double? = null)

data class CreateSubjectResponse(var uuid: UUID = UUID(0, 0))