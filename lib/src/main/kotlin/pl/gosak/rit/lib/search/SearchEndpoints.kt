package pl.gosak.rit.lib.search

import pl.gosak.rit.lib.search.dto.SearchSubjectsResponse


object SearchEndpoints {
    const val PREFIX = "search"

    /**
     * Required parameters: query: String, from: Int, count: Int
     * @return 200 -> successful
     * @see SearchSubjectsResponse
     */
    const val SEARCH_SUBJECTS = "$PREFIX/" // get

    object Params {
        const val QUERY = "string"
        const val COUNT = "count"
        const val FROM = "from"
    }
}