package pl.gosak.rit.lib.recomme.dto

import pl.gosak.rit.lib.subject.dto.SubjectPreviewDto

class RecommendedSubjects(var subjects: Array<SubjectPreviewDto> = arrayOf())