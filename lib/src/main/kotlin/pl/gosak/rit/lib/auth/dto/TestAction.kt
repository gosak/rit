package pl.gosak.rit.lib.auth.dto

import java.util.*


class TestResponse(
        var deviceUuid: UUID = UUID(0, 0),
        var userUuid: UUID = UUID(0, 0)
)