package pl.gosak.rit.recomme.kafka

import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Profile
import org.springframework.kafka.annotation.KafkaHandler
import org.springframework.kafka.annotation.KafkaListener
import org.springframework.messaging.handler.annotation.Payload
import org.springframework.stereotype.Component
import pl.gosak.rit.microbase.kafka.KafkaConstants.USER_TOPIC
import pl.gosak.rit.microbase.kafka.KafkaConstants.USER_TOPIC_RECOMME_GROUP
import pl.gosak.rit.microbase.kafka.listener.UserTopicListener
import pl.gosak.rit.microbase.kafka.message.user.NewUserMsg
import pl.gosak.rit.recomme.service.RecommendationService


@Component
@Profile("local", "docker")
@KafkaListener(topics = [USER_TOPIC], groupId = USER_TOPIC_RECOMME_GROUP)
class RecommeUserTopicListenerImpl(private val recommendationService: RecommendationService) : UserTopicListener {
    private val logger = LoggerFactory.getLogger(RecommeUserTopicListenerImpl::class.java)

    @KafkaHandler
    override fun handle(@Payload msg: NewUserMsg) {
        logger.info("KAFKA_RECOMME: received NewUserMsg: {}", msg)
        recommendationService.addUser(msg)
    }
}