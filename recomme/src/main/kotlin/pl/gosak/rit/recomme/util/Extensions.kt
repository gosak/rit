package pl.gosak.rit.recomme.util

import com.recombee.api_client.bindings.Recommendation
import pl.gosak.rit.lib.subject.dto.SubjectPreviewDto
import pl.gosak.rit.recomme.recombee.RecombeeConfiguration.ItemProperties.CHILD_SAFE
import pl.gosak.rit.recomme.recombee.RecombeeConfiguration.ItemProperties.DESCRIPTION
import pl.gosak.rit.recomme.recombee.RecombeeConfiguration.ItemProperties.MANUFACTURER
import pl.gosak.rit.recomme.recombee.RecombeeConfiguration.ItemProperties.NAME
import pl.gosak.rit.recomme.recombee.RecombeeConfiguration.ItemProperties.PHOTO
import pl.gosak.rit.recomme.recombee.RecombeeConfiguration.ItemProperties.PHOTO_DOMINANT_COLOR
import pl.gosak.rit.recomme.recombee.RecombeeConfiguration.ItemProperties.RATE
import java.util.*


fun Recommendation.toSubjectPreviewDto() = SubjectPreviewDto(
        uuid = UUID.fromString(id),
        name = values[NAME] as String,
        description = values[DESCRIPTION] as String,
        manufacturer = values[MANUFACTURER] as String,
        rate = values[RATE] as Double,
        photo = UUID.fromString(values[PHOTO] as String),
        childSafe = values[CHILD_SAFE] as Boolean,
        photoDominantColor = values[PHOTO_DOMINANT_COLOR] as String)

fun Recommendation.isDataCorrect() = run {
    id != null &&
            values[NAME] as String? != null &&
            values[DESCRIPTION] as String? != null &&
            values[MANUFACTURER] as String? != null &&
            values[RATE] as Double? != null &&
            values[PHOTO] as String? != null &&
            values[PHOTO_DOMINANT_COLOR] as String? != null &&
            values[CHILD_SAFE] as Boolean? != null

}

fun Recommendation.print() = run {
    "$id " +
            " ${values[NAME] as String?} " +
            " ${values[DESCRIPTION] as String?} " +
            " ${values[MANUFACTURER] as String?} " +
            " ${values[RATE] as Double?} " +
            " ${values[PHOTO] as String?} " +
            " ${values[PHOTO_DOMINANT_COLOR] as String?} " +
            " ${values[CHILD_SAFE] as Boolean?} "

}