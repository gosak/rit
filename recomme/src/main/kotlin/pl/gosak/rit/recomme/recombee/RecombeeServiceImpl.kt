package pl.gosak.rit.recomme.recombee

import com.recombee.api_client.RecombeeClient
import com.recombee.api_client.api_requests.AddItem
import com.recombee.api_client.api_requests.AddRating
import com.recombee.api_client.api_requests.AddUser
import com.recombee.api_client.api_requests.DeleteItem
import com.recombee.api_client.api_requests.RecommendItemsToUser
import com.recombee.api_client.api_requests.ResetDatabase
import com.recombee.api_client.api_requests.SetItemValues
import com.recombee.api_client.bindings.Recommendation
import org.springframework.stereotype.Service
import pl.gosak.rit.microbase.kafka.message.photo.NewPrimaryPhotoMsg
import pl.gosak.rit.microbase.kafka.message.subject.NewSubjectMsg
import pl.gosak.rit.microbase.kafka.message.subject.NewSubjectRateMsg
import pl.gosak.rit.microbase.kafka.message.user.NewUserMsg
import pl.gosak.rit.microbase.security.User
import pl.gosak.rit.recomme.recombee.RecombeeConfiguration.ItemProperties.CHILD_SAFE
import pl.gosak.rit.recomme.recombee.RecombeeConfiguration.ItemProperties.DESCRIPTION
import pl.gosak.rit.recomme.recombee.RecombeeConfiguration.ItemProperties.MANUFACTURER
import pl.gosak.rit.recomme.recombee.RecombeeConfiguration.ItemProperties.NAME
import pl.gosak.rit.recomme.recombee.RecombeeConfiguration.ItemProperties.PHOTO
import pl.gosak.rit.recomme.recombee.RecombeeConfiguration.ItemProperties.PHOTO_DOMINANT_COLOR
import pl.gosak.rit.recomme.recombee.RecombeeConfiguration.ItemProperties.RATE
import pl.gosak.rit.recomme.recombee.RecombeeConfiguration.ItemProperties.configureRecombee
import pl.gosak.rit.recomme.service.RecommendationService
import java.util.*


@Service
class RecombeeServiceImpl(val recombeeClient: RecombeeClient,
                          val recombeeProperties: RecombeeProperties) : RecommendationService {
    override fun clearDatabase(): String = recombeeClient.run {
        val response = send(ResetDatabase())
        configureRecombee()
        response
    }

    override fun deleteItem(uuid: UUID): String = recombeeClient.send(DeleteItem(uuid.toString()))

    override fun recommendItemsToUser(user: User, count: Long): Array<Recommendation> =
            recombeeClient.send(RecommendItemsToUser(user.userUuid.toString(), count)
                    .setRotationRate(recombeeProperties.rotationRate)
                    .setRotationTime(recombeeProperties.rotationTime)
                    .setReturnProperties(true)).recomms

    override fun recommendItemsIdsToUser(user: User, count: Long): Array<Recommendation> =
            recombeeClient.send(RecommendItemsToUser(user.userUuid.toString(), count)
                    .setRotationRate(recombeeProperties.rotationRate)
                    .setRotationTime(recombeeProperties.rotationTime)).recomms

    override fun addItem(msg: NewSubjectMsg) {
        recombeeClient.apply {
            val itemUuid = msg.subjectUuid.toString()
            send(AddItem(itemUuid))
            recombeeClient.send(SetItemValues(itemUuid, HashMap<String, Any>().apply {
                put(NAME, msg.name)
                put(DESCRIPTION, msg.description)
                put(MANUFACTURER, msg.manufacturer)
                put(RATE, msg.rate)
            }).setCascadeCreate(true))
        }
    }

    override fun addUser(msg: NewUserMsg) {
        recombeeClient.send(AddUser(msg.uuid.toString()))
    }

    override fun addRating(msg: NewSubjectRateMsg) {
        msg.let {
            AddRating(it.userUuid.toString(), it.uuid.toString(), it.userRate).setTimestamp(Date()).setCascadeCreate(true)
        }.also { recombeeClient.send(it) }

        msg.let {
            SetItemValues(it.uuid.toString(), HashMap<String, Any>().apply {
                put(RATE, it.rate)
            }).setCascadeCreate(true)
        }.also { recombeeClient.send(it) }
    }

    override fun addItemPhoto(msg: NewPrimaryPhotoMsg) {
        SetItemValues(msg.subjectUuid.toString(), HashMap<String, Any>().apply {
            put(PHOTO, msg.photoUuid.toString())
            put(PHOTO_DOMINANT_COLOR, msg.photoDominantColor)
            put(CHILD_SAFE, msg.childSafe)
        }).setCascadeCreate(true).also { recombeeClient.send(it) }
    }
}