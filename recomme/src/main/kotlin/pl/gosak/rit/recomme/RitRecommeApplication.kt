package pl.gosak.rit.recomme

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.Import
import pl.gosak.rit.microbase.eureka.EurekaModule
import pl.gosak.rit.microbase.kafka.KafkaModule
import pl.gosak.rit.microbase.security.WebSecurityModule
import pl.gosak.rit.microbase.swagger.SwaggerModule
import pl.gosak.rit.microbase.util.RitUtilsModule

@SpringBootApplication
@Import(RitUtilsModule::class,
        SwaggerModule::class,
        WebSecurityModule::class,
        KafkaModule::class,
        EurekaModule::class)
class RitRecommeApplication {
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            SpringApplication.run(RitRecommeApplication::class.java, *args)
        }
    }
}