package pl.gosak.rit.recomme.controller

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import pl.gosak.rit.microbase.util.responseEntity
import pl.gosak.rit.recomme.exception.SubjectNotFoundException

@ControllerAdvice
class SubjectNotFoundExceptionAdvice {
    @ExceptionHandler(SubjectNotFoundException::class)
    fun handle() = HttpStatus.NOT_FOUND.responseEntity()
}