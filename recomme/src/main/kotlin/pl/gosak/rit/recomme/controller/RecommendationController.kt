package pl.gosak.rit.recomme.controller

import com.recombee.api_client.bindings.Recommendation
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.core.Authentication
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestHeader
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import pl.gosak.rit.lib.SecurityConstants.AUTHORIZATION_HEADER
import pl.gosak.rit.lib.recomme.RecommeEndpoints.Params.COUNT
import pl.gosak.rit.lib.recomme.RecommeEndpoints.RECOMMEND_SUBJECTS_TO_USER
import pl.gosak.rit.lib.recomme.RecommeEndpoints.RECOMMEND_SUBJECTS_UUIDS_TO_USER
import pl.gosak.rit.lib.recomme.dto.RecommendedSubjects
import pl.gosak.rit.lib.recomme.dto.RecommendedSubjectsUuids
import pl.gosak.rit.microbase.security.ritUser
import pl.gosak.rit.microbase.util.responseEntity
import pl.gosak.rit.recomme.service.RecommendationService
import pl.gosak.rit.recomme.util.isDataCorrect
import pl.gosak.rit.recomme.util.print
import pl.gosak.rit.recomme.util.toSubjectPreviewDto


@RestController
class RecommendationController(private val recommendationService: RecommendationService) {
    private val logger = LoggerFactory.getLogger(RecommendationController::class.java)

    @GetMapping(RECOMMEND_SUBJECTS_UUIDS_TO_USER)
    fun recommendSubjectIds(auth: Authentication,
                            @RequestHeader(AUTHORIZATION_HEADER) jwt: String,
                            @RequestParam(COUNT) count: Long): ResponseEntity<RecommendedSubjectsUuids> {
        val recommends = recommendationService.recommendItemsIdsToUser(auth.ritUser(), count)
        val response = RecommendedSubjectsUuids(recommends.map(Recommendation::getId).toTypedArray())
        return HttpStatus.OK.responseEntity(response)
    }

    @GetMapping(RECOMMEND_SUBJECTS_TO_USER)
    fun recommendSubjects(auth: Authentication,
                          @RequestHeader(AUTHORIZATION_HEADER) jwt: String,
                          @RequestParam(COUNT) count: Long): ResponseEntity<RecommendedSubjects> {
        val recommends = recommendationService.recommendItemsToUser(auth.ritUser(), count)
        recommends.forEach { logger.debug(it.print()) }
        val response = RecommendedSubjects(recommends.filter { it.isDataCorrect() }.map { it.toSubjectPreviewDto() }.toTypedArray())
        return HttpStatus.OK.responseEntity(response)
    }
}