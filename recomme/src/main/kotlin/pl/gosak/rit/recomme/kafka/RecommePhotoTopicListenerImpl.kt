package pl.gosak.rit.recomme.kafka

import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Profile
import org.springframework.kafka.annotation.KafkaHandler
import org.springframework.kafka.annotation.KafkaListener
import org.springframework.messaging.handler.annotation.Payload
import org.springframework.stereotype.Component
import pl.gosak.rit.microbase.kafka.KafkaConstants.PHOTO_TOPIC
import pl.gosak.rit.microbase.kafka.KafkaConstants.PHOTO_TOPIC_RECOMME_GROUP
import pl.gosak.rit.microbase.kafka.listener.PhotoTopicListener
import pl.gosak.rit.microbase.kafka.message.photo.NewPhotoMsg
import pl.gosak.rit.microbase.kafka.message.photo.NewPrimaryPhotoMsg
import pl.gosak.rit.recomme.service.RecommendationService

@Component
@Profile("local", "docker")
@KafkaListener(topics = [PHOTO_TOPIC], groupId = PHOTO_TOPIC_RECOMME_GROUP)
class RecommePhotoTopicListenerImpl(private val recommendationService: RecommendationService) : PhotoTopicListener {
    private val logger = LoggerFactory.getLogger(RecommePhotoTopicListenerImpl::class.java)

    @KafkaHandler
    override fun handle(@Payload msg: NewPrimaryPhotoMsg) {
        logger.info("KAFKA_RECOMME: received NewPrimaryPhotoMsg: {}", msg)
        recommendationService.addItemPhoto(msg)
    }

    @KafkaHandler
    override fun handle(@Payload msg: NewPhotoMsg) {
        logger.info("KAFKA_RECOMME: ignore NewPhotoMsg: {}", msg)
    }
}