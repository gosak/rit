package pl.gosak.rit.recomme.service

import pl.gosak.rit.microbase.security.User
import java.util.*

interface UserReactionService {
    fun viewDetails(user: User, subjectUuid: UUID): String
}