package pl.gosak.rit.recomme.recombee

import org.springframework.boot.context.properties.ConfigurationProperties

@ConfigurationProperties(prefix = "recombee")
class RecombeeProperties(var databaseId: String = "",
                         var token: String = "",
                         var rotationRate: Double = 0.0,
                         var rotationTime: Double = 0.0)