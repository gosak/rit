package pl.gosak.rit.recomme.kafka

import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Profile
import org.springframework.kafka.annotation.KafkaHandler
import org.springframework.kafka.annotation.KafkaListener
import org.springframework.messaging.handler.annotation.Payload
import org.springframework.stereotype.Component
import pl.gosak.rit.microbase.kafka.KafkaConstants.SUBJECT_TOPIC
import pl.gosak.rit.microbase.kafka.KafkaConstants.SUBJECT_TOPIC_RECOMME_GROUP
import pl.gosak.rit.microbase.kafka.message.subject.NewSubjectMsg
import pl.gosak.rit.microbase.kafka.message.subject.NewSubjectRateMsg
import pl.gosak.rit.microbase.kafka.listener.SubjectTopicListener
import pl.gosak.rit.recomme.service.RecommendationService

@Component
@Profile("local", "docker")
@KafkaListener(topics = [SUBJECT_TOPIC], groupId = SUBJECT_TOPIC_RECOMME_GROUP)
class RecommeSubjectTopicListenerImpl(private val recommendationService: RecommendationService) : SubjectTopicListener {
    private val logger = LoggerFactory.getLogger(RecommeUserTopicListenerImpl::class.java)

    @KafkaHandler
    override fun handle(@Payload msg: NewSubjectMsg) {
        logger.info("KAFKA_RECOMME: received NewSubjectMsg: {}", msg)
        recommendationService.addItem(msg)
        msg.rate.let {
            recommendationService.addRating(NewSubjectRateMsg(msg.subjectUuid, msg.creatorUuid, it, it))
        }
    }

    @KafkaHandler
    override fun handle(@Payload msg: NewSubjectRateMsg) {
        logger.info("KAFKA_RECOMME: received NewSubjectRateMsg: {}", msg)
        recommendationService.addRating(msg)
    }
}