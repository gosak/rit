package pl.gosak.rit.recomme.controller

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.core.Authentication
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestHeader
import pl.gosak.rit.lib.SecurityConstants.AUTHORIZATION_HEADER
import pl.gosak.rit.lib.recomme.RecommeEndpoints.Paths.SUBJECT_UUID
import pl.gosak.rit.lib.recomme.RecommeEndpoints.VIEW_SUBJECT
import pl.gosak.rit.microbase.security.ritUser
import pl.gosak.rit.microbase.util.responseEntity
import pl.gosak.rit.recomme.service.UserReactionService
import java.util.*


@Controller
class UserReactionsController(private val userReactionService: UserReactionService) {
    @PostMapping(VIEW_SUBJECT)
    fun viewSubject(auth: Authentication,
                    @RequestHeader(AUTHORIZATION_HEADER) jwt: String,
                    @PathVariable(SUBJECT_UUID) uuid: UUID): ResponseEntity<String> {
        return HttpStatus.OK.responseEntity(userReactionService.viewDetails(auth.ritUser(), uuid))
    }
}