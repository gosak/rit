package pl.gosak.rit.recomme.recombee

import com.recombee.api_client.RecombeeClient
import com.recombee.api_client.api_requests.AddItemProperty
import com.recombee.api_client.exceptions.ResponseException
import org.slf4j.LoggerFactory
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary

@Primary
@Configuration
@EnableConfigurationProperties(RecombeeProperties::class)
class RecombeeConfiguration(private val recombeeProperties: RecombeeProperties) {
    @Bean
    fun recombeeClient() = RecombeeClient(recombeeProperties.databaseId, recombeeProperties.token).apply {
        configureRecombee()
    }

    companion object ItemProperties {
        const val NAME = "name"
        const val DESCRIPTION = "description"
        const val MANUFACTURER = "manufacturer"
        const val PHOTO = "photo"
        const val PHOTO_DOMINANT_COLOR = "photoDominantColor"
        const val CHILD_SAFE = "childSafe"
        const val RATE = "rate"

        /**
         * int- Signed integer number.
         * double - Floating point number. It uses 64-bit base-2 format (IEEE 754 standard).
         * string - UTF-8 string.
         * boolean - true / false
         * timestamp - Value representing date and time.
         * set - Set of strings.
         * image - URL of an image (jpeg, png or gif).
         * imageList - List of URLs that refer to images.
         */
        fun RecombeeClient.configureRecombee() {
            val logger = LoggerFactory.getLogger(RecombeeConfiguration::class.java)

            arrayOf(AddItemProperty(NAME, "string"),
                    AddItemProperty(DESCRIPTION, "string"),
                    AddItemProperty(PHOTO, "string"),
                    AddItemProperty(PHOTO_DOMINANT_COLOR, "string"),
                    AddItemProperty(CHILD_SAFE, "boolean"),
                    AddItemProperty(MANUFACTURER, "string"),
                    AddItemProperty(RATE, "double")).forEach {
                try {
                    send(it)
                } catch (ex: ResponseException) {
                    logger.warn("Recombee property ${it.propertyName} already configured.")
                }
            }
        }
    }
}