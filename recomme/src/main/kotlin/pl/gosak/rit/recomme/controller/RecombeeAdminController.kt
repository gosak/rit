package pl.gosak.rit.recomme.controller

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.security.core.Authentication
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestHeader
import org.springframework.web.bind.annotation.RestController
import pl.gosak.rit.lib.SecurityConstants.AUTHORIZATION_HEADER
import pl.gosak.rit.lib.recomme.RecommeEndpoints.PREFIX
import pl.gosak.rit.lib.recomme.RecommeEndpoints.Paths.SUBJECT_UUID
import pl.gosak.rit.microbase.security.Privileges
import pl.gosak.rit.microbase.util.responseEntity
import pl.gosak.rit.recomme.service.RecommendationService
import java.util.*

@RestController
class RecombeeAdminController(private val recommendationService: RecommendationService) {
    @DeleteMapping("$PREFIX/clearDatabase")
    @PreAuthorize("hasAuthority('${Privileges.CLEAR_RECOMME_DATABASE}')")
    fun clearDatabase(auth: Authentication, @RequestHeader(AUTHORIZATION_HEADER) jwt: String): ResponseEntity<String> {
        return HttpStatus.ACCEPTED.responseEntity(recommendationService.clearDatabase())
    }

    @DeleteMapping("$PREFIX/{$SUBJECT_UUID}")
    @PreAuthorize("hasAuthority('${Privileges.DELETE_RECOMME_ITEM}')")
    fun deleteItem(auth: Authentication,
                   @RequestHeader(AUTHORIZATION_HEADER) jwt: String,
                   @PathVariable(SUBJECT_UUID) itemUuid: UUID): ResponseEntity<String> {
        return HttpStatus.NO_CONTENT.responseEntity(recommendationService.deleteItem(itemUuid))
    }
}