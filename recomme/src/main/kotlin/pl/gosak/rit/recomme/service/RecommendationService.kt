package pl.gosak.rit.recomme.service

import com.recombee.api_client.bindings.Recommendation
import pl.gosak.rit.microbase.kafka.message.user.NewUserMsg
import pl.gosak.rit.microbase.kafka.message.photo.NewPrimaryPhotoMsg
import pl.gosak.rit.microbase.kafka.message.subject.NewSubjectMsg
import pl.gosak.rit.microbase.kafka.message.subject.NewSubjectRateMsg
import pl.gosak.rit.microbase.security.User
import java.util.*

interface RecommendationService {
    fun clearDatabase(): String

    fun deleteItem(uuid: UUID): String

    fun recommendItemsIdsToUser(user: User, count: Long): Array<Recommendation>

    fun recommendItemsToUser(user: User, count: Long): Array<Recommendation>

    fun addItem(msg: NewSubjectMsg)

    fun addUser(msg: NewUserMsg)

    fun addRating(msg: NewSubjectRateMsg)

    fun addItemPhoto(msg: NewPrimaryPhotoMsg)
}