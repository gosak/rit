package pl.gosak.rit.recomme.recombee

import com.recombee.api_client.RecombeeClient
import com.recombee.api_client.api_requests.AddDetailView
import org.springframework.stereotype.Service
import pl.gosak.rit.microbase.security.User
import pl.gosak.rit.recomme.exception.SubjectNotFoundException
import pl.gosak.rit.recomme.service.UserReactionService
import java.util.*


@Service
class UserReactionServiceImpl(private val recombeeClient: RecombeeClient) : UserReactionService {
    override fun viewDetails(user: User, subjectUuid: UUID): String {
        return try {
            recombeeClient.send(AddDetailView(user.userUuid.toString(), subjectUuid.toString()))
        } catch (ex: Exception) {
            throw SubjectNotFoundException()
        }
    }
}