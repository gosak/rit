# clear gradle cache
./gradlew clean

# database data
sudo rm -rf ./database/data

# photos
sudo rm -rf ./photo/photos

# remove docker images
docker image rm rit-gateway
docker image rm rit-discovery
docker image rm rit-database
docker image rm rit-recomme
docker image rm rit-search
docker image rm rit-subject
docker image rm rit-auth
docker image rm rit-photo
docker image rm rit-config

#remove docker volumes
docker volume rm rit_elastic-search-data
docker volume prune
