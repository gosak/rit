package pl.gosak.rit.microbase.kafka.service.impl

import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Profile
import org.springframework.kafka.core.KafkaTemplate
import org.springframework.kafka.support.KafkaHeaders
import org.springframework.messaging.support.MessageBuilder
import org.springframework.stereotype.Service
import pl.gosak.rit.microbase.kafka.service.KafkaSender


@Service
@Profile("local", "docker")
class KafkaSenderImpl(private val kafkaTemplate: KafkaTemplate<String, Any>) : KafkaSender {
    private val logger = LoggerFactory.getLogger(KafkaSenderImpl::class.java)

    override fun send(topic: String, msg: Any) {
        logger.info("KAFKA: sending any='{}' to topic='{}'", msg.toString(), topic)

        kafkaTemplate.send(MessageBuilder
                .withPayload(msg)
                .setHeader(KafkaHeaders.TOPIC, topic)
                .build())
    }
}
