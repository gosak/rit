package pl.gosak.rit.microbase.eureka

import org.springframework.context.annotation.ComponentScan

@ComponentScan("pl.gosak.rit.microbase.eureka")
class EurekaModule