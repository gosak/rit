package pl.gosak.rit.microbase.kafka.listener

import pl.gosak.rit.microbase.kafka.message.photo.NewPhotoMsg
import pl.gosak.rit.microbase.kafka.message.photo.NewPrimaryPhotoMsg

interface PhotoTopicListener {
    fun handle(msg: NewPhotoMsg) {}

    fun handle(msg: NewPrimaryPhotoMsg) {}
}