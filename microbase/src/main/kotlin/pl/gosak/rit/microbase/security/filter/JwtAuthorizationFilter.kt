/*
 * Copyright (c) 2018. This file is subject to the terms and conditions defined in file 'LICENSE.txt' which is part of this source code package.
 */

package pl.gosak.rit.microbase.security.filter

import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter
import pl.gosak.rit.lib.SecurityConstants.AUTHORIZATION_HEADER
import pl.gosak.rit.lib.SecurityConstants.TOKEN_PREFIX
import pl.gosak.rit.microbase.security.JwtTokenBody
import pl.gosak.rit.microbase.security.User
import java.util.*
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

class JwtAuthorizationFilter(authManager: AuthenticationManager,
                             private val objectMapper: ObjectMapper,
                             private val base64Decoder: Base64.Decoder) : BasicAuthenticationFilter(authManager) {

    override fun doFilterInternal(request: HttpServletRequest, response: HttpServletResponse, chain: FilterChain) {
        val header = request.getHeader(AUTHORIZATION_HEADER).takeIf { it != null && it.startsWith(TOKEN_PREFIX) }
        if (header == null) {
            chain.doFilter(request, response)
            return
        }

        try {
            SecurityContextHolder.getContext().authentication = getAuthentication(header)
        } catch (ex: Exception) { // need it for logs
            logger.error(ex.message)
            SecurityContextHolder.clearContext()
        }
        chain.doFilter(request, response)
    }

    private fun getAuthentication(header: String): UsernamePasswordAuthenticationToken {
        val token = header.replace(TOKEN_PREFIX, "")
        val untrustedTokenBody = untrustedJwtTokenBody(token)
        val roles = untrustedTokenBody.roles.map { SimpleGrantedAuthority(it) }
        // we don't need check sign of jwt token, it's gateway's job

        val user = User(untrustedTokenBody.userUuid, untrustedTokenBody.deviceUuid, roles)
        return UsernamePasswordAuthenticationToken(user, null, roles)
    }

    private fun untrustedJwtTokenBody(jwtToken: String): JwtTokenBody {
        val payload = jwtToken.split('.')[1]
        return objectMapper.readValue(base64Decoder.decode(payload), JwtTokenBody::class.java)
    }
}