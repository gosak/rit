package pl.gosak.rit.microbase.kafka.message.device

import pl.gosak.rit.microbase.util.NoArgConstructor
import java.util.*

@NoArgConstructor
data class UserDevicesLoggedOutMsg(val userUuid: UUID)