package pl.gosak.rit.microbase.database

import org.postgresql.util.PGobject
import java.sql.SQLException
import java.util.*
import javax.persistence.AttributeConverter
import javax.persistence.Converter


@Converter(autoApply = true)
class UuidConverter : AttributeConverter<UUID, Any> {
    override fun convertToDatabaseColumn(uuid: UUID?): Any =
            PGobject().apply {
                type = "uuid"
                try {
                    value = uuid?.toString()
                } catch (e: SQLException) {
                    throw IllegalArgumentException("Error when creating Postgres uuid", e)
                }
            }

    override fun convertToEntityAttribute(dbData: Any?) = dbData as UUID
}