package pl.gosak.rit.microbase.database

import com.zaxxer.hikari.HikariDataSource
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary
import org.springframework.context.annotation.Profile
import org.springframework.orm.jpa.JpaVendorAdapter
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean
import java.io.PrintWriter
import javax.sql.DataSource


@Configuration
@Profile("local", "docker")
@EnableConfigurationProperties(DatabaseProperties::class)
class DatabaseConfig(private val databaseProperties: DatabaseProperties) {
    @Bean
    @Primary
    @ConfigurationProperties("spring.datasource")
    fun dataSource() = HikariDataSource().apply {
        logWriter = (PrintWriter(System.out))
        isAutoCommit = true
    }

    @Bean
    @Primary
    fun entityManagerFactory(dataSource: DataSource, jpaVendorAdapter: JpaVendorAdapter) = LocalContainerEntityManagerFactoryBean().apply {
        setDataSource(dataSource())
        setJpaVendorAdapter(jpaVendorAdapter)
        setPackagesToScan(databaseProperties.packagesToScan)
    }
}