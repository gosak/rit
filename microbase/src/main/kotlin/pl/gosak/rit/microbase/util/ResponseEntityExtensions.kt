package pl.gosak.rit.microbase.util

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity


fun HttpStatus.responseEntity() = ResponseEntity(reasonPhrase, this)

fun <T>HttpStatus.responseEntity(body: T) = ResponseEntity(body, this)