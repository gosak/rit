package pl.gosak.rit.microbase.kafka.message.user

import pl.gosak.rit.microbase.util.NoArgConstructor
import java.util.*


@NoArgConstructor
data class NewUserMsg(val uuid: UUID)