package pl.gosak.rit.microbase.swagger

import org.springframework.boot.context.properties.ConfigurationProperties

@ConfigurationProperties("swagger")
class SwaggerProperties(var basePackage: String = "",
                        var title: String = "",
                        var description: String = "",
                        var version: String = "")