package pl.gosak.rit.microbase.security

import org.springframework.stereotype.Component
import pl.gosak.rit.microbase.util.WhiteListPathsResolver
import java.util.*

@Component
class DefaultWhiteListPathsResolver(private val securityProperties: SecurityProperties) : WhiteListPathsResolver {
    override fun resolve() = HashSet<String>(securityProperties.whitelist)
}