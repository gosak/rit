package pl.gosak.rit.microbase.swagger

import org.springframework.context.annotation.ComponentScan

@ComponentScan("pl.gosak.rit.microbase.swagger")
class SwaggerModule