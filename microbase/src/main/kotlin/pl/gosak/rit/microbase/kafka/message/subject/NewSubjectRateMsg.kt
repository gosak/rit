package pl.gosak.rit.microbase.kafka.message.subject

import pl.gosak.rit.microbase.util.NoArgConstructor
import java.util.*

@NoArgConstructor
data class NewSubjectRateMsg(val uuid: UUID,
                             val userUuid: UUID,
                             val userRate: Double,
                             val rate: Double)