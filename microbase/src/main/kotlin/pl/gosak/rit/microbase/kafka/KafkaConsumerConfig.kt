package pl.gosak.rit.microbase.kafka

import org.apache.kafka.clients.consumer.ConsumerConfig
import org.apache.kafka.common.serialization.StringDeserializer
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile
import org.springframework.kafka.annotation.EnableKafka
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory
import org.springframework.kafka.core.ConsumerFactory
import org.springframework.kafka.core.DefaultKafkaConsumerFactory
import org.springframework.kafka.support.serializer.JsonDeserializer
import java.util.*


@EnableKafka
@Configuration
@Profile("local", "docker")
class KafkaConsumerConfig {
    @Value("\${spring.kafka.bootstrap-servers}")
    private lateinit var bootstrapServers: String

    @Bean
    fun consumerConfigs() = HashMap<String, Any>().apply {
        this[ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG] = bootstrapServers
        this[ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG] = StringDeserializer::class.java
        this[ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG] = JsonDeserializer::class.java
    }

    @Bean
    fun valueDeserializer() = JsonDeserializer<Any>().apply {
        addTrustedPackages("pl.gosak.rit.microbase.kafka.message.device")
        addTrustedPackages("pl.gosak.rit.microbase.kafka.message.photo")
        addTrustedPackages("pl.gosak.rit.microbase.kafka.message.subject")
        addTrustedPackages("pl.gosak.rit.microbase.kafka.message.user")
    }

    @Bean
    fun consumerFactory(): ConsumerFactory<String, Any> = consumerConfigs().run {
        DefaultKafkaConsumerFactory<String, Any>(this, StringDeserializer(), valueDeserializer())
    }

    @Bean
    fun kafkaListenerContainerFactory() = ConcurrentKafkaListenerContainerFactory<String, Any>().apply {
        consumerFactory = consumerFactory()
    }
}
