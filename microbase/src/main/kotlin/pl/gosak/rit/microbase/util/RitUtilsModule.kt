package pl.gosak.rit.microbase.util

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import org.modelmapper.ModelMapper
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder
import org.springframework.web.client.RestTemplate
import java.util.*

@Configuration
@ComponentScan("pl.gosak.rit.microbase.util")
class RitUtilsModule {
    @Bean
    @Primary
    fun modelMapper() = ModelMapper()

    @Bean
    @Primary
    fun restTemplate() = RestTemplate()

    @Bean
    fun base64Decoder(): Base64.Decoder = Base64.getDecoder()

    @Bean
    @Primary
    fun jsonObjectMapper(): ObjectMapper = Jackson2ObjectMapperBuilder.json()
            .modules(JavaTimeModule())
            .build()
}