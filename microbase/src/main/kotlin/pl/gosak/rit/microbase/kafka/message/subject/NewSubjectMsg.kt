package pl.gosak.rit.microbase.kafka.message.subject

import pl.gosak.rit.microbase.util.NoArgConstructor
import java.util.*

@NoArgConstructor
data class NewSubjectMsg(val subjectUuid: UUID,
                         val manufacturer: String,
                         val name: String,
                         val description: String,
                         val rate: Double,
                         val creatorUuid: UUID)