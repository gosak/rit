package pl.gosak.rit.microbase.kafka.listener

import pl.gosak.rit.microbase.kafka.message.user.NewUserMsg

interface UserTopicListener {
    fun handle(msg: NewUserMsg)
}