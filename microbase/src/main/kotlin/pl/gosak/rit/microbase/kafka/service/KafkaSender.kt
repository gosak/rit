package pl.gosak.rit.microbase.kafka.service

interface KafkaSender {
    fun send(topic: String, msg: Any)
}