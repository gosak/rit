/*
 * Copyright (c) 2018 This file is subject to the terms and conditions defined in file 'LICENSE.txt' which is part of this source code package.
 */

package pl.gosak.rit.microbase.security

import org.springframework.security.core.Authentication

object Roles {
    const val RECOMME_ADMIN = "RECOMME_ADMIN"
    const val ROLE_RECOMME_ADMIN = "ROLE_RECOMME_ADMIN"
}

object Privileges {
    const val CLEAR_RECOMME_DATABASE = "CLEAR_RECOMME_DATABASE_PRIVILEGE"
    const val DELETE_RECOMME_ITEM = "DELETE_RECOMME_ITEM_PRIVILEGE"
}

fun Authentication.ritUser(): User = principal as User