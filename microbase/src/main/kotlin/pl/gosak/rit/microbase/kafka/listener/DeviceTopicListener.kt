package pl.gosak.rit.microbase.kafka.listener

import pl.gosak.rit.microbase.kafka.message.device.DeviceLoggedMsg
import pl.gosak.rit.microbase.kafka.message.device.DeviceLoggedOutMsg
import pl.gosak.rit.microbase.kafka.message.device.UserDevicesLoggedOutMsg

interface DeviceTopicListener {
    fun handle(msg: DeviceLoggedMsg) {}

    fun handle(msg: DeviceLoggedOutMsg) {}

    fun handle(msg: UserDevicesLoggedOutMsg) {}
}