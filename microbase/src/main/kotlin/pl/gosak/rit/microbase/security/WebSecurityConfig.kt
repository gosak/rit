/*
 * Copyright (c) 2018. This file is subject to the terms and conditions defined in file 'LICENSE.txt' which is part of this source code package.
 */

package pl.gosak.rit.microbase.security

import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpMethod
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.web.cors.CorsConfiguration
import org.springframework.web.cors.UrlBasedCorsConfigurationSource
import pl.gosak.rit.microbase.security.filter.JwtAuthorizationFilter
import pl.gosak.rit.microbase.util.WhiteListPathsResolver
import java.util.*


@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
@EnableConfigurationProperties(SecurityProperties::class)
class WebSecurityConfig(private val base64Decoder: Base64.Decoder,
                        private val jacksonObjectMapper: ObjectMapper,
                        private val whiteListPathsResolver: WhiteListPathsResolver) : WebSecurityConfigurerAdapter() {
    override fun configure(http: HttpSecurity) {
        val authorizationFilter = JwtAuthorizationFilter(authenticationManager(), jacksonObjectMapper, base64Decoder)
        http.cors().and().csrf().disable().authorizeRequests().antMatchers(*whiteListPathsResolver.resolve().toTypedArray()).permitAll()
                .anyRequest().authenticated()
                .and()
                .addFilter(authorizationFilter)
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
    }

    @Bean
    fun authManager(): AuthenticationManager = authenticationManager()

    @Bean
    fun corsConfigurationSource() = UrlBasedCorsConfigurationSource().apply {
        registerCorsConfiguration("/**", CorsConfiguration().applyPermitDefaultValues().apply {
            allowedMethods = mutableListOf(HttpMethod.GET, HttpMethod.POST, HttpMethod.PUT,
                    HttpMethod.DELETE, HttpMethod.HEAD).map(HttpMethod::name)
        })
    }
}