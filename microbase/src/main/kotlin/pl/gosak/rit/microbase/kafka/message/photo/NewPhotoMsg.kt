package pl.gosak.rit.microbase.kafka.message.photo

import pl.gosak.rit.microbase.util.NoArgConstructor
import java.util.*


@NoArgConstructor
data class NewPhotoMsg(val uuid: UUID,
                       val subjectUuid: UUID,
                       val childSafe: Boolean,
                       val dominantColor: String)