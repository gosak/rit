package pl.gosak.rit.microbase.kafka.message.device

import pl.gosak.rit.microbase.util.NoArgConstructor
import java.util.*

@NoArgConstructor
data class DeviceLoggedMsg(val userUuid: UUID,
                           val deviceUuid: UUID,
                           val refreshSecret: String,
                           val resourceSecret: String)