package pl.gosak.rit.microbase.database

import org.springframework.boot.context.properties.ConfigurationProperties

@ConfigurationProperties("database")
class DatabaseProperties(var packagesToScan: String = "")