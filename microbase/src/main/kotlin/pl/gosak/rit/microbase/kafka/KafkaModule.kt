package pl.gosak.rit.microbase.kafka

import org.springframework.context.annotation.ComponentScan


@ComponentScan("pl.gosak.rit.microbase.kafka")
class KafkaModule