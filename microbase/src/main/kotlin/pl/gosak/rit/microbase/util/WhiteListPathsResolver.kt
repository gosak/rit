package pl.gosak.rit.microbase.util

interface WhiteListPathsResolver {
    fun resolve(): Set<String>
}