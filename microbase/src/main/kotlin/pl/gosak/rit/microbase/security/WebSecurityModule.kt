package pl.gosak.rit.microbase.security

import org.springframework.context.annotation.ComponentScan

@ComponentScan("pl.gosak.rit.microbase.security")
class WebSecurityModule