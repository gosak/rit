package pl.gosak.rit.microbase.kafka

object KafkaConstants {
    /**
     * Device topic and receivers groups for:
     * @see pl.gosak.rit.microbase.kafka.message.device.DeviceLoggedMsg
     * @see pl.gosak.rit.microbase.kafka.message.device.DeviceLoggedOutMsg
     * @see pl.gosak.rit.microbase.kafka.message.device.UserDevicesLoggedOutMsg
     */
    const val DEVICE_TOPIC = "deviceTopic"
    const val DEVICE_TOPIC_GATEWAY_GROUP = "${DEVICE_TOPIC}GatewayGroup"

    /**
     * User topic and receivers groups for:
     * @see pl.gosak.rit.microbase.kafka.message.user.NewUserMsg
     */
    const val USER_TOPIC = "userTopic"
    const val USER_TOPIC_RECOMME_GROUP = "${USER_TOPIC}RecommeGroup"

    /**
     * Subject topic and receivers groups for:
     * @see pl.gosak.rit.microbase.kafka.message.subject.NewSubjectMsg
     * @see pl.gosak.rit.microbase.kafka.message.subject.NewSubjectRateMsg
     **/
    const val SUBJECT_TOPIC = "subjectTopic"
    const val SUBJECT_TOPIC_PHOTO_GROUP = "${SUBJECT_TOPIC}PhotoGroup"
    const val SUBJECT_TOPIC_SEARCH_GROUP = "${SUBJECT_TOPIC}SearchGroup"
    const val SUBJECT_TOPIC_RECOMME_GROUP = "${SUBJECT_TOPIC}RecommeGroup"

    /**
     * Photo topic and receivers groups for:
     * @see pl.gosak.rit.microbase.kafka.message.photo.NewPhotoMsg
     * @see pl.gosak.rit.microbase.kafka.message.photo.NewPrimaryPhotoMsg
     **/
    const val PHOTO_TOPIC = "photoTopic"
    const val PHOTO_TOPIC_SEARCH_GROUP = "${PHOTO_TOPIC}SearchGroup"
    const val PHOTO_TOPIC_SUBJECT_GROUP = "${PHOTO_TOPIC}SubjectGroup"
    const val PHOTO_TOPIC_RECOMME_GROUP = "${PHOTO_TOPIC}RecommeGroup"
}