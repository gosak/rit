package pl.gosak.rit.microbase.kafka.listener

import pl.gosak.rit.microbase.kafka.message.subject.NewSubjectMsg
import pl.gosak.rit.microbase.kafka.message.subject.NewSubjectRateMsg

interface SubjectTopicListener {
    fun handle(msg: NewSubjectMsg) {}

    fun handle(msg: NewSubjectRateMsg) {}
}