package pl.gosak.rit.microbase.kafka.message.photo

import pl.gosak.rit.microbase.util.NoArgConstructor
import java.util.*


@NoArgConstructor
data class NewPrimaryPhotoMsg(val subjectUuid: UUID,
                              val photoUuid: UUID,
                              val photoDominantColor: String,
                              val childSafe: Boolean)