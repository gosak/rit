/*
 * Copyright (c) 2018 This file is subject to the terms and conditions defined in file 'LICENSE.txt' which is part of this source code package.
 */

package pl.gosak.rit.microbase.util

import org.modelmapper.ModelMapper
import org.springframework.http.HttpStatus

inline fun <reified T> ModelMapper.postResponse(source: Any?) =
        if (source == null)
            HttpStatus.CONFLICT.responseEntity()
        else
            HttpStatus.CREATED.responseEntity(map(source, T::class.java))

inline fun <reified T> ModelMapper.getResponse(source: Any?) =
        if (source == null)
            HttpStatus.NOT_FOUND.responseEntity()
        else
            HttpStatus.OK.responseEntity(map(source, T::class.java))

inline fun <reified T> ModelMapper.putResponse(source: Any?) =
        if (source == null)
            HttpStatus.BAD_REQUEST.responseEntity()
        else
            HttpStatus.NO_CONTENT.responseEntity(map(source, T::class.java))

inline fun <reified T> ModelMapper.deleteResponse(source: Any?) =
        if (source == null)
            HttpStatus.BAD_REQUEST.responseEntity()
        else
            HttpStatus.NO_CONTENT.responseEntity(map(source, T::class.java))