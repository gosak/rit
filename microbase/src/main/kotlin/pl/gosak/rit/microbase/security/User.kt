/*
 * Copyright (c) 2018. This file is subject to the terms and conditions defined in file 'LICENSE.txt' which is part of this source code package.
 */

package pl.gosak.rit.microbase.security

import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import java.util.*

class User(val userUuid: UUID,
           val deviceUuid: UUID,
           private val grantedAuthorities: List<GrantedAuthority>) : UserDetails {
    override fun getUsername() = deviceUuid.toString()

    override fun isCredentialsNonExpired() = true

    override fun isAccountNonExpired() = true

    override fun isAccountNonLocked() = true

    override fun isEnabled() = true

    override fun getPassword() = null

    override fun getAuthorities() = grantedAuthorities
}