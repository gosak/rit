package pl.gosak.rit.microbase.security

import org.springframework.boot.context.properties.ConfigurationProperties

@ConfigurationProperties("security")
class SecurityProperties(var whitelist: List<String> = ArrayList() )