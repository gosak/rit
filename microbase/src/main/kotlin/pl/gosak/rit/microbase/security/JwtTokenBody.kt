package pl.gosak.rit.microbase.security

import java.util.*

class JwtTokenBody(
        val userUuid: UUID = UUID(0, 0),
        val deviceUuid: UUID = UUID(0, 0),
        val exp: Long = -1,
        val roles: Array<String> = arrayOf()
)