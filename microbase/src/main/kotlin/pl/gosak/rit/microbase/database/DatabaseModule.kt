package pl.gosak.rit.microbase.database

import org.springframework.context.annotation.ComponentScan

@ComponentScan("pl.gosak.rit.microbase.database")
class DatabaseModule