package pl.gosak.rit.microbase.kafka.service.impl

import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Service
import pl.gosak.rit.microbase.kafka.service.KafkaSender

@Service
@Profile("standalone")
class KafkaSenderStandaloneImpl : KafkaSender {
    private val logger = LoggerFactory.getLogger(KafkaSenderImpl::class.java)

    override fun send(topic: String, msg: Any) {
        logger.info("KAFKA_STANDALONE: sending any='{}' to topic='{}'", msg.toString(), topic)
    }
}