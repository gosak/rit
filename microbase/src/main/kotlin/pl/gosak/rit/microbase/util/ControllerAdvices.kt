package pl.gosak.rit.microbase.util

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import javax.persistence.EntityNotFoundException

@ControllerAdvice
class EntityNotFoundExceptionAdvice {
    @ExceptionHandler(EntityNotFoundException::class)
    fun handle() = HttpStatus.NOT_FOUND.responseEntity()
}