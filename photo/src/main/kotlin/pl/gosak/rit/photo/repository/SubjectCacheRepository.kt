package pl.gosak.rit.photo.repository

import pl.gosak.rit.photo.model.SubjectCache
import java.util.*

interface SubjectCacheRepository {
    fun persist(subjectCache: SubjectCache)

    fun load(subjectUuid: UUID): SubjectCache
}