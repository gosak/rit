package pl.gosak.rit.photo.service

import org.springframework.core.io.UrlResource
import java.util.*

interface DownloadService {
    fun getPhoto(photoUuid: UUID, size: String): UrlResource
}