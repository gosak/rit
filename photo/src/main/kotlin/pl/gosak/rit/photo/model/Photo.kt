package pl.gosak.rit.photo.model

import pl.gosak.rit.photo.model.Photo.Names.TABLE
import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table


@Entity
@Table(name = TABLE)
class Photo(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        val id: Long,

        @Column(name = DB_UUID, nullable = false)
        val uuid: UUID,

        @Column(name = DB_CREATOR_UUID, nullable = false)
        val creatorUuid: UUID,

        @Column(name = DB_CHILD_SAFE, nullable = false)
        var childSafe: Boolean,

        @Column(name = DB_DOMINANT_COLOR, nullable = false)
        var dominantColor: String
) {
    constructor(userUuid: UUID, childSafe: Boolean, dominantColor: String) : this(0, UUID.randomUUID(), userUuid, childSafe, dominantColor)

    companion object Names {
        const val TABLE = "photo"
        const val ID = "id"
        const val DB_UUID = "uuid"
        const val CHILD_SAFE = "childSafe"
        const val CREATOR_UUID = "creatorUuid"
        const val DB_CREATOR_UUID = "creator_uuid"
        const val DB_CHILD_SAFE = "child_safe"
        const val DOMINANT_COLOR = "dominantColor"
        const val DB_DOMINANT_COLOR = "dominant_color"
    }
}