package pl.gosak.rit.photo.property

import org.springframework.boot.context.properties.ConfigurationProperties

@ConfigurationProperties(prefix = "thumbnail")
class PhotoThumbnailProperties(
        var height: Int = 128,
        var width: Int = 128,
        var quality: Double = 0.9)