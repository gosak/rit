package pl.gosak.rit.photo.controller

import org.springframework.core.io.Resource
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestHeader
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import pl.gosak.rit.lib.SecurityConstants.AUTHORIZATION_HEADER
import pl.gosak.rit.lib.photo.PhotoEndpoints.PHOTO_DOWNLOAD
import pl.gosak.rit.lib.photo.PhotoEndpoints.Params.SIZE
import pl.gosak.rit.lib.photo.PhotoEndpoints.Paths.PHOTO_UUID
import pl.gosak.rit.lib.photo.PhotoSizes.ORIGIN
import pl.gosak.rit.photo.service.DownloadService
import java.util.*


@RestController
class DownloadController(private val downloadService: DownloadService) {
    @GetMapping(PHOTO_DOWNLOAD)
    fun downloadPhoto(@PathVariable(PHOTO_UUID) photoUuid: UUID,
                      @RequestParam(SIZE, required = false) size: String?,
                      @RequestHeader(AUTHORIZATION_HEADER) jwt: String) =
            response(photoUuid, size ?: ORIGIN.name)

    private fun response(photoUuid: UUID, size: String): ResponseEntity<Resource> =
            downloadService.getPhoto(photoUuid, size).let {
                ResponseEntity.ok()
                        .contentType(MediaType.IMAGE_JPEG)
                        .header(HttpHeaders.CONTENT_DISPOSITION)
                        .body(it)
            }
}
