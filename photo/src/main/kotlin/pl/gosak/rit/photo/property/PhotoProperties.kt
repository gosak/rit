package pl.gosak.rit.photo.property

import org.springframework.boot.context.properties.ConfigurationProperties

@ConfigurationProperties(prefix = "photo")
class PhotoProperties(var dir: String = "")