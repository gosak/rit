package pl.gosak.rit.photo

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import pl.gosak.rit.microbase.database.DatabaseModule
import pl.gosak.rit.microbase.eureka.EurekaModule
import pl.gosak.rit.microbase.kafka.KafkaModule
import pl.gosak.rit.microbase.security.WebSecurityModule
import pl.gosak.rit.microbase.swagger.SwaggerModule
import pl.gosak.rit.microbase.util.RitUtilsModule
import pl.gosak.rit.photo.property.PhotoProperties
import pl.gosak.rit.photo.property.PhotoThumbnailProperties
import pl.gosak.rit.photo.property.SightEngineProperties

@SpringBootApplication
@Import(RitUtilsModule::class,
        SwaggerModule::class,
        WebSecurityModule::class,
        DatabaseModule::class,
        KafkaModule::class,
        EurekaModule::class)
class RitPhotoApplication {
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            SpringApplication.run(RitPhotoApplication::class.java, *args)
        }
    }
}

@Configuration
@EnableConfigurationProperties(PhotoProperties::class)
class PhotoPropertiesConfiguration

@Configuration
@EnableConfigurationProperties(SightEngineProperties::class)
class SightEnginePropertiesConfiguration

@Configuration
@EnableConfigurationProperties(PhotoThumbnailProperties::class)
class PhotoThumbnailPropertiesConfiguration