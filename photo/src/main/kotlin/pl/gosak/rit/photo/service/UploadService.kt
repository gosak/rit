package pl.gosak.rit.photo.service

import org.springframework.web.multipart.MultipartFile
import pl.gosak.rit.microbase.security.User
import pl.gosak.rit.photo.model.Photo
import java.util.*

interface UploadService {
    fun upload(user: User, subjectUuid: UUID, file: MultipartFile): Photo
}