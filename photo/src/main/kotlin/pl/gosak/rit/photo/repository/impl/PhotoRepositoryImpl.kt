package pl.gosak.rit.photo.repository.impl

import org.springframework.stereotype.Repository
import pl.gosak.rit.microbase.util.findBy
import pl.gosak.rit.photo.model.Photo
import pl.gosak.rit.photo.repository.PhotoRepository
import java.util.*
import javax.persistence.EntityManager
import javax.persistence.EntityNotFoundException
import javax.persistence.PersistenceContext

@Repository
class PhotoRepositoryImpl : PhotoRepository {
    @PersistenceContext
    lateinit var entityManager: EntityManager

    override fun add(photo: Photo) = photo.apply {
        entityManager.persist(this)
    }

    override fun find(photoUuid: UUID): Photo = entityManager.findBy(Photo.DB_UUID, photoUuid)
            ?: throw EntityNotFoundException("Photo $photoUuid not found")

    override fun delete(photo: Photo) = entityManager.contains(photo).apply {
        entityManager.remove(if (this) photo else entityManager.merge(photo))
    }
}