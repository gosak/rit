package pl.gosak.rit.photo.service.impl

import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Service
import org.springframework.web.multipart.MultipartFile
import pl.gosak.rit.photo.model.PhotoJudgment
import pl.gosak.rit.photo.service.ChildSafeArbiter

@Service
@Profile("standalone")
class ChildSafeArbiterStandaloneImpl : ChildSafeArbiter {
    override fun judge(file: MultipartFile) = PhotoJudgment(true, true)
}