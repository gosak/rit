package pl.gosak.rit.photo.service.impl

import de.androidpit.colorthief.ColorThief
import org.springframework.stereotype.Service
import pl.gosak.rit.photo.service.PhotoDominantColorResolver
import java.awt.image.BufferedImage


@Service
class PhotoDominantColorResolverImpl : PhotoDominantColorResolver {
    override fun dominant(bufferedImage: BufferedImage): String {
        return ColorThief.getColor(bufferedImage).let {
            "#" + Integer.toHexString(it[0]) + Integer.toHexString(it[1]) + Integer.toHexString(it[2])
        }
    }
}