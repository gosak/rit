package pl.gosak.rit.photo.service.impl

import org.springframework.core.io.UrlResource
import org.springframework.stereotype.Service
import pl.gosak.rit.photo.property.PhotoProperties
import pl.gosak.rit.photo.repository.PhotoRepository
import pl.gosak.rit.photo.service.DownloadService
import java.nio.file.Paths
import java.util.*


@Service
class DownloadServiceImpl(photoProperties: PhotoProperties) : DownloadService {
    private val photosLocation = Paths.get(photoProperties.dir).toAbsolutePath().normalize()

    override fun getPhoto(photoUuid: UUID, size: String) =
            UrlResource(photosLocation.resolve("${photoUuid}_$size.jpg").normalize().toUri())

}