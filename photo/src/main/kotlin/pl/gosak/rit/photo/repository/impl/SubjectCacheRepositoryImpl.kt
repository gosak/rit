package pl.gosak.rit.photo.repository.impl

import org.springframework.stereotype.Repository
import pl.gosak.rit.microbase.util.findBy
import pl.gosak.rit.photo.model.SubjectCache
import pl.gosak.rit.photo.repository.SubjectCacheRepository
import java.util.*
import javax.persistence.EntityManager
import javax.persistence.EntityNotFoundException
import javax.persistence.PersistenceContext


@Repository
class SubjectCacheRepositoryImpl : SubjectCacheRepository {
    @PersistenceContext
    private lateinit var entityManager: EntityManager

    override fun persist(subjectCache: SubjectCache) = entityManager.persist(subjectCache)

    override fun load(subjectUuid: UUID): SubjectCache = entityManager.findBy(SubjectCache.SUBJECT_UUID, subjectUuid)
            ?: throw EntityNotFoundException("SubjectCache with uuid=$subjectUuid not found")
}