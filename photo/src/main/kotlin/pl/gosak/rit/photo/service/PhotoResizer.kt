package pl.gosak.rit.photo.service

import java.awt.image.BufferedImage

interface PhotoResizer {
    val prefix: String

    fun resize(bufferedImage: BufferedImage): BufferedImage
}