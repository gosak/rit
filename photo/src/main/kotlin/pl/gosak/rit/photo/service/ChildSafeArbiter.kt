package pl.gosak.rit.photo.service

import org.springframework.web.multipart.MultipartFile
import pl.gosak.rit.photo.model.PhotoJudgment

interface ChildSafeArbiter {
    fun judge(file: MultipartFile): PhotoJudgment
}