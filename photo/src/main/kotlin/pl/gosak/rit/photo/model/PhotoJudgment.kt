package pl.gosak.rit.photo.model

class PhotoJudgment(val childSafe: Boolean,
                    val acceptable: Boolean)