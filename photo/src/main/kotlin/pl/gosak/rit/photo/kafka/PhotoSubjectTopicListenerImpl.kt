package pl.gosak.rit.photo.kafka

import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Profile
import org.springframework.kafka.annotation.KafkaHandler
import org.springframework.kafka.annotation.KafkaListener
import org.springframework.messaging.handler.annotation.Payload
import org.springframework.stereotype.Component
import pl.gosak.rit.microbase.kafka.KafkaConstants.SUBJECT_TOPIC
import pl.gosak.rit.microbase.kafka.KafkaConstants.SUBJECT_TOPIC_PHOTO_GROUP
import pl.gosak.rit.microbase.kafka.listener.SubjectTopicListener
import pl.gosak.rit.microbase.kafka.message.subject.NewSubjectMsg
import pl.gosak.rit.microbase.kafka.message.subject.NewSubjectRateMsg
import pl.gosak.rit.photo.model.SubjectCache
import pl.gosak.rit.photo.service.SubjectCacheService


@Component
@Profile("local", "docker")
@KafkaListener(topics = [SUBJECT_TOPIC], groupId = SUBJECT_TOPIC_PHOTO_GROUP)
class PhotoSubjectTopicListenerImpl(private val subjectCacheService: SubjectCacheService) : SubjectTopicListener {
    private val logger = LoggerFactory.getLogger(PhotoSubjectTopicListenerImpl::class.java)

    @KafkaHandler
    override fun handle(@Payload msg: NewSubjectMsg) {
        logger.info("KAFKA_PHOTO: received NewSubjectMsg: {}", msg)
        subjectCacheService.create(SubjectCache(msg.subjectUuid))
    }

    @KafkaHandler
    override fun handle(@Payload msg: NewSubjectRateMsg) {
        logger.info("KAFKA_PHOTO: ignore NewSubjectRateMsg: {}", msg)
    }
}