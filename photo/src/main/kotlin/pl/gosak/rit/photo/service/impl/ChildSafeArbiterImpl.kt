package pl.gosak.rit.photo.service.impl

import org.springframework.context.annotation.Profile
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.stereotype.Component
import org.springframework.stereotype.Service
import org.springframework.util.LinkedMultiValueMap
import org.springframework.util.MultiValueMap
import org.springframework.web.client.RestClientException
import org.springframework.web.client.RestTemplate
import org.springframework.web.multipart.MultipartFile
import pl.gosak.rit.photo.model.PhotoJudgment
import pl.gosak.rit.photo.model.SightEngineResponse
import pl.gosak.rit.photo.property.SightEngineProperties
import pl.gosak.rit.photo.service.ChildSafeArbiter
import pl.gosak.rit.photo.util.sightEngineCall


@Service
@Profile("docker")
class ChildSafeArbiterImpl(private val sightEngineProperties: SightEngineProperties,
                           private val restTemplate: RestTemplate) : ChildSafeArbiter {
    override fun judge(file: MultipartFile) =
            try {
                sightEngineJudge(file).let {
                    val childSafe: Boolean = it.alcohol < 0.2f && it.drugs < 0.2f && it.weapon < 0.2f
                    val acceptable: Boolean = it.nudity.safe > 0.8 && it.offensive.prob < 0.2
                    PhotoJudgment(childSafe && acceptable, acceptable)
                }
            } catch (ex: RestClientException) {
                PhotoJudgment(false, false)
            }

    private fun sightEngineJudge(file: MultipartFile): SightEngineResponse =
            restTemplate.sightEngineCall(sightEngineProperties.addr, sightEngineRequest(file))

    private fun sightEngineRequest(file: MultipartFile) =
            LinkedMultiValueMap<String, Any>().let {
                it["api_user"] = sightEngineProperties.user
                it["api_secret"] = sightEngineProperties.secret
                it["models"] = models
                it["media"] = file.resource
                HttpEntity<MultiValueMap<String, Any>>(it, HttpHeaders())
            }

    companion object {
        const val models = "nudity,wad,offensive"
    }
}