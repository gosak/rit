package pl.gosak.rit.photo.service.impl

import net.coobird.thumbnailator.Thumbnails
import net.coobird.thumbnailator.geometry.Positions
import org.springframework.stereotype.Service
import pl.gosak.rit.lib.photo.PhotoSizes.THUMBNAIL
import pl.gosak.rit.photo.property.PhotoThumbnailProperties
import pl.gosak.rit.photo.service.PhotoResizer
import java.awt.image.BufferedImage


@Service
class ThumbnailPhotoResizer(private val photoThumbnailProperties: PhotoThumbnailProperties) : PhotoResizer {
    override val prefix = THUMBNAIL.name

    override fun resize(bufferedImage: BufferedImage): BufferedImage =
            Thumbnails.of(bufferedImage)
                    .crop(Positions.CENTER)
                    .size(photoThumbnailProperties.width, photoThumbnailProperties.height)
                    .keepAspectRatio(true)
                    .outputQuality(photoThumbnailProperties.quality)
                    .asBufferedImage()


    fun dupa(bufferedImage: BufferedImage) {

    }
}