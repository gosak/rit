package pl.gosak.rit.photo.service.impl

import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.springframework.stereotype.Service
import org.springframework.web.multipart.MultipartFile
import pl.gosak.rit.lib.photo.PhotoSizes.ORIGIN
import pl.gosak.rit.microbase.kafka.KafkaConstants.PHOTO_TOPIC
import pl.gosak.rit.microbase.kafka.message.photo.NewPhotoMsg
import pl.gosak.rit.microbase.kafka.service.KafkaSender
import pl.gosak.rit.microbase.security.User
import pl.gosak.rit.photo.exception.FileTypeNotAcceptableException
import pl.gosak.rit.photo.exception.PhotoNotAcceptableException
import pl.gosak.rit.photo.model.Photo
import pl.gosak.rit.photo.property.PhotoProperties
import pl.gosak.rit.photo.repository.PhotoRepository
import pl.gosak.rit.photo.service.ChildSafeArbiter
import pl.gosak.rit.photo.service.PhotoDominantColorResolver
import pl.gosak.rit.photo.service.PhotoResizer
import pl.gosak.rit.photo.service.SubjectCacheService
import pl.gosak.rit.photo.service.UploadService
import java.awt.Color
import java.awt.image.BufferedImage
import java.nio.file.Files
import java.nio.file.Paths
import java.util.*
import javax.imageio.ImageIO
import javax.transaction.Transactional


@Service
class UploadServiceImpl(private var photoRepository: PhotoRepository,
                        private var childSafeArbiter: ChildSafeArbiter,
                        private var photoResizer: PhotoResizer,
                        private var subjectCacheService: SubjectCacheService,
                        private var kafkaSender: KafkaSender,
                        private var photoDominantColorResolver: PhotoDominantColorResolver,
                        photoProperties: PhotoProperties) : UploadService {
    private val photosLocation = Paths.get(photoProperties.dir).toAbsolutePath().normalize()

    init {
        if (Files.notExists(this.photosLocation)) {
            Files.createDirectories(this.photosLocation)
        }
    }

    @Transactional
    override fun upload(user: User, subjectUuid: UUID, file: MultipartFile): Photo {
        subjectCacheService.load(subjectUuid) // if subject does not exists then exception will be thrown and caught by controller advice
        checkFileType(file)
        val dominantColor = photoDominantColorResolver.dominant(ImageIO.read(file.inputStream))

        return Photo(user.userUuid, true, dominantColor).also {
            val judgment = judgePhoto(file)

            runBlocking {
                launch { storeOriginal(file, it.uuid) }
                launch { storeThumbnail(file, it.uuid) }
                it.childSafe = judgment.childSafe
                photoRepository.add(it)
                kafkaSender.send(PHOTO_TOPIC, NewPhotoMsg(it.uuid, subjectUuid, it.childSafe, it.dominantColor))
            }
        }
    }

    private fun judgePhoto(file: MultipartFile) =
            childSafeArbiter.judge(file).apply {
                if (!acceptable)
                    throw PhotoNotAcceptableException()
            }

    private fun checkFileType(file: MultipartFile) {
        if (file.contentType != IMAGE_JPEG && file.contentType != IMAGE_PNG && file.contentType != IMAGE_JPG)
            throw FileTypeNotAcceptableException()
    }

    private fun storeOriginal(file: MultipartFile, uuid: UUID) {
        var originPhoto = ImageIO.read(file.inputStream)
        if (file.contentType == IMAGE_PNG) {
            originPhoto = anyToJpg(originPhoto)
        }
        storePhoto(originPhoto, "${uuid}_${ORIGIN.name}")
    }

    private fun storeThumbnail(file: MultipartFile, uuid: UUID) {
        val photo = anyToJpg(ImageIO.read(file.inputStream))
        storePhoto(photoResizer.resize(photo), "${uuid}_${photoResizer.prefix}")
    }

    private fun storePhoto(bufferedImage: BufferedImage, name: String) {
        ImageIO.write(bufferedImage, "jpg", photosLocation.resolve("$name.jpg").toFile())
    }

    private fun anyToJpg(bufferedImage: BufferedImage) =
            BufferedImage(bufferedImage.width, bufferedImage.height, BufferedImage.TYPE_INT_RGB).apply {
                createGraphics().drawImage(bufferedImage, 0, 0, Color.WHITE, null)
            }

    companion object {
        const val IMAGE_JPEG = "image/jpeg"
        const val IMAGE_JPG = "image/jpg"
        const val IMAGE_PNG = "image/png"
    }
}