package pl.gosak.rit.photo.property

import org.springframework.boot.context.properties.ConfigurationProperties

@ConfigurationProperties(prefix = "sightengine")
class SightEngineProperties(var addr: String = "",
                            var user: String = "",
                            var secret: String = "")