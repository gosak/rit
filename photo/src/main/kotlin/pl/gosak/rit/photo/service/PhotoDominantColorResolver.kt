package pl.gosak.rit.photo.service

import java.awt.image.BufferedImage

interface PhotoDominantColorResolver {
    fun dominant(bufferedImage: BufferedImage): String
}