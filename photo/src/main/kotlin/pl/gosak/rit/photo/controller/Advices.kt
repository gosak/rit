package pl.gosak.rit.photo.controller

import org.apache.tomcat.util.http.fileupload.FileUploadBase
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.client.HttpClientErrorException
import org.springframework.web.multipart.MaxUploadSizeExceededException
import org.springframework.web.server.ServerErrorException
import pl.gosak.rit.microbase.util.responseEntity
import java.io.FileNotFoundException

@ControllerAdvice
class MaxUploadSizeExceededExceptionAdvice {
    @ExceptionHandler(MaxUploadSizeExceededException::class)
    fun handle() = HttpStatus.PAYLOAD_TOO_LARGE.responseEntity()
}

@ControllerAdvice
class FileSizeLimitExceededExceptionAdvice {
    @ExceptionHandler(FileUploadBase.FileSizeLimitExceededException::class)
    fun handle() = HttpStatus.PAYLOAD_TOO_LARGE.responseEntity()
}

@ControllerAdvice
class FileNotFoundExceptionAdvice {
    @ExceptionHandler(FileNotFoundException::class)
    fun handle() = HttpStatus.NOT_FOUND.responseEntity()
}

@ControllerAdvice
class ServerErrorExceptionAdvice {
    @ExceptionHandler(ServerErrorException::class)
    fun handle() = HttpStatus.INTERNAL_SERVER_ERROR.responseEntity()
}

@ControllerAdvice
class HttpClientErrorExceptionAdvice {
    @ExceptionHandler(HttpClientErrorException::class)
    fun handle() = HttpStatus.UNSUPPORTED_MEDIA_TYPE.responseEntity()
}