package pl.gosak.rit.photo.service

import pl.gosak.rit.photo.model.SubjectCache
import java.util.*

interface SubjectCacheService {
    fun create(subjectCache: SubjectCache)

    fun load(uuid: UUID): SubjectCache
}