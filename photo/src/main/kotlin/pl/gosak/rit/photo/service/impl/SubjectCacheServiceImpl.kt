package pl.gosak.rit.photo.service.impl

import org.springframework.stereotype.Service
import pl.gosak.rit.photo.model.SubjectCache
import pl.gosak.rit.photo.repository.SubjectCacheRepository
import pl.gosak.rit.photo.service.SubjectCacheService
import java.util.*
import javax.transaction.Transactional


@Service
class SubjectCacheServiceImpl(private val subjectCacheRepository: SubjectCacheRepository) : SubjectCacheService {
    @Transactional
    override fun create(subjectCache: SubjectCache) = subjectCacheRepository.persist(subjectCache)

    override fun load(uuid: UUID) = subjectCacheRepository.load(uuid)
}