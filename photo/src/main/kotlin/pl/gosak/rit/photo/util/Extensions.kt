package pl.gosak.rit.photo.util

import org.springframework.http.HttpEntity
import org.springframework.util.MultiValueMap
import org.springframework.web.client.RestTemplate
import pl.gosak.rit.photo.model.SightEngineResponse


fun RestTemplate.sightEngineCall(addr: String, request: HttpEntity<MultiValueMap<String, Any>>) =
        postForObject<SightEngineResponse>(addr, request, SightEngineResponse::class.java)
