package pl.gosak.rit.photo.model

class SightEngineResponse(
        var status: String,
        var weapon: Double,
        var alcohol: Double,
        var drugs: Double,
        var nudity: Nudity,
        var offensive: Offensive
)

class Nudity(var safe: Double)

class Offensive(var prob: Double)