package pl.gosak.rit.photo.exception

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
class PhotoNotAcceptableException : Exception()

@ResponseStatus(HttpStatus.UNSUPPORTED_MEDIA_TYPE)
class FileTypeNotAcceptableException : Exception()