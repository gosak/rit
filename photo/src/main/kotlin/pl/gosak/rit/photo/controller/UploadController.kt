package pl.gosak.rit.photo.controller


import org.modelmapper.ModelMapper
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.core.Authentication
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestHeader
import org.springframework.web.bind.annotation.RequestPart
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.multipart.MultipartFile
import pl.gosak.rit.lib.SecurityConstants.AUTHORIZATION_HEADER
import pl.gosak.rit.lib.photo.PhotoEndpoints.PHOTO_UPLOAD
import pl.gosak.rit.lib.photo.PhotoEndpoints.Parts.PHOTO
import pl.gosak.rit.lib.photo.PhotoEndpoints.Parts.SUBJECT_UUID
import pl.gosak.rit.lib.photo.dto.UploadFileResponse
import pl.gosak.rit.microbase.security.User
import pl.gosak.rit.microbase.util.responseEntity
import pl.gosak.rit.photo.service.UploadService
import java.util.*


@RestController
class UploadController(private val uploadService: UploadService,
                       private val modelMapper: ModelMapper) {
    @PostMapping(PHOTO_UPLOAD)
    fun uploadPhoto(authentication: Authentication,
                    @RequestPart(PHOTO) file: MultipartFile,
                    @RequestPart(SUBJECT_UUID) subjectUuid: UUID,
                    @RequestHeader(AUTHORIZATION_HEADER) jwt: String): ResponseEntity<UploadFileResponse> {
        val photoEntity = uploadService.upload(authentication.principal as User, subjectUuid, file)
        return HttpStatus.CREATED.responseEntity(modelMapper.map(photoEntity, UploadFileResponse::class.java))
    }
}