package pl.gosak.rit.photo.model

import pl.gosak.rit.photo.model.SubjectCache.Names.TABLE
import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table


@Entity
@Table(name = TABLE)
class SubjectCache(
        @Id
        @Column(name = DB_SUBJECT_UUID, nullable = false)
        val subjectUuid: UUID
) {
    companion object Names {
        const val TABLE = "subject_cache"
        const val SUBJECT_UUID = "subjectUuid"
        const val DB_SUBJECT_UUID = "subject_uuid"
    }
}