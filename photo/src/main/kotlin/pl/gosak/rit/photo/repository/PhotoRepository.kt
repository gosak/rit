package pl.gosak.rit.photo.repository

import pl.gosak.rit.photo.model.Photo
import java.util.*


interface PhotoRepository {
    fun add(photo: Photo): Photo

    fun find(photoUuid: UUID): Photo

    fun delete(photo: Photo): Boolean
}

